'use strict';

const fs = require('fs');
const _get = require('lodash/get');
const _pick = require('lodash/pick');
const validator = require('validator');
const _extend = require('lodash/extend');
const _isEmpty = require('lodash/isEmpty');
const _includes = require('lodash/includes');
const _cloneDeep = require('lodash/cloneDeep');

const {
    User,
    Thread,
    UserToken,
    UserProfile,
    MessageRecipient,
    generateSearchQuery,
    sequelize
} = require('../data/models');
const { Mailer, Thumbnail } = require('../components/');
const { ErrorMessages, UserTokenType } = require('../constants');

const config = require('../config');

class UserHandler {
    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { q = '' } = ctx.query;

        const query = !_isEmpty(q) ? generateSearchQuery(q) : {};

        const { rows: users, count: total } = await User.scope({
            method: ['expand']
        }).findAndCountAll({
            where: query,
            offset,
            limit
        });

        return ctx.ok({
            data: users,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionView(ctx) {
        const id = ctx.params.id === 'me' ? ctx.state.user.id : ctx.params.id;

        const user = await User.scope({
            method: ['expand']
        }).findOne({
            where: { id }
        });

        if (_isEmpty(user)) {
            return ctx.notFound(ErrorMessages.USER_NOT_FOUND);
        }

        return ctx.ok({ data: user });
    }

    static async actionCreate(ctx) {
        const user = await sequelize.transaction(async transaction => {
            const userModel = await User.create(_pick(ctx.request.body, ['email', 'password']), {
                transaction
            });

            const profile = await UserProfile.create(
                _extend(_pick(ctx.request.body, ['firstName', 'lastName', 'gender', 'typeId']), {
                    userId: userModel.id
                }),
                { transaction }
            );

            await userModel.createActivationToken({}, { transaction });

            return _extend(userModel, { profile });
        });
        await user.save();

        Mailer.send(
            _extend(
                {
                    data: {
                        firstName: user.profile.firstName,
                        activationCode: `http://${config.get('server:url')}/users/activation?token=${
                            (await user.getActivationToken()).token
                        }`
                    },
                    to: [user.email]
                },
                config.get('params:emailTemplates:activation')
            )
        ).catch(console.error);

        return ctx.created({ data: user });
    }

    static async actionDelete(ctx) {
        return (await User.destroy({
            where: {
                id: ctx.state.user.id
            }
        }))
            ? ctx.accepted()
            : ctx.notFound();
    }

    static async actionLogin(ctx) {
        let user = {};
        let { token = '', email = '', password } = ctx.request.body;

        if (_isEmpty(token)) {
            user = await User.scope({
                method: ['expand']
            }).findOne({
                where: {
                    email: validator.normalizeEmail(email),
                    isVerified: true
                }
            });

            if (_isEmpty(user) || !(await user.comparePassword(password))) {
                return ctx.notFound(ErrorMessages.INVALID_CREDENTIALS);
            }
        } else {
            user = await User.findOne({
                include: [{ model: UserToken, as: 'tempAccessToken', where: { token } }, 'profile']
            });

            if (_isEmpty(user)) {
                return ctx.notFound();
            }

            await UserToken.destroy({ where: { token } });
        }

        let oldUser = _cloneDeep(user);

        return ctx.ok({
            data: {
                user: oldUser,
                info: {
                    followers: await user.countReceivedRequests({
                        where: {
                            receiverId: user.id
                        }
                    }),
                    followings: await user.countReceivedRequests({
                        where: {
                            senderId: user.id
                        }
                    }),
                    // unSeenNotifications: await ActivityLog.count({
                    //     where: { receiverId: user.id, isSeen: false, isNotification: true }
                    // }),
                    unreadThreads: await Thread.count({
                        where: { '$recipient.isSeen$': false, '$recipient.userId$': user.id },
                        include: [
                            {
                                model: MessageRecipient,
                                as: 'recipient'
                            }
                        ],
                        distinct: true,
                        col: 'id'
                    })
                },
                token: user.generateToken()
            }
        });
    }

    static async actionUpdate(ctx) {
        let profileData = _pick(ctx.request.body, [
            'university',
            'profession',
            'firstName',
            'lastName',
            'avatar',
            'gender',
            'typeId',
            'work'
        ]);
        const profile = await ctx.state.user.getProfile();

        return ctx.ok({ data: await profile.update(profileData) });
    }

    static async actionUploadAvatar(ctx) {
        const { extensions, size, dimension } = config.get('params:validation:files:image');
        let media = _get(ctx.request, 'files.avatar');

        if (_isEmpty(media)) {
            return ctx.badRequest(ErrorMessages.FILE_REQUIRED);
        } else if (!_includes(extensions, media.ext)) {
            return ctx.unsupportedMediaType(ErrorMessages.INVALID_FILE_TYPE);
        } else if (media.size > size) {
            return ctx.badRequest(ErrorMessages.INVALID_FILE_SIZE);
        }

        media = await Thumbnail.image(media, dimension);

        const thumbnail = await Thumbnail.image(media, dimension / 2);

        const mediaKey = `avatar/${ctx.state.user.id}/${media.key}`;
        fs.mkdirSync(`media/avatar/${ctx.state.user.id}`, { recursive: true }, err => {
            if (err) throw err;
        });
        fs.writeFileSync(`media/${mediaKey}`, fs.readFileSync(thumbnail.path));

        const profile = await ctx.state.user.getProfile();

        return ctx.accepted({ data: await profile.update({ avatar: mediaKey }) });
    }

    static async actionActivation(ctx) {
        const { token } = ctx.query;

        const userToken = await UserToken.findOne({
            where: {
                token,
                typeId: UserTokenType.ACTIVATION
            }
        });

        if (_isEmpty(userToken)) {
            return ctx.notFound(ErrorMessages.INVALID_TOKEN);
        }

        const user = await userToken.getUser();
        user.isVerified = true;

        await user.save();
        await userToken.destroy();

        return ctx.accepted();
    }

    static async actionNotConnect(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { id: userId } = ctx.state.user;
        const { q = '', type = '' } = ctx.query;

        const searchCondition = !_isEmpty(q)
            ? generateSearchQuery(q, ['profile.firstName', 'profile.lastName'])
            : {};

        const { rows: users, count: total } = await User.scope({
            method: ['notConnect', userId, searchCondition, type]
        }).findAndCountAll({
            offset,
            limit
        });

        return ctx.ok({
            data: users,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }
}

module.exports = UserHandler;
