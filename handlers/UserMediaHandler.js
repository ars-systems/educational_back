'use strict';

const fs = require('fs');
const _get = require('lodash/get');
const _pick = require('lodash/pick');
const scissors = require('scissors');
const _extend = require('lodash/extend');
const _isEmpty = require('lodash/isEmpty');
const _includes = require('lodash/includes');

const { User, Media, MediaRead, generateSearchQuery } = require('./../data/models');
const { ErrorMessages, MediaType } = require('./../constants');
const config = require('./../config');

class UserMediaHandler {
    static async actionCreate(ctx) {
        let mediaData = _pick(ctx.request.body, ['name', 'sphere', 'author', 'description', 'availability']);
        const { extensions, size } = config.get('params:validation:files:media');
        let media = _get(ctx.request, 'files.media');
        let image = _get(ctx.request, 'files.image');
        const { typeId } = ctx.query;
        const { user } = ctx.state;

        if (_isEmpty(media) || _isEmpty(image)) {
            return ctx.badRequest(ErrorMessages.FILE_REQUIRED);
        } else if (!_includes(extensions, _get(media, 'ext'))) {
            return ctx.badRequest(ErrorMessages.INVALID_FILE_TYPE);
        } else if (_get(media, 'size') > size) {
            return ctx.badRequest(ErrorMessages.INVALID_FILE_SIZE);
        }

        fs.mkdirSync(`media/file/${user.id}/`, { recursive: true }, err => err);
        fs.writeFileSync(`media/file/${user.id}/${media.key}`, fs.readFileSync(media.path));

        fs.mkdirSync(`media/image/${user.id}/`, { recursive: true }, err => err);
        fs.writeFileSync(`media/image/${user.id}/${image.key}`, fs.readFileSync(image.path));

        mediaData.image = `image/${user.id}/${image.key}`;

        if (typeId === MediaType.BOOK) {
            if (mediaData.availability === 'false') {
                fs.mkdirSync(`media/thumb/${user.id}/`, { recursive: true }, err => err);
                await scissors(`media/file/${user.id}/${media.key}`)
                    .range(1, 5)
                    .pdfStream()
                    .pipe(await fs.createWriteStream(`media/thumb/${user.id}/${media.key}`));

                mediaData.thumbnail = `thumb/${user.id}/${media.key}`;
            }

            mediaData.pageCount = await scissors(`media/file/${user.id}/${media.key}`).getNumPages();
        }

        const userMedia = await user.createMedia(
            _extend(mediaData, { data: `file/${user.id}/${media.key}`, meta: media, typeId })
        );

        return ctx.created({ media: userMedia });
    }

    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        let { category = '', type = '', q = '' } = ctx.query;

        const query = !_isEmpty(q) ? generateSearchQuery(q, ['author', 'name']) : {};
        let where = type
            ? { ...query, typeId: Number(type) }
            : _extend({ ...query }, { typeId: { $ne: MediaType.GROUP_MEDIA_ATTACHMENT } });

        where = !_isEmpty(category) ? { ...where, category: Number(category) } : where;

        const { rows: usersMedia, count: total } = await Media.findAndCountAll({
            where,
            order: [['name', 'asc']],
            attributes: ['id', 'name', 'data', 'image', 'author', 'typeId', 'category'],
            offset,
            limit
        });

        return ctx.ok({
            usersMedia,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionView(ctx) {
        const media = await Media.findOne({
            where: {
                id: ctx.params.id
            },
            include: [{ model: User, as: 'user', include: ['profile'] }]
        });

        if (_isEmpty(media)) {
            return ctx.notFound(ErrorMessages.MEDIA_NOT_FOUND);
        }

        const recommendedBooks = await Media.findAll({
            where: { category: media.category, id: { $ne: media.id } },
            attributes: ['id', 'image'],
            limit: 10,
            order: [['createdAt', 'desc']]
        });

        return ctx.ok({ data: media, recommendedBooks });
    }

    static async actionRead(ctx) {
        const { id: userId } = ctx.state.user;
        const { mediaId } = ctx.params;
        const { status } = ctx.request.body;

        const media = await Media.findByPk(mediaId);

        if (_isEmpty(media)) {
            return ctx.notFound(ErrorMessages.MEDIA_NOT_FOUND);
        }

        let mediaRead = await MediaRead.findOne({
            where: { userId, mediaId }
        });

        if (_isEmpty(mediaRead)) {
            mediaRead = MediaRead.create({ mediaId, userId, status });
        } else {
            mediaRead.status = status;

            await mediaRead.save();
        }

        return ctx.ok({ data: mediaRead });
    }

    static async actionDelete(ctx) {
        const result = await Media.destroy({
            where: {
                id: ctx.params.id,
                userId: ctx.state.user.id
            }
        });

        return result ? ctx.accepted() : ctx.notFound();
    }
}

module.exports = UserMediaHandler;
