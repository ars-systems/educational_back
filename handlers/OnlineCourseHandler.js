'use strict';

const fs = require('fs');
const _ = require('lodash');

const { User, OnlineCourse, sequelize, generateSearchQuery } = require('../data/models');
const { ErrorMessages, GroupConnectionStatus, AccountType } = require('../constants');
const config = require('./../config');

class OnlineCourseHandler {
    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { type = '', q = '' } = ctx.query;

        const query = !_.isEmpty(q) ? generateSearchQuery(q, ['type', 'category']) : {};

        const where = type ? { ...query, typeId: Number(type) } : query;

        const { rows: courses, count: total } = await OnlineCourse.findAndCountAll({
            where,
            include: [
                {
                    model: User,
                    as: 'creator',
                    include: ['profile']
                }
            ],
            order: [['startDate', 'DESC'], ['name', 'ASC']],
            offset,
            limit
        });

        return ctx.ok({
            data: courses,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionView(ctx) {
        const { id } = ctx.params;

        const course = await OnlineCourse.findOne({
            where: { id },
            include: [
                {
                    model: User,
                    as: 'creator',
                    include: ['profile']
                }
            ]
        });

        if (_.isEmpty(course)) {
            return ctx.notFound(ErrorMessages.COURSE_NOT_FOUND);
        }

        return ctx.ok({ data: course });
    }

    static async actionCreate(ctx) {
        const { extensions, size } = config.get('params:validation:files:image');
        const image = _.get(ctx.request, 'files.image');
        const { user } = ctx.state;
        let onlineCourseImage = '';

        if (
            !(await User.count({
                where: {
                    id: user.id,
                    $or: [
                        { '$profile.typeId$': AccountType.LECTURER },
                        { '$profile.typeId$': AccountType.COMPANY }
                    ]
                },
                include: ['profile']
            }))
        ) {
            return ctx.badRequest(ErrorMessages.YOU_CAN_NOT_CREATE_COURSE);
        }

        if (!_.isEmpty(image)) {
            if (!_.includes(extensions, _.get(image, 'ext'))) {
                return ctx.badRequest(ErrorMessages.INVALID_FILE_TYPE);
            } else if (_.get(image, 'size') > size) {
                return ctx.badRequest(ErrorMessages.INVALID_FILE_SIZE);
            }
        }

        const onlineCourse = await sequelize.transaction(async transaction => {
            const course = await OnlineCourse.create(
                _.extend(
                    _.pick(ctx.request.body, [
                        'name',
                        'type',
                        'duration',
                        'category',
                        'startDate',
                        'description',
                        'availability',
                        'participantsCount'
                    ]),
                    {
                        userId: user.id
                    }
                ),
                { transaction }
            );

            if (!_.isEmpty(image)) {
                fs.mkdirSync(`media/onlineCourse/image/${course.id}`, { recursive: true }, err => err);
                fs.writeFileSync(
                    `media/onlineCourse/image/${course.id}/${image.key}`,
                    fs.readFileSync(image.path)
                );

                onlineCourseImage = `onlineCourse/image/${course.id}/${image.key}`;
            }

            course.image = onlineCourseImage;

            await course.save();

            return course;
        });

        return ctx.created({ data: onlineCourse });
    }

    static async actionDelete(ctx) {
        return (await OnlineCourse.destroy({
            where: {
                id: ctx.params.id,
                userId: ctx.state.user.id
            }
        }))
            ? ctx.accepted()
            : ctx.notFound();
    }

    static async actionUpdate(ctx) {
        const { extensions, size } = config.get('params:validation:files:media');
        let image = _.get(ctx.request, 'files.image');
        const { id: userId } = ctx.state.user;
        const { courseId } = ctx.params;
        let onlineCourseImage = '';

        const course = await OnlineCourse.findOne({
            where: { id: courseId, userId, status: GroupConnectionStatus.ACCEPTED }
        });

        if (_.isEmpty(course) || _.get(course, 'startDate') <= new Date(Date.now() + 86400000)) {
            return ctx.notFound(ErrorMessages.COURSE_NOT_FOUND_OR_YOU_HAVE_NOT_ACCESS);
        }

        if (!_.isEmpty(image)) {
            if (!_.includes(extensions, _.get(image, 'ext'))) {
                return ctx.badRequest(ErrorMessages.INVALID_FILE_TYPE);
            } else if (_.get(image, 'size') > size) {
                return ctx.badRequest(ErrorMessages.INVALID_FILE_SIZE);
            }
        }

        if (!_.isEmpty(image)) {
            fs.mkdirSync(`media/onlineCourse/image/${courseId}`, { recursive: true }, err => err);
            fs.writeFileSync(`media/onlineCourse/image/${courseId}/${image.key}`, fs.readFileSync(image.path));

            onlineCourseImage = `onlineCourse/image/${courseId}/${image.key}`;
        }

        const courseData = await course.update(
            _.extend(
                _.pick(ctx.request.body, [
                    'name',
                    'type',
                    'duration',
                    'category',
                    'startDate',
                    'description',
                    'availability',
                    'participantsCount'
                ]),
                {
                    image: onlineCourseImage
                }
            )
        );

        return ctx.ok({ data: courseData });
    }
}

module.exports = OnlineCourseHandler;
