'use strict';

const _get = require('lodash/get');
const _map = require('lodash/map');
const _take = require('lodash/take');
const _pick = require('lodash/pick');
const _extend = require('lodash/extend');
const _without = require('lodash/without');
const _isEmpty = require('lodash/isEmpty');
const _includes = require('lodash/includes');
const _difference = require('lodash/difference');

const {
    User,
    Company,
    UserWork,
    UserProfile,
    CompanyConnect,
    CompanyUserConnect,
    sequelize,
    generateSearchQuery
} = require('./../data/models');
const { UserConnectStatusType, ActivityLogType, ErrorMessages } = require('../constants');
const config = require('./../config');

class CompanyConnectHandler {
    static async actionCreate(ctx) {
        const { user } = ctx.state;

        const profile = await user.getProfile();

        let ids = _take(
            _without(_get(ctx, 'request.body.ids'), user.id),
            config.get('params:validation:connect:max:count')
        );

        if (_isEmpty(ids)) {
            return ctx.badRequest(ErrorMessages.IDS_REQUIRED);
        }

        let work = await user.getWork({ attributes: ['companyId'] });

        if (_isEmpty(work)) {
            return ctx.notFound(ErrorMessages.WORK_NOT_FOUND);
        }

        let invitedConnects = await CompanyConnect.findAll({
            where: {
                $or: [
                    { senderId: user.id, receiverId: { $in: ids } },
                    { receiverId: user.id, senderId: { $in: ids } }
                ],
                $and: {
                    $or: [
                        { statusId: UserConnectStatusType.PENDING },
                        {
                            '$userConnect.statusId$': {
                                $ne: UserConnectStatusType.DISCONNECTED
                            }
                        }
                    ]
                }
            },
            include: [{ model: CompanyUserConnect, as: 'userConnect' }]
        });

        let usersHasCompany = await CompanyUserConnect.findAll({
            where: { userId: { $in: ids }, connectId: { $is: null } }
        });

        let companyIds = [];

        _map(usersHasCompany, item =>
            _map(ids, id => {
                if (item.userId === id) {
                    companyIds.push(item.userId);
                }
            })
        );

        invitedConnects = _map(invitedConnects, connect =>
            connect.senderId === user.id ? connect.receiverId : connect.senderId
        );

        let differentIds = _difference(ids, invitedConnects, companyIds);

        if (_isEmpty(differentIds)) {
            return ctx.badRequest(ErrorMessages.INVALID_IDS);
        }

        await sequelize.transaction(async transaction => {
            await CompanyConnect.destroy({
                where: {
                    $or: [
                        { senderId: user.id, receiverId: { $in: differentIds } },
                        { receiverId: user.id, senderId: { $in: differentIds } }
                    ]
                },
                transaction
            });

            await CompanyConnect.bulkCreate(
                _map(differentIds, id => ({
                    senderId: user.id,
                    receiverId: id,
                    companyId: work.companyId
                })),
                { transaction, validate: true }
            );
        });

        let data = {
            sender: _extend(await user.fields({}, user), { profile: await profile.fields() }),
            typeId: ActivityLogType.COMPANY_CONNECTION_NEW
        };

        for (const id of differentIds) {
            global.io.to(id).emit('company:connection:new', data);
        }

        return ctx.accepted();
    }

    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { type = 'connects', sort = 'name', q = '' } = ctx.query;
        const { user } = ctx.state;

        if (!_includes(['disconnects', 'connects'], type) || !_includes(['name', 'date'], sort)) {
            return ctx.badRequest(ErrorMessages.INVALID_TYPE);
        }

        const work = await user.getWork({ attributes: ['companyId'] });

        if (_isEmpty(work)) {
            return ctx.notFound(ErrorMessages.WORK_NOT_FOUND);
        }

        const queryString = !_isEmpty(q)
            ? generateSearchQuery(q, ['receiver->profile.firstName', 'receiver->profile.lastName'])
            : {};

        const { rows: users, count: total } = await CompanyUserConnect.scope({
            method: [type, user.id, work.companyId, queryString]
        }).findAndCountAll({
            limit,
            offset,
            order: sequelize.literal(sort === 'date' ? `"createdAt" desc` : `"receiver.profile.firstName" asc`)
        });

        return ctx.ok({
            data: users,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionUsers(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        let { q = '' } = ctx.query;

        let queryString = !_isEmpty(q) ? generateSearchQuery(q) : {};

        let { rows: users, count: total } = await User.findAndCountAll({
            where: {
                id: {
                    $ne: ctx.state.user.id
                }
            },
            include: [
                {
                    model: UserProfile,
                    as: 'profile',
                    where: {
                        ...queryString
                    }
                },
                {
                    model: UserWork,
                    as: 'work',
                    attributes: ['industry', 'occupation'],
                    include: [
                        {
                            model: Company,
                            as: 'company',
                            attributes: ['name']
                        }
                    ]
                }
            ],
            limit,
            offset
        });

        return ctx.ok({
            data: users,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionUpdate(ctx) {
        let { id, status } = ctx.request.body;
        const { user } = ctx.state;

        const profile = await user.getProfile();

        if (!_includes(['accept', 'reject'], status)) {
            return ctx.badRequest(ErrorMessages.MISSING_ARGUMENTS);
        }

        const companyConnect = await CompanyConnect.findOne({
            where: {
                $or: [{ senderId: user.id, receiverId: id }, { receiverId: user.id, senderId: id }]
            },
            include: [{ model: Company, as: 'company', attributes: ['name'] }]
        });

        if (_isEmpty(companyConnect)) {
            return ctx.notFound();
        } else if (companyConnect.statusId !== UserConnectStatusType.PENDING) {
            return ctx.forbidden();
        }

        if (status === 'accept') {
            if (companyConnect.senderId === user.id) {
                return ctx.forbidden();
            }

            await sequelize.transaction(async transaction => {
                await companyConnect.update({ statusId: UserConnectStatusType.ACCEPTED }, { transaction });

                await CompanyUserConnect.create(
                    {
                        companyId: companyConnect.companyId,
                        connectId: companyConnect.id,
                        userId: companyConnect.receiverId
                    },
                    { transaction }
                );

                await CompanyConnect.destroy({
                    where: {
                        receiverId: user.id,
                        statusId: UserConnectStatusType.PENDING
                    },
                    transaction
                });

                await UserWork.destroy({
                    where: { userId: user.id },
                    transaction
                });

                await user.createWork({ companyId: companyConnect.companyId }, { transaction });
            });

            global.io.to(id).emit('company:connection:accepted', {
                sender: _extend(
                    await user.fields({}, user),
                    { profile: await profile.fields() },
                    { company: { name: _get(companyConnect, 'company.name') } }
                ),
                typeId: ActivityLogType.COMPANY_CONNECTION_ACCEPT
            });
        } else {
            await companyConnect.destroy();
        }

        return ctx.accepted();
    }

    static async actionPending(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { type = 'sent', q = '' } = ctx.query;

        if (!_includes(['sent', 'received'], type)) {
            return ctx.badRequest(ErrorMessages.INVALID_TYPE);
        }

        const queryString = !_isEmpty(q) ? generateSearchQuery(q) : {};

        const { rows: users, count: total } = await User.scope({
            method: [`${type}CompanyConnects`, ctx.state.user.id, UserConnectStatusType.PENDING]
        }).findAndCountAll({
            where: {
                ...queryString
            },
            limit,
            offset
        });

        return ctx.ok({
            data: users,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionAutoComplete(ctx) {
        let { q = '' } = ctx.query;

        let queryString = !_isEmpty(q) ? generateSearchQuery(q) : {};

        let users = await User.findAll({
            distinct: true,
            attributes: [
                [
                    sequelize.fn(
                        'concat',
                        sequelize.col('profile.firstName'),
                        ' ',
                        sequelize.col('profile.lastName')
                    ),
                    'text'
                ],
                'id'
            ],
            include: [
                {
                    model: UserProfile,
                    as: 'profile',
                    attributes: [],
                    where: {
                        ...queryString
                    }
                },
                {
                    model: UserWork,
                    as: 'work',
                    attributes: ['industry', 'occupation'],
                    include: [
                        {
                            model: Company,
                            as: 'company',
                            attributes: ['name']
                        }
                    ]
                }
            ],
            group: ['profile.id', 'User.id', 'work.id', 'work->company.id'],
            limit: 5
        });

        return ctx.ok({
            data: users
        });
    }

    static async actionDisconnect(ctx) {
        const { id: userId } = _get(ctx, 'params');
        const { user } = ctx.state;

        let company = await Company.count({ where: { userId } });

        if (userId === ctx.state.user.id || company) {
            return ctx.forbidden();
        }

        let work = await user.getWork({ attributes: ['companyId'] });

        if (_isEmpty(work)) {
            return ctx.notFound(ErrorMessages.WORK_NOT_FOUND);
        }

        let companyUserConnect = await CompanyUserConnect.findOne({
            where: {
                userId,
                companyId: work.companyId,
                statusId: UserConnectStatusType.ACCEPTED
            }
        });

        if (_isEmpty(companyUserConnect)) {
            return ctx.notFound();
        }

        await sequelize.transaction(async transaction => {
            companyUserConnect.statusId = UserConnectStatusType.DISCONNECTED;
            await companyUserConnect.save({ transaction });

            await UserWork.destroy({
                where: {
                    userId
                },
                transaction
            });
        });

        return ctx.noContent();
    }

    static async actionConnection(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { sort = 'asc', q = '' } = ctx.query;
        const { user } = ctx.state;

        const work = await user.getWork({ attributes: ['companyId'] });

        let companyId = { $is: null };

        if (!_isEmpty(work)) {
            companyId = work.companyId;
        }

        const queryString = !_isEmpty(q)
            ? generateSearchQuery(q, ['receiver->profile.firstName', 'receiver->profile.lastName'])
            : {};

        const { rows: users, count: total } = await CompanyUserConnect.findAndCountAll({
            where: {
                '$receiver.id$': {
                    $ne: user.id
                },
                companyId,
                statusId: UserConnectStatusType.ACCEPTED,
                ...queryString
            },
            include: [
                {
                    model: User,
                    as: 'receiver',
                    include: [
                        {
                            model: UserProfile,
                            as: 'profile'
                        },
                        {
                            model: UserWork,
                            as: 'work',
                            attributes: ['industry', 'occupation'],
                            include: [
                                {
                                    model: Company,
                                    as: 'company',
                                    attributes: ['name']
                                }
                            ]
                        }
                    ]
                }
            ],
            limit,
            offset,
            order: sequelize.literal(`"receiver.profile.firstName" ${sort}`)
        });

        return ctx.ok({
            data: _map(users, 'receiver'),
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionUpdateRole(ctx) {
        const { id: userId } = _get(ctx, 'params');
        const { body } = ctx.request;

        if (userId === ctx.state.user.id) {
            return ctx.forbidden();
        }

        const work = await UserWork.findOne({ where: { userId } });

        if (_isEmpty(work)) {
            return ctx.notFound(ErrorMessages.WORK_NOT_FOUND);
        }

        let companyUserConnect = await CompanyUserConnect.findOne({
            where: { companyId: work.companyId, userId }
        });

        if (_isEmpty(companyUserConnect)) {
            return ctx.notFound();
        }

        companyUserConnect = _extend(companyUserConnect, _pick(body, ['roleId']));
        await companyUserConnect.save();

        return ctx.ok({ data: companyUserConnect });
    }

    static async actionUpdateWork(ctx) {
        const { id: userId } = _get(ctx, 'params');
        const { body } = ctx.request;
        const { user } = ctx.state;

        const work = await user.getWork();

        if (_isEmpty(work)) {
            return ctx.notFound(ErrorMessages.WORK_NOT_FOUND);
        }

        let userWork = await UserWork.findOne({ where: { userId, companyId: work.companyId } });

        if (_isEmpty(userWork)) {
            return ctx.notFound(ErrorMessages.WORK_NOT_FOUND);
        }

        userWork = _extend(userWork, _pick(body, ['occupation', 'industry']));
        await userWork.save();

        return ctx.ok({ data: userWork });
    }
}

module.exports = CompanyConnectHandler;
