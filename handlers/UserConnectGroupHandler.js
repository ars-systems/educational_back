'use strict';

const _map = require('lodash/map');
const _get = require('lodash/get');
const _take = require('lodash/take');
const _isEmpty = require('lodash/isEmpty');
const _without = require('lodash/without');

const { UserConnectGroup, UserProfile, Group, User, sequelize, generateSearchQuery } = require('./../data/models');
const { ErrorMessages } = require('../constants');

class UserConnectGroupHandler {
    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { sort = 'asc', q = '' } = ctx.query;
        const { id: groupId } = ctx.params;
        const { user } = ctx.state;

        const group = await Group.findOne({
            where: {
                id: groupId,
                userId: user.id
            }
        });

        if (_isEmpty(group)) {
            return ctx.notFound();
        }

        const queryString = !_isEmpty(q) ? generateSearchQuery(q) : {};

        const { rows: connectGroupUser, count: total } = await User.findAndCountAll({
            subQuery: false,
            include: [
                {
                    model: UserConnectGroup,
                    as: 'connectGroupUsers',
                    attributes: [],
                    where: {
                        groupId
                    }
                },
                {
                    model: UserProfile,
                    as: 'profile',
                    where: queryString
                }
            ],
            order: sequelize.literal(`"profile"."firstName" ${sort}`),
            limit,
            offset
        });

        return ctx.ok({
            data: connectGroupUser,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionNotInGroup(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { sort = 'asc', q = '' } = ctx.query;
        const { id: groupId } = ctx.params;
        const { user } = ctx.state;

        const group = await Group.findOne({
            where: {
                id: groupId,
                userId: user.id
            }
        });

        if (_isEmpty(group)) {
            return ctx.notFound();
        }

        const queryString = !_isEmpty(q) ? generateSearchQuery(q) : {};

        const { rows: connectGroupUser, count: total } = await User.findAndCountAll({
            subQuery: false,
            include: [
                {
                    model: UserConnectGroup,
                    as: 'connectGroupUsers',
                    attributes: [],
                    where: {
                        groupId
                    }
                },
                {
                    model: UserProfile,
                    as: 'profile',
                    where: queryString
                }
            ],
            order: sequelize.literal(`"profile"."firstName" ${sort}`),
            limit,
            offset
        });

        return ctx.ok({
            data: connectGroupUser,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionSearch(ctx) {
        const { user } = ctx.state;
        const { id: groupId } = ctx.params;
        const { sort = 'asc', q = '' } = ctx.query;
        const { limit, offset, page: currentPage } = ctx.state.paginate;

        const group = await UserConnectGroup.findOne({
            where: {
                id: groupId,
                userId: user.id
            }
        });

        if (_isEmpty(group)) {
            return ctx.notFound();
        }

        const availableUsersLiteral = {
            id: {
                $notIn: UserConnectGroup.generateNestedQuery({
                    attributes: ['userId'],
                    where: {
                        groupId
                    }
                })
            }
        };

        const queryString = !_isEmpty(q) ? generateSearchQuery(q) : {};

        const { rows: users, count: total } = await User.scope({
            method: ['connects', user.id, availableUsersLiteral, queryString]
        }).findAndCountAll({
            subQuery: false,
            order: sequelize.literal(`"profile"."firstName" ${sort}`),
            limit,
            offset
        });

        return ctx.ok({
            data: users,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionCreate(ctx) {
        const { user } = ctx.state;
        const { id: groupId } = ctx.params;

        const group = await Group.findOne({
            where: {
                id: groupId,
                userId: user.id
            }
        });

        if (_isEmpty(group)) {
            return ctx.notFound();
        }

        let ids = _take(_without(_get(ctx, 'request.body.ids'), user.id));

        if (_isEmpty(ids)) {
            return ctx.badRequest(ErrorMessages.IDS_REQUIRED);
        }

        await UserConnectGroup.bulkCreate(
            _map(ids, id => ({
                groupId,
                userId: id
            }))
        );

        return ctx.created();
    }

    static async actionDelete(ctx) {
        const { user } = ctx.state;
        const { ids: userId } = ctx.query;
        const { id: groupId } = ctx.params;

        const group = await UserConnectGroup.findOne({
            where: {
                id: groupId,
                userId: user.id
            }
        });

        if (_isEmpty(group)) {
            return ctx.notFound();
        }

        await UserConnectGroup.destroy({
            where: {
                groupId,
                userId
            }
        });

        return ctx.noContent();
    }
}

module.exports = UserConnectGroupHandler;
