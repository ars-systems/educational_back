'use strict';

const _isEmpty = require('lodash/isEmpty');

const { User, Media, SavedMedia, generateSearchQuery } = require('./../data/models');
const { ErrorMessages } = require('./../constants');

class MediaViewHandler {
    static async actionFollowerMedia(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { type = '', q = '' } = ctx.query;

        const query = !_isEmpty(q) ? generateSearchQuery(q) : {};
        const where = type ? { ...query, typeId: Number(type) } : query;

        const { rows: usersMedia, count: total } = await User.scope({
            method: ['media', ctx.state.user.id]
        }).findAndCountAll({
            where,
            offset,
            limit
        });

        return ctx.ok({
            usersMedia,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionSave(ctx) {
        const { id: userId } = ctx.state.user;
        const { mediaId } = ctx.params;

        const media = await Media.findByPk(ctx.params.id);

        if (_isEmpty(media)) {
            return ctx.notFound(ErrorMessages.MEDIA_NOT_FOUND);
        }

        const savedMedia = await SavedMedia.create({ userId, mediaId });

        return ctx.ok({ data: savedMedia });
    }

    static async actionView(ctx) {
        const id = ctx.params.id === 'me' ? ctx.state.user.id : ctx.params.id;

        const media = await Media.findOne({ where: { userId: id } });

        return ctx.ok({ data: media || [] });
    }
}

module.exports = MediaViewHandler;
