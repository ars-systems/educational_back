'use strict';

const fs = require('fs');
const _get = require('lodash/get');
const _has = require('lodash/has');
const _uniq = require('lodash/uniq');
const _pickBy = require('lodash/pickBy');
const _filter = require('lodash/filter');
const _extend = require('lodash/extend');
const _isEmpty = require('lodash/isEmpty');
const _isNumber = require('lodash/isNumber');
const _castArray = require('lodash/castArray');

const {
    UserConnectOnlineCourse,
    OnlineCourseAttachment,
    OnlineCourseMedia,
    OnlineCourseLink,
    Media,
    sequelize
} = require('./../data/models');
const { ErrorMessages, OnlineCourseMediaType, MediaType } = require('./../constants');

class OnlineCourseMediaHandler {
    static async actionCreate(ctx) {
        const { courseId } = ctx.params;
        const { user } = ctx.state;
        let { body } = ctx.request;

        body = _pickBy(body, v => _isNumber(v) || !_isEmpty(v));

        if (!(await UserConnectOnlineCourse.count({ where: { id: courseId, userId: user.id } }))) {
            return ctx.notFound(ErrorMessages.INVALID_CREDENTIALS);
        }

        let attachment;
        let mediaAttachment = [];
        const uploadedFiles = [];
        const hasLink = !_isEmpty(_get(body, 'url'));
        const hasBody = !_isEmpty(_get(body, 'text')) || hasLink;
        const hasAttachment = !_isEmpty(_get(ctx, 'request.files'));
        const hasMediaId = !_isEmpty(_get(body, 'mediaId'));

        if (!hasBody && !hasAttachment && !hasMediaId) {
            return ctx.badRequest();
        }

        if (hasAttachment) {
            attachment = ctx.request.files.attachment;

            if (_isEmpty(attachment)) {
                return ctx.badRequest();
            }
        }

        if (hasMediaId) {
            let mediaId = _uniq(_filter(_castArray(body.mediaId), id => Number(id)));

            if (!_isEmpty(mediaId)) {
                mediaAttachment = await Media.findOne({
                    where: { id: mediaId, userId: user.id },
                    attributes: ['data']
                });

                if (_isEmpty(mediaAttachment)) {
                    return ctx.notFound(ErrorMessages.MEDIA_NOT_FOUND);
                }
            }
        }

        user.profile = await user.getProfile();

        const onlineCourseMedia = await sequelize.transaction(async transaction => {
            const model = new OnlineCourseMedia({
                courseId,
                text: body.text,
                userId: user.id
            });

            await model.save({ transaction });

            _extend(model, {
                attachment: []
            });

            if (!_isEmpty(mediaAttachment)) {
                model.attachment = await OnlineCourseAttachment.create(
                    {
                        key: mediaAttachment.data,
                        courseId: model.id
                    },
                    { transaction, validate: true }
                );
            }

            if (hasBody && !hasAttachment && !hasMediaId) {
                if (hasLink) {
                    const linkModel = new OnlineCourseLink({
                        courseId: model.id,
                        url: _get(body, 'url')
                    });

                    model.typeId = OnlineCourseMediaType.LINK;
                    _extend(model, {
                        link: await linkModel.save({ transaction })
                    });
                } else {
                    model.typeId = OnlineCourseMediaType.TEXT;
                }
            } else if (hasAttachment || hasMediaId) {
                model.typeId = OnlineCourseMediaType.FILE;
            }

            if (model.typeId !== OnlineCourseMediaType.NO_CONTENT) {
                await model.save({ transaction });
            }

            if (hasAttachment && !_isEmpty(attachment)) {
                const attachmentKey = `onlineCourse/attachment/${courseId}/${_get(attachment, 'key')}`;
                fs.mkdirSync(`media/onlineCourse/attachment/${courseId}`, { recursive: true }, err => {
                    if (err) throw err;
                });
                fs.writeFileSync(`media/${attachmentKey}`, fs.readFileSync(attachment.path));

                await Media.create(
                    {
                        userId: user.id,
                        data: attachmentKey,
                        typeId: MediaType.ONLINE_COURSE_ATTACHMENT,
                        description: _get(ctx, 'request.description')
                    },
                    { transaction }
                );
                const attachmentModel = new OnlineCourseAttachment({
                    courseId: model.id,
                    key: attachmentKey
                });

                model.attachment.push(await attachmentModel.save({ transaction }));
                uploadedFiles.push({
                    source: attachment,
                    key: attachmentModel.key
                });

                if (_isEmpty(model.attachment)) {
                    throw new Error();
                }
            }

            if (!_isEmpty(uploadedFiles)) {
                // Todo aws save
            }

            return _extend(model, {
                user
            });
        });

        return ctx.created({ data: onlineCourseMedia });
    }

    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        let { filter, id } = ctx.request.query;

        const filters = OnlineCourseMedia.filters(id);

        if (!_isEmpty(filter)) {
            if (!_has(filters, filter)) {
                return ctx.badRequest();
            }

            filter = _get(filters, filter);
        } else {
            filter = {};
        }

        const { rows: posts, count: total } = await OnlineCourseMedia.scope({
            method: ['expand', ctx.state.user.id, filter]
        }).findAndCountAll({
            order: [['createdAt', 'DESC']],
            limit,
            offset
        });

        return ctx.ok({
            data: posts,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionView(ctx) {
        const onlineCourseMedia = await OnlineCourseMedia.scope({
            method: ['expand', ctx.state.user.id]
        }).findByPk(ctx.params.courseMediaId);

        return ctx.ok({ data: onlineCourseMedia || [] });
    }

    static async actionUpdate(ctx) {
        let { body } = ctx.request;
        let { user } = ctx.state;

        const onlineCourseMedia = await OnlineCourseMedia.findOne({
            where: {
                id: ctx.params.courseMediaId,
                userId: user.id
            }
        });

        if (_isEmpty(onlineCourseMedia)) {
            return ctx.notFound(ErrorMessages.COURSE_NOT_FOUND);
        }

        body = _pickBy(body, v => _isNumber(v) || !_isEmpty(v));

        let mediaAttachment = [];
        const hasLink = !_isEmpty(_get(body, 'url'));
        const hasBody = !_isEmpty(_get(body, 'text')) || hasLink;
        const hasAttachment = !_isEmpty(_get(body, 'request.files'));
        const hasMediaId = !_isEmpty(_get(body, 'mediaId'));

        if (!hasBody && !hasAttachment && !hasMediaId) {
            return ctx.badRequest();
        }

        let attachments = [];
        if (hasAttachment) {
            attachments = _get(body, 'attachment.files');

            if (_isEmpty(attachments)) {
                return ctx.badRequest();
            }
        }

        if (hasMediaId) {
            let mediaId = _uniq(_filter(_castArray(body.mediaId), id => Number(id)));

            if (!_isEmpty(mediaId)) {
                mediaAttachment = await Media.findAll({
                    where: { id: mediaId, userId: user.id },
                    attributes: ['key']
                });

                if (_isEmpty(mediaAttachment)) {
                    return ctx.notFound();
                }
            }
        }

        user.profile = await user.getProfile();

        const data = await sequelize.transaction(async transaction => {
            onlineCourseMedia.text = _get(body, 'text');

            // remove post attachments
            if (onlineCourseMedia.typeId === OnlineCourseMediaType.FILE) {
                const attachmentId = _castArray(_get(body, 'attachment.deletedId', []));

                if (!_isEmpty(attachmentId)) {
                    await OnlineCourseAttachment.destroy(
                        { where: { id: attachmentId, onlineCourseId: onlineCourseMedia.id } },
                        { transaction }
                    );
                }
            }

            // remove post Link
            if (onlineCourseMedia.typeId === OnlineCourseMediaType.LINK) {
                await OnlineCourseLink.destroy(
                    { where: { onlineCourseId: onlineCourseMedia.id } },
                    { transaction }
                );
            }

            // add attachments from media
            if (!_isEmpty(mediaAttachment)) {
                onlineCourseMedia.attachment = await OnlineCourseAttachment.create(
                    {
                        key: mediaAttachment.key,
                        postId: onlineCourseMedia.id
                    },
                    { transaction }
                );
            }

            // add attachments
            if (!_isEmpty(attachments)) {
                const attachmentKey = `media/onlineCourse/attachment/${onlineCourseMedia.id}/${_get(
                    attachments,
                    'key'
                )}`;
                fs.mkdirSync(`media/onlineCourse/attachment/${onlineCourseMedia.id}`, { recursive: true }, err => {
                    if (err) throw err;
                });
                fs.writeFileSync(`media/${attachmentKey}`, fs.readFileSync(attachments.path));

                await Media.create(
                    {
                        userId: user.id,
                        data: attachmentKey,
                        typeId: MediaType.ONLINE_COURSE_ATTACHMENT
                    },
                    { transaction }
                );

                const attachmentModel = new OnlineCourseAttachment({
                    onlineCourseId: onlineCourseMedia.id,
                    key: attachmentKey
                });

                onlineCourseMedia.attachment.push(await attachmentModel.save({ transaction }));

                if (_isEmpty(onlineCourseMedia.attachment)) {
                    throw new Error();
                }
            }

            if (onlineCourseMedia.typeId !== OnlineCourseMediaType.NO_CONTENT) {
                await onlineCourseMedia.save({ transaction });
            }

            return _extend(onlineCourseMedia, {
                user
            });
        });

        return ctx.ok({ data });
    }

    static async actionDelete(ctx) {
        const { id: userId } = ctx.state.user;

        const media = await OnlineCourseMedia.findOne({
            where: {
                id: ctx.params.courseMediaId,
                $or: [{ userId: userId }, { '$onlineCourse.userId$': userId }]
            },
            include: ['onlineCourse']
        });

        if (_isEmpty(media)) {
            return ctx.notFound(ErrorMessages.INVALID_CREDENTIALS);
        }

        await media.destroy();

        return ctx.accepted();
    }
}

module.exports = OnlineCourseMediaHandler;
