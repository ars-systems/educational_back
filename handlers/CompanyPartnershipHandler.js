'use strict';

const _get = require('lodash/get');
const _map = require('lodash/map');
const _take = require('lodash/take');
const _extend = require('lodash/extend');
const _without = require('lodash/without');
const _isEmpty = require('lodash/isEmpty');
const _template = require('lodash/template');
const _includes = require('lodash/includes');
const _difference = require('lodash/difference');

const {
    User,
    Company,
    CompanyConnectPartnership,
    CompanyPartnership,
    generateSearchQuery,
    sequelize
} = require('./../data/models');
const { CompanyConnectStatusType, ActivityLogType, ErrorMessages } = require('../constants');
const { Mailer } = require('../components/');
const config = require('./../config');

class CompanyPartnershipHandler {
    static async actionCreate(ctx) {
        const { user } = ctx.state;
        const { message = '' } = _get(ctx, 'request.body');

        const company = await user.getCompany();

        if (_isEmpty(company)) {
            return ctx.notFound(ErrorMessages.COMPANY_NOT_FOUND);
        }

        let ids = _take(
            _without(_get(ctx, 'request.body.ids'), company.id),
            config.get('params:validation:connect:max:count')
        );

        if (_isEmpty(ids)) {
            return ctx.badRequest(ErrorMessages.IDS_REQUIRED);
        }

        let invitedConnects = await CompanyConnectPartnership.findAll({
            where: {
                $or: [
                    { senderCompanyId: company.id, receiverCompanyId: { $in: ids } },
                    { receiverCompanyId: company.id, senderCompanyId: { $in: ids } }
                ],
                $and: {
                    $or: [
                        { statusId: CompanyConnectStatusType.PENDING },
                        {
                            '$companyPartnership.statusId$': {
                                $ne: CompanyConnectStatusType.DISCONNECTED
                            }
                        }
                    ]
                }
            },
            include: [{ model: CompanyPartnership, as: 'companyPartnership' }]
        });

        invitedConnects = _map(invitedConnects, connect =>
            connect.senderCompanyId === company.id ? connect.receiverCompanyId : connect.senderCompanyId
        );

        let differentIds = _difference(ids, invitedConnects);

        if (_isEmpty(differentIds)) {
            return ctx.badRequest(ErrorMessages.INVALID_IDS);
        }

        await sequelize.transaction(async transaction => {
            await CompanyConnectPartnership.destroy(
                {
                    where: {
                        $or: [
                            { senderCompanyId: company.id, receiverCompanyId: { $in: differentIds } },
                            { receiverCompanyId: company.id, senderCompanyId: { $in: differentIds } }
                        ]
                    }
                },
                { transaction }
            );

            await CompanyConnectPartnership.bulkCreate(
                _map(differentIds, id => ({
                    senderCompanyId: company.id,
                    receiverCompanyId: id
                })),
                { transaction }
            );
        });

        let companies = await Company.findAll({
            where: { id: ids },
            attributes: ['userId', 'name'],
            include: [{ model: User, as: 'users', attributes: ['email'] }]
        });

        const template = _template(config.get('deeplink:url'));

        const profile = await user.getProfile();

        const data = {
            company: {
                id: company.id,
                name: company.name
            },
            sender: _extend(await user.fields({}, user), { profile: await profile.fields() }),
            typeId: ActivityLogType.COMPANY_NEW_PARTNERSHIP
        };

        if (!_isEmpty(companies)) {
            for (let item of companies) {
                Mailer.send(
                    _extend(
                        {
                            data: {
                                acceptLink: template({
                                    path: `auth/partnership/request?action=accept&companyId=${company.id}`
                                }),
                                declineLink: template({
                                    path: `auth/partnership/request?action=reject&companyId=${company.id}`
                                }),
                                receiver: item.name,
                                sender: company.name,
                                message
                            },
                            to: [item.users.email]
                        },
                        config.get('params:emailTemplates:companyPartnershipInvitation')
                    )
                ).catch(console.error);

                global.io.to(_get(item, 'userId')).emit('partnership:new', data);
            }
        }

        return ctx.accepted();
    }

    static async actionAutoComplete(ctx) {
        let { q = '' } = ctx.query;

        if (_isEmpty(await ctx.state.user.getCompany())) {
            return ctx.notFound(ErrorMessages.COMPANY_NOT_FOUND);
        }

        return ctx.ok({
            data: await Company.findAll({
                where: {
                    ...(!_isEmpty(q) ? generateSearchQuery(q, ['name']) : {})
                },
                attributes: [['name', 'text'], 'logo', 'id'],
                limit: 5
            })
        });
    }

    static async actionPending(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { type = 'sent', q = '' } = ctx.query;
        const { user } = ctx.state;

        if (!_includes(['sent', 'received'], type)) {
            return ctx.badRequest(ErrorMessages.INVALID_TYPE);
        }

        const company = await user.getCompany();

        if (_isEmpty(company)) {
            return ctx.notFound(ErrorMessages.COMPANY_NOT_FOUND);
        }

        const queryString = !_isEmpty(q) ? generateSearchQuery(q, 'name') : {};

        const { rows: companies, count: total } = await Company.scope({
            method: [`${type}CompanyConnects`, company.id, CompanyConnectStatusType.PENDING]
        }).findAndCountAll({
            where: {
                ...queryString
            },
            limit,
            offset
        });

        return ctx.ok({
            data: companies,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionUpdate(ctx) {
        let { id, status } = ctx.request.body;
        const { user } = ctx.state;

        if (!_includes(['accept', 'reject'], status)) {
            return ctx.badRequest(ErrorMessages.MISSING_ARGUMENTS);
        }

        const company = await user.getCompany();

        if (_isEmpty(company)) {
            return ctx.notFound(ErrorMessages.COMPANY_NOT_FOUND);
        }

        const companyPartnership = await CompanyConnectPartnership.findOne({
            where: {
                $or: [
                    { senderCompanyId: company.id, receiverCompanyId: id },
                    { receiverCompanyId: company.id, senderCompanyId: id }
                ]
            }
        });

        if (_isEmpty(companyPartnership)) {
            return ctx.notFound();
        } else if (companyPartnership.statusId !== CompanyConnectStatusType.PENDING) {
            return ctx.forbidden();
        }

        if (status === 'accept') {
            if (companyPartnership.senderCompanyId === company.id) {
                return ctx.forbidden();
            }

            await sequelize.transaction(async transaction => {
                await companyPartnership.update({ statusId: CompanyConnectStatusType.ACCEPTED }, { transaction });

                await CompanyPartnership.create(
                    {
                        companyId: company.id,
                        connectPartnershipId: companyPartnership.id
                    },
                    { transaction }
                );
            });

            const senderCompany = await Company.findById(id, { attributes: ['userId'] });

            const profile = await user.getProfile();

            global.io.to(_get(senderCompany, 'userId')).emit('partnership:accepted', {
                sender: _extend(await user.fields({}, user), { profile: await profile.fields() }),
                typeId: ActivityLogType.COMPANY_ACCEPT_PARTNERSHIP
            });
        } else {
            await companyPartnership.destroy();
        }

        return ctx.accepted();
    }

    static async actionCompanies(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        let { q = '' } = ctx.query;
        const { user } = ctx.state;

        const company = await user.getCompany();

        if (_isEmpty(company)) {
            return ctx.notFound(ErrorMessages.COMPANY_NOT_FOUND);
        }

        let queryString = !_isEmpty(q) ? generateSearchQuery(q, ['name', 'keywords']) : {};

        let { rows: companies, count: total } = await Company.findAndCountAll({
            where: {
                id: {
                    $ne: company.id
                },
                ...queryString
            },
            attributes: ['id', 'name', 'logo', 'email'],
            limit,
            offset
        });

        return ctx.ok({
            data: companies,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { type = 'connects', sort = 'name', q = '' } = ctx.query;
        const { user } = ctx.state;

        if (!_includes(['disconnects', 'connects'], type) || !_includes(['name', 'date'], sort)) {
            return ctx.badRequest(ErrorMessages.INVALID_TYPE);
        }

        const company = await user.getCompany();

        if (_isEmpty(Company)) {
            return ctx.notFound(ErrorMessages.COMPANY_NOT_FOUND);
        }

        const queryString = !_isEmpty(q) ? generateSearchQuery(q, ['name', 'keywords']) : {};

        const { rows: companies, count: total } = await Company.scope({
            method: [type, company.id, queryString]
        }).findAndCountAll({
            limit,
            offset,
            order: sequelize.literal(sort === 'date' ? `"partnership.createdAt" desc` : `"name" asc`)
        });

        return ctx.ok({
            data: companies,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionDisconnect(ctx) {
        const { id: companyId } = _get(ctx, 'params');
        const { user } = ctx.state;

        const company = await user.getCompany();

        if (_isEmpty(company)) {
            return ctx.notFound(ErrorMessages.COMPANY_NOT_FOUND);
        }

        if (companyId === company.id) {
            return ctx.forbidden();
        }

        let companyPartnership = await CompanyConnectPartnership.findOne({
            where: {
                $and: [
                    {
                        $or: [
                            {
                                receiverCompanyId: companyId,
                                senderCompanyId: company.id
                            },
                            {
                                senderCompanyId: companyId,
                                receiverCompanyId: company.id
                            }
                        ],
                        statusId: CompanyConnectStatusType.ACCEPTED
                    }
                ]
            }
        });

        if (_isEmpty(companyPartnership)) {
            return ctx.notFound();
        }

        await sequelize.transaction(async transaction => {
            await CompanyPartnership.destroy({
                where: { connectPartnershipId: companyPartnership.id },
                transaction
            });

            companyPartnership.statusId = CompanyConnectStatusType.DISCONNECTED;
            await companyPartnership.save({ transaction });
        });

        return ctx.noContent();
    }
}

module.exports = CompanyPartnershipHandler;
