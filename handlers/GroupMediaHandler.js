'use strict';

const fs = require('fs');
const _get = require('lodash/get');
const _has = require('lodash/has');
const _uniq = require('lodash/uniq');
const _pickBy = require('lodash/pickBy');
const _filter = require('lodash/filter');
const _extend = require('lodash/extend');
const _isEmpty = require('lodash/isEmpty');
const _isNumber = require('lodash/isNumber');
const _castArray = require('lodash/castArray');

const { UserConnectGroup, GroupAttachment, GroupMedia, GroupLink, Media, sequelize } = require('./../data/models');
const { ErrorMessages, GroupMediaType, MediaType } = require('./../constants');

class GroupMediaHandler {
    static async actionCreate(ctx) {
        const { groupId } = ctx.params;
        const { user } = ctx.state;
        let { body } = ctx.request;

        body = _pickBy(body, v => _isNumber(v) || !_isEmpty(v));

        if (!(await UserConnectGroup.count({ where: { id: groupId, userId: user.id } }))) {
            return ctx.notFound(ErrorMessages.INVALID_CREDENTIALS);
        }

        let attachment;
        let mediaAttachment = [];
        const uploadedFiles = [];
        const hasLink = !_isEmpty(_get(body, 'url'));
        const hasBody = !_isEmpty(_get(body, 'text')) || hasLink;
        const hasAttachment = !_isEmpty(_get(ctx, 'request.files'));
        const hasMediaId = !_isEmpty(_get(body, 'mediaId'));

        if (!hasBody && !hasAttachment && !hasMediaId) {
            return ctx.badRequest();
        }

        if (hasAttachment) {
            attachment = ctx.request.files.attachment;

            if (_isEmpty(attachment)) {
                return ctx.badRequest();
            }
        }

        if (hasMediaId) {
            let mediaId = _uniq(_filter(_castArray(body.mediaId), id => Number(id)));

            if (!_isEmpty(mediaId)) {
                mediaAttachment = await Media.findOne({
                    where: { id: mediaId, userId: user.id },
                    attributes: ['data']
                });

                if (_isEmpty(mediaAttachment)) {
                    return ctx.notFound(ErrorMessages.MEDIA_NOT_FOUND);
                }
            }
        }

        user.profile = await user.getProfile();

        const groupMedia = await sequelize.transaction(async transaction => {
            const model = new GroupMedia({
                groupId,
                text: body.text,
                userId: user.id
            });

            await model.save({ transaction });

            _extend(model, {
                attachment: []
            });

            if (!_isEmpty(mediaAttachment)) {
                model.attachment = await GroupAttachment.create(
                    {
                        key: mediaAttachment.data,
                        groupId: model.id
                    },
                    { transaction, validate: true }
                );
            }

            if (hasBody && !hasAttachment && !hasMediaId) {
                if (hasLink) {
                    const linkModel = new GroupLink({
                        groupId: model.id,
                        url: _get(body, 'url')
                    });

                    model.typeId = GroupMediaType.LINK;
                    _extend(model, {
                        link: await linkModel.save({ transaction })
                    });
                } else {
                    model.typeId = GroupMediaType.TEXT;
                }
            } else if (hasAttachment || hasMediaId) {
                model.typeId = GroupMediaType.FILE;
            }

            if (model.typeId !== GroupMediaType.NO_CONTENT) {
                await model.save({ transaction });
            }

            if (hasAttachment && !_isEmpty(attachment)) {
                const attachmentKey = `group/attachment/${groupId}/${_get(attachment, 'key')}`;
                fs.mkdirSync(`media/group/attachment/${groupId}`, { recursive: true }, err => {
                    if (err) throw err;
                });
                fs.writeFileSync(`media/${attachmentKey}`, fs.readFileSync(attachment.path));

                await Media.create(
                    {
                        userId: user.id,
                        data: attachmentKey,
                        typeId: MediaType.GROUP_MEDIA_ATTACHMENT,
                        description: _get(ctx, 'request.description')
                    },
                    { transaction }
                );
                const attachmentModel = new GroupAttachment({
                    groupId: model.id,
                    key: attachmentKey
                });

                model.attachment.push(await attachmentModel.save({ transaction }));
                uploadedFiles.push({
                    source: attachment,
                    key: attachmentModel.key
                });

                if (_isEmpty(model.attachment)) {
                    throw new Error();
                }
            }

            if (!_isEmpty(uploadedFiles)) {
                // Todo aws save
            }

            return _extend(model, {
                user
            });
        });

        return ctx.created({ data: groupMedia });
    }

    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        let { filter, id } = ctx.request.query;

        const filters = GroupMedia.filters(id);

        if (!_isEmpty(filter)) {
            if (!_has(filters, filter)) {
                return ctx.badRequest();
            }

            filter = _get(filters, filter);
        } else {
            filter = {};
        }

        const { rows: posts, count: total } = await GroupMedia.scope({
            method: ['expand', ctx.state.user.id, filter]
        }).findAndCountAll({
            order: [['createdAt', 'DESC']],
            limit,
            offset
        });

        return ctx.ok({
            data: posts,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionView(ctx) {
        const groupMedia = await GroupMedia.scope({
            method: ['expand', ctx.state.user.id]
        }).findByPk(ctx.params.groupMediaId);

        return ctx.ok({ data: groupMedia || [] });
    }

    static async actionUpdate(ctx) {
        let { body } = ctx.request;
        let { user } = ctx.state;

        const groupMedia = await GroupMedia.findOne({
            where: {
                id: ctx.params.groupMediaId,
                userId: user.id
            }
        });

        if (_isEmpty(groupMedia)) {
            return ctx.notFound(ErrorMessages.GROUP_NOT_FOUND);
        }

        body = _pickBy(body, v => _isNumber(v) || !_isEmpty(v));

        let mediaAttachment = [];
        const hasLink = !_isEmpty(_get(body, 'url'));
        const hasBody = !_isEmpty(_get(body, 'text')) || hasLink;
        const hasAttachment = !_isEmpty(_get(body, 'request.files'));
        const hasMediaId = !_isEmpty(_get(body, 'mediaId'));

        if (!hasBody && !hasAttachment && !hasMediaId) {
            return ctx.badRequest();
        }

        let attachments = [];
        if (hasAttachment) {
            attachments = _get(body, 'attachment.files');

            if (_isEmpty(attachments)) {
                return ctx.badRequest();
            }
        }

        if (hasMediaId) {
            let mediaId = _uniq(_filter(_castArray(body.mediaId), id => Number(id)));

            if (!_isEmpty(mediaId)) {
                mediaAttachment = await Media.findAll({
                    where: { id: mediaId, userId: user.id },
                    attributes: ['key']
                });

                if (_isEmpty(mediaAttachment)) {
                    return ctx.notFound();
                }
            }
        }

        user.profile = await user.getProfile();

        const data = await sequelize.transaction(async transaction => {
            groupMedia.text = _get(body, 'text');

            // remove post attachments
            if (groupMedia.typeId === GroupMediaType.FILE) {
                const attachmentId = _castArray(_get(body, 'attachment.deletedId', []));

                if (!_isEmpty(attachmentId)) {
                    await GroupAttachment.destroy(
                        { where: { id: attachmentId, groupId: groupMedia.id } },
                        { transaction }
                    );
                }
            }

            // remove post Link
            if (groupMedia.typeId === GroupMediaType.LINK) {
                await GroupLink.destroy({ where: { groupId: groupMedia.id } }, { transaction });
            }

            // add attachments from media
            if (!_isEmpty(mediaAttachment)) {
                groupMedia.attachment = await GroupAttachment.create(
                    {
                        key: mediaAttachment.key,
                        postId: groupMedia.id
                    },
                    { transaction }
                );
            }

            // add attachments
            if (!_isEmpty(attachments)) {
                const attachmentKey = `media/group/attachment/${groupMedia.id}/${_get(attachments, 'key')}`;
                fs.mkdirSync(`media/group/attachment/${groupMedia.id}`, { recursive: true }, err => {
                    if (err) throw err;
                });
                fs.writeFileSync(`media/${attachmentKey}`, fs.readFileSync(attachments.path));

                await Media.create(
                    {
                        userId: user.id,
                        data: attachmentKey,
                        typeId: MediaType.GROUP_MEDIA_ATTACHMENT
                    },
                    { transaction }
                );

                const attachmentModel = new GroupAttachment({
                    groupId: groupMedia.id,
                    key: attachmentKey
                });

                groupMedia.attachment.push(await attachmentModel.save({ transaction }));

                if (_isEmpty(groupMedia.attachment)) {
                    throw new Error();
                }
            }

            if (groupMedia.typeId !== GroupMediaType.NO_CONTENT) {
                await groupMedia.save({ transaction });
            }

            return _extend(groupMedia, {
                user
            });
        });

        return ctx.ok({ data });
    }

    static async actionDelete(ctx) {
        const { id: userId } = ctx.state.user;

        const media = await GroupMedia.findOne({
            where: {
                id: ctx.params.groupMediaId,
                $or: [{ userId: userId }, { '$group.userId$': userId }]
            },
            include: ['group']
        });

        if (_isEmpty(media)) {
            return ctx.notFound(ErrorMessages.INVALID_CREDENTIALS);
        }

        await media.destroy();

        return ctx.accepted();
    }
}

module.exports = GroupMediaHandler;
