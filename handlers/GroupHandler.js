'use strict';

const fs = require('fs');
const _get = require('lodash/get');
const _pick = require('lodash/pick');
const _extend = require('lodash/extend');
const _isEmpty = require('lodash/isEmpty');
const _includes = require('lodash/includes');

const { User, Group, GroupMedia, UserConnectGroup, generateSearchQuery } = require('./../data/models');
const { ErrorMessages } = require('./../constants');
const config = require('./../config');

class GroupHandler {
    static async actionCreate(ctx) {
        const { extensions, size } = config.get('params:validation:files:image');
        let media = _get(ctx.request, 'files.media');
        const { typeId } = ctx.query;
        const { user } = ctx.state;

        if (_isEmpty(media)) {
            return ctx.badRequest(ErrorMessages.FILE_REQUIRED);
        } else if (!_includes(extensions, media.ext)) {
            return ctx.badRequest(ErrorMessages.INVALID_FILE_TYPE);
        } else if (media.size > size) {
            return ctx.badRequest(ErrorMessages.INVALID_FILE_SIZE);
        }

        const mediaKey = `group/${user.id}/${_get(media, 'key')}`;
        fs.mkdirSync(`media/group/${user.id}`, { recursive: true }, err => {
            if (err) throw err;
        });
        fs.writeFileSync(`media/${mediaKey}`, fs.readFileSync(media.path));

        const groupMedia = await user.createGroup(
            _extend(
                { ..._pick(ctx.request.body, ['name', 'description', 'availability']) },
                {
                    tags: JSON.parse(ctx.request.body.tags),
                    image: mediaKey,
                    meta: media,
                    typeId
                }
            )
        );

        await UserConnectGroup.create({ groupId: groupMedia.id, userId: user.id });

        return ctx.created({ data: groupMedia });
    }

    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { userId = '', type = '', q = '' } = ctx.query;

        const query = !_isEmpty(q) ? generateSearchQuery(q, ['name', 'tags']) : {};

        const where = type ? { ...query, typeId: Number(type) } : query;

        if (!_isEmpty(userId)) {
            _extend(where, { userId });
        }

        const { rows: groups, count: total } = await Group.findAndCountAll({
            where,
            include: [
                {
                    model: User,
                    as: 'groupAdmin',
                    include: ['profile']
                }
            ],
            order: [['name', 'ASC'], ['createdAt', 'DESC']],
            offset,
            limit
        });

        return ctx.ok({
            data: groups,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionView(ctx) {
        let { userId } = ctx.query;
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        let group = await Group.findOne({
            where: {
                id: ctx.params.groupId
            },
            include: [
                {
                    model: User,
                    as: 'groupAdmin',
                    include: ['profile']
                }
            ]
        });
        userId = userId || 0;
        if (_isEmpty(group)) {
            return ctx.notFound(ErrorMessages.GROUP_NOT_FOUND);
        }
        const { rows: media, count: total } = await GroupMedia.scope({
            method: ['expand', userId]
        }).findAndCountAll({
            where: { groupId: group.id },
            order: [['createdAt', 'desc']],
            offset,
            limit
        });

        group.media = media;
        group._meta = {
            total,
            currentPage,
            pageCount: Math.ceil(total / limit)
        };
        if (group.availability === 'false') {
            if (
                !(await UserConnectGroup.count({
                    where: { id: group.id, userId: userId }
                }))
            ) {
                group.media = [];
                group._meta = {};
            }
        }
        let recommendedGroups = await Group.scope({
            method: ['count']
        }).findAll({
            where: {
                typeId: group.typeId,
                id: {
                    $and: [
                        { $ne: group.id },
                        {
                            $notIn: UserConnectGroup.generateNestedQuery({
                                attributes: ['id'],
                                where: {
                                    userId
                                }
                            })
                        }
                    ]
                }
            },
            limit: 4,
            group: [['id']],
            order: [['createdAt', 'desc']]
        });

        recommendedGroups = !_isEmpty(recommendedGroups)
            ? recommendedGroups
            : await Group.scope({
                  method: ['count']
              }).findAll({
                  where: {
                      id: {
                          $and: [
                              { $ne: group.id },
                              {
                                  $notIn: UserConnectGroup.generateNestedQuery({
                                      attributes: ['id'],
                                      where: {
                                          userId
                                      }
                                  })
                              }
                          ]
                      }
                  },
                  group: [['id']],
                  limit: 4,
                  order: [['createdAt', 'desc']]
              });

        return ctx.ok({
            data: group,
            recommendedGroups,
            connected: !!(await UserConnectGroup.count({
                where: { id: group.id, userId }
            }))
        });
    }

    static async actionDelete(ctx) {
        return (await Group.destroy({
            where: {
                id: ctx.params.groupId,
                userId: ctx.state.user.id
            }
        }))
            ? ctx.accepted()
            : ctx.notFound();
    }
}

module.exports = GroupHandler;
