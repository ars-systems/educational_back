'use strict';

const _get = require('lodash/get');
const _pick = require('lodash/pick');
const _extend = require('lodash/extend');
const _isEmpty = require('lodash/isEmpty');
const _includes = require('lodash/includes');

const {
    Company,
    CompanyConnect,
    CompanyLocation,
    CompanyUserConnect,
    UserWork,
    sequelize
} = require('../data/models');
const { AccountType, UserConnectStatusType, CompanyRole, ErrorMessages } = require('../constants');

const config = require('../config');

class CompanyHandler {
    static async actionCreate(ctx) {
        const { body } = ctx.request;
        const { user } = ctx.state;

        if (
            (await Company.count({ where: { userId: user.id } })) ||
            (await UserWork.count({ where: { userId: user.id } }))
        ) {
            return ctx.forbidden(ErrorMessages.ALREADY_COMPANY_MEMBER);
        }

        let companyData = _pick(body, ['ein', 'name', 'email', 'websiteUrl', 'companyType']);

        let accountType = AccountType.COMPANY;

        const company = await sequelize.transaction(async transaction => {
            await CompanyConnect.destroy(
                { where: { receiverId: user.id, statusId: UserConnectStatusType.PENDING } },
                { transaction }
            );

            const model = await Company.create(_extend(companyData, { userId: user.id }), { transaction });

            await user.createWork({ companyId: model.id }, { transaction });

            await CompanyUserConnect.create(
                { userId: user.id, companyId: model.id, roleId: CompanyRole.ADMIN },
                { transaction }
            );

            model.location = await CompanyLocation.create(
                _extend(
                    _pick(body, ['firstAddress', 'secondAddress', 'country', 'zipCode', 'state', 'city']),
                    {
                        companyId: model.id
                    },
                    { transaction }
                )
            );

            user.accountTypeId = accountType;
            await user.save({ transaction });

            return model;
        });

        return ctx.created({ data: company });
    }

    static async actionUpdate(ctx) {
        const { body } = ctx.request;
        const { user } = ctx.state;

        const company = await user.getCompany({
            include: [{ model: CompanyLocation, as: 'location' }]
        });

        if (_isEmpty(company)) {
            return ctx.notFound(ErrorMessages.COMPANY_NOT_FOUND);
        }

        let accountType = AccountType.COMPANY;

        let companyData = _pick(body, ['name', 'email', 'websiteUrl']);

        const companyModel = await sequelize.transaction(async transaction => {
            const model = _extend(company, companyData);

            await model.save({ transaction });

            const companyLocation = _extend(
                company.location,
                _pick(body, ['firstAddress', 'secondAddress', 'country', 'zipCode', 'state', 'city']),
                {
                    companyId: model.id
                }
            );

            model.location = await companyLocation.save({ transaction });
            user.accountTypeId = accountType;

            await user.save({ transaction });

            return model;
        });

        return ctx.ok({
            data: companyModel
        });
    }

    static async actionIndex(ctx) {
        return ctx.ok({
            data:
                (await Company.scope({
                    method: ['expand', ctx.state.user.id]
                }).findOne()) || {}
        });
    }

    static async actionDelete(ctx) {
        const { user } = ctx.state;

        const company = await sequelize.transaction(async transaction => {
            const result = await Company.destroy(
                {
                    where: { userId: user.id }
                },
                transaction
            );

            user.accountTypeId = AccountType.LECTURER;
            await user.save({ transaction });

            return result;
        });

        if (!company) {
            return ctx.notFound(ErrorMessages.COMPANY_NOT_FOUND);
        }

        return ctx.noContent();
    }

    static async actionUploadLogo(ctx) {
        const { extensions } = config.get('params:files:logos:allowed');
        const logo = _get(ctx, 'request.files.logo');

        const company = await ctx.state.user.getCompany();

        if (_isEmpty(company)) {
            return ctx.notFound(ErrorMessages.COMPANY_NOT_FOUND);
        }

        if (_isEmpty(logo)) {
            return ctx.badRequest(ErrorMessages.FILE_REQUIRED);
        } else if (!_includes(extensions, _get(logo, 'ext'))) {
            return ctx.badRequest(ErrorMessages.INVALID_FILE_TYPE);
        }

        const oldLogo = company.logo;

        company.logo = logo.key;
        await company.save();

        // TODO save logo
        if (!_isEmpty(oldLogo)) {
            // TODO remove logo
        }

        return ctx.ok({ data: company.logo });
    }

    static async actionRemoveLogo(ctx) {
        const company = await ctx.state.user.getCompany();

        if (_isEmpty(company)) {
            return ctx.notFound(ErrorMessages.COMPANY_NOT_FOUND);
        }

        const logo = _get(company, 'logo');

        if (!_isEmpty(logo)) {
            await company.update({ logo: null });

            // TODO remove logo
        }

        return ctx.noContent();
    }
}

module.exports = CompanyHandler;
