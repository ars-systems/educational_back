'use strict';

const _extend = require('lodash/extend');
const _isEmpty = require('lodash/isEmpty');

const { ErrorMessages, OnlineCourseConnectionStatus, OnlineCourseConnectionType } = require('./../constants');
const { OnlineCourse, User, UserOnlineCourse, UserConnectOnlineCourse } = require('./../data/models');
const { Mailer } = require('./../components');
const config = require('./../config');

class UserOnlineCourseHandler {
    static async actionInvite(ctx) {
        const { id: userId } = ctx.state.user;
        const { requestedUserId, courseId } = ctx.params;

        if (!(await User.count({ where: { id: requestedUserId } }))) {
            return ctx.badRequest(ErrorMessages.USER_NOT_FOUND);
        } else if (!(await OnlineCourse.count({ where: { id: courseId } }))) {
            return ctx.badRequest(ErrorMessages.COURSE_NOT_FOUND);
        } else if (userId === requestedUserId) {
            return ctx.badRequest(ErrorMessages.INVALID_RECEIVER_ID);
        } else if (!(await OnlineCourse.count({ where: { userId } }))) {
            return ctx.badRequest(ErrorMessages.ONLY_ADMIN_CAN_INVITE);
        } else if (await UserOnlineCourse.count({ where: { userId: requestedUserId, courseId } })) {
            return ctx.badRequest(ErrorMessages.ARE_ALREADY_IN_THIS_COURSE);
        }

        return ctx.ok({
            data: await UserOnlineCourse.create({
                courseId,
                userId: requestedUserId,
                type: OnlineCourseConnectionType.INVITE
            })
        });
    }

    static async actionRequest(ctx) {
        const { user } = ctx.state;
        const { courseId } = ctx.params;

        const course = await OnlineCourse.findByPk(courseId);

        if (_isEmpty(course)) {
            return ctx.badRequest(ErrorMessages.COURSE_NOT_FOUND);
        } else if (await OnlineCourse.count({ where: { userId: user.id } })) {
            return ctx.badRequest(ErrorMessages.ARE_ALREADY_IN_THIS_COURSE);
        } else if (
            await OnlineCourse.count({ where: { userId: user.id, status: OnlineCourseConnectionStatus.DECLINED } })
        ) {
            return ctx.badRequest(ErrorMessages.YOU_ARE_DECLINED);
        }
        user.profile = await user.getProfile();

        if (course.availability === true) {
            Mailer.send(
                _extend(
                    {
                        data: {
                            firstName: user.profile.firstName,
                            count: (await UserConnectOnlineCourse.count({ where: { courseId } })) + 1
                        },
                        to: [user.email]
                    },
                    config.get('params:emailTemplates:courseWelcome')
                )
            ).catch(console.error);

            return ctx.ok({
                data: await UserConnectOnlineCourse.create({
                    userId: user.id,
                    courseId
                })
            });
        }

        return ctx.ok({
            data: await UserOnlineCourse.create({
                userId: user.id,
                courseId,
                type: OnlineCourseConnectionType.REQUEST
            })
        });
    }

    static async actionAcceptInvitation(ctx) {
        const { user } = ctx.state;
        const { courseId } = ctx.params;
        const status = ctx.query.status
            ? OnlineCourseConnectionStatus.ACCEPTED
            : OnlineCourseConnectionStatus.DECLINED;

        const invitation = await UserOnlineCourse.findOne({
            where: {
                userId: user.id,
                courseId,
                type: OnlineCourseConnectionType.INVITE,
                status: OnlineCourseConnectionStatus.PENDING
            }
        });

        if (_isEmpty(invitation)) {
            return ctx.notFound(ErrorMessages.INVITATION_NOT_FOUND);
        }

        if (await UserConnectOnlineCourse.count({ where: { courseId, userId: user.id } })) {
            return ctx.badRequest(ErrorMessages.ARE_ALREADY_IN_THIS_COURSE);
        }

        invitation.status = status;
        user.profile = await user.getProfile();

        Mailer.send(
            _extend(
                {
                    data: {
                        firstName: user.profile.firstName,
                        count: (await UserConnectOnlineCourse.count({ where: { courseId } })) + 1
                    },
                    to: [user.email]
                },
                config.get('params:emailTemplates:courseWelcome')
            )
        ).catch(console.error);

        await UserConnectOnlineCourse.create({
            userId: user.id,
            courseId: invitation.courseId
        });

        return ctx.ok({ data: await invitation.save() });
    }

    static async actionAcceptRequest(ctx) {
        const { id: userId } = ctx.state.user;
        const { courseId, requestedUserId } = ctx.params;
        const status = ctx.query.status
            ? OnlineCourseConnectionStatus.ACCEPTED
            : OnlineCourseConnectionStatus.DECLINED;

        const course = await OnlineCourse.findOne({ where: { userId, id: courseId } });

        if (_isEmpty(course)) {
            return ctx.badRequest(ErrorMessages.COURSE_NOT_FOUND_OR_YOU_HAVE_NOT_ACCESS);
        }

        const request = await UserOnlineCourse.findOne({
            where: {
                courseId,
                userId: requestedUserId,
                type: OnlineCourseConnectionType.REQUEST,
                status: OnlineCourseConnectionStatus.PENDING
            }
        });

        if (_isEmpty(request)) {
            return ctx.notFound(ErrorMessages.REQUEST_NOT_FOUND);
        }

        if (await UserConnectOnlineCourse.count({ where: { courseId, userId: requestedUserId } })) {
            return ctx.badRequest(ErrorMessages.ARE_ALREADY_IN_THIS_COURSE);
        }

        request.status = status;

        await UserConnectOnlineCourse.create({
            courseId,
            userId: requestedUserId
        });

        return ctx.ok({ data: await request.save() });
    }
}

module.exports = UserOnlineCourseHandler;
