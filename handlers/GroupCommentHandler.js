'use strict';

const _get = require('lodash/get');
const _has = require('lodash/has');
const _pick = require('lodash/pick');
const _extend = require('lodash/extend');
const _pickBy = require('lodash/pickBy');
const _isEmpty = require('lodash/isEmpty');

const {
    GroupMedia,
    GroupComment,
    GroupCommentLink,
    UserConnectGroup,
    GroupCommentAttachment,
    sequelize
} = require('../data/models');
const { ErrorMessages } = require('./../constants');

class GroupCommentHandler {
    static async actionCreate(ctx) {
        let attachment = _get(ctx, 'request.files.attachment');
        let { body } = ctx.request;
        let { user } = ctx.state;

        body = _pickBy(body, v => !_isEmpty(v));

        const isLink = _has(body, 'link.url');
        const hasBody = _has(body, 'text') || isLink;
        const hasAttachment = !_isEmpty(attachment);

        if (!hasBody && !hasAttachment) {
            return ctx.badRequest();
        }

        const groupMedia = await GroupMedia.findOne({
            where: { id: ctx.params.mediaGroupId }
        });

        if (
            !(await UserConnectGroup.count({
                where: { groupId: groupMedia.groupId, userId: user.id }
            }))
        ) {
            return ctx.notFound(ErrorMessages.INVALID_CREDENTIALS);
        }

        user.profile = await user.getProfile();

        const comment = await sequelize.transaction(async transaction => {
            const model = await groupMedia.createComment(
                {
                    userId: user.id,
                    text: body.text
                },
                { transaction }
            );

            if (hasBody && isLink) {
                _extend(model, {
                    link: await GroupCommentLink.create(
                        {
                            groupCommentId: model.id,
                            ..._pick(body.link, ['url', 'image', 'title', 'description'])
                        },
                        { transaction }
                    )
                });
            } else if (!_isEmpty(attachment)) {
                const documentModel = new GroupCommentAttachment({
                    groupCommentId: model.id,
                    meta: attachment
                });

                model.attachment = await documentModel.save({ transaction });
            }

            return _extend(model, {
                user,
                groupMedia: {
                    userId: groupMedia.userId
                }
            });
        });

        return ctx.created({ data: comment });
    }

    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { user } = ctx.state;

        const { rows: comments, count: total } = await GroupComment.scope({
            method: ['expand', user.id, ctx.params.mediaGroupId]
        }).findAndCountAll({
            limit,
            offset
        });

        return ctx.ok({
            data: comments,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionDelete(ctx) {
        const { id: userId } = ctx.state.user;

        const comment = await GroupComment.findOne({
            where: {
                id: ctx.params.mediaGroupId,
                $or: [{ userId: userId }, { '$group.userId$': userId }, { '$groupMedia.userId$': userId }]
            },
            include: ['group', 'groupMedia']
        });

        if (_isEmpty(comment)) {
            return ctx.notFound(ErrorMessages.INVALID_CREDENTIALS);
        }

        await comment.destroy();

        return ctx.accepted();
    }
}

module.exports = GroupCommentHandler;
