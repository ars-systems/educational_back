'use strict';

const _get = require('lodash/get');
const _has = require('lodash/has');
const _pick = require('lodash/pick');
const _extend = require('lodash/extend');
const _pickBy = require('lodash/pickBy');
const _isEmpty = require('lodash/isEmpty');

const { Media, MediaComment, MediaCommentLink, MediaCommentAttachment, sequelize } = require('../data/models');
const { ErrorMessages } = require('./../constants');

class MediaCommentHandler {
    static async actionCreate(ctx) {
        let attachment = _get(ctx, 'request.files.attachment');
        let { body } = ctx.request;
        let { user } = ctx.state;

        body = _pickBy(body, v => !_isEmpty(v));

        const isLink = _has(body, 'link.url');
        const hasBody = _has(body, 'text') || isLink;
        const hasAttachment = !_isEmpty(attachment);

        if (!hasBody && !hasAttachment) {
            return ctx.badRequest();
        }

        const media = await Media.findOne({
            where: { id: ctx.params.mediaId }
        });

        user.profile = await user.getProfile();

        const comment = await sequelize.transaction(async transaction => {
            const model = await media.createComment(
                {
                    userId: user.id,
                    text: body.text
                },
                { transaction }
            );

            if (hasBody && isLink) {
                _extend(model, {
                    link: await MediaCommentLink.create(
                        {
                            mediaCommentId: model.id,
                            ..._pick(body.link, ['url', 'image', 'title', 'description'])
                        },
                        { transaction }
                    )
                });
            } else if (!_isEmpty(attachment)) {
                const documentModel = new MediaCommentAttachment({
                    mediaCommentId: model.id,
                    meta: attachment
                });

                model.attachment = await documentModel.save({ transaction });
            }

            return _extend(model, {
                user,
                media: {
                    userId: media.userId
                }
            });
        });

        return ctx.created({ data: comment });
    }

    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { user } = ctx.state;

        const { rows: comments, count: total } = await MediaComment.scope({
            method: ['expand', user.id, ctx.params.mediaId]
        }).findAndCountAll({
            limit,
            offset
        });

        return ctx.ok({
            data: comments,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionDelete(ctx) {
        const { id: userId } = ctx.state.user;

        const comment = await MediaComment.findOne({
            where: {
                id: ctx.params.mediaId,
                $or: [{ userId: userId }, { '$media.userId$': userId }]
            },
            include: ['media']
        });

        if (_isEmpty(comment)) {
            return ctx.notFound(ErrorMessages.INVALID_CREDENTIALS);
        }

        await comment.destroy();

        return ctx.accepted();
    }
}

module.exports = MediaCommentHandler;
