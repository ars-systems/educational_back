'use strict';

const _ = require('lodash');

const {
    MessageRecipient,
    ThreadRequest,
    ThreadUser,
    Thread,
    User,
    sequelize,
    generateSearchQuery
} = require('../data/models');
const { ACCEPTED, REJECTED } = require('../constants/ConnectStatus');
const { ErrorMessages, ConnectStatus } = require('../constants');
// const { Notification } = require('../services');

class ThreadHandler {
    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { id: userId } = ctx.state.user;
        const { q = '' } = ctx.query;
        const unSeen = false;

        const searchCondition = !_.isEmpty(q) ? generateSearchQuery(q) : {};

        const { rows: threads, count: total } = await Thread.scope({
            method: ['main', userId, unSeen, searchCondition]
        }).findAndCountAll({
            order: [['lastMessage', 'updatedAt', 'DESC']],
            offset,
            limit
        });

        await MessageRecipient.update(
            { isSeen: true },
            {
                where: {
                    userId,
                    isSeen: false,
                    threadId: {
                        $in: ThreadRequest.generateNestedQuery({
                            attributes: ['threadId'],
                            where: {
                                $or: [{ senderId: userId }, { receiverId: userId }],
                                statusId: { $ne: REJECTED }
                            }
                        })
                    }
                }
            }
        );

        return ctx.ok({
            threads,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionView(ctx) {
        const { id: userId } = ctx.state.user;
        const { threadId } = ctx.params;

        const thread = await Thread.scope({ method: ['available', userId] }).findByPk(threadId);

        return !_.isEmpty(thread) ? ctx.ok({ thread }) : ctx.notFound(ErrorMessages.THREAD_NOT_FOUND);
    }

    static async actionCreate(ctx) {
        const { id: senderId } = ctx.state.user;
        let { userId } = ctx.request.body;

        if (_.isEmpty(userId)) {
            return ctx.badRequest(ErrorMessages.IDS_REQUIRED);
        }

        const receiverId = userId;

        if (
            !(await User.scope({ method: ['expand'] }).count({
                where: { id: receiverId },
                raw: true
            }))
        ) {
            return ctx.notFound(ErrorMessages.USER_NOT_FOUND);
        }

        const threadRequest = await ThreadRequest.findOne({
            where: { $or: [{ senderId, receiverId }, { senderId: receiverId, receiverId: senderId }] }
        });

        if (!_.isEmpty(threadRequest)) {
            const { statusId, receiverId, threadId } = threadRequest;

            if (_.includes([REJECTED], statusId) && receiverId === senderId) {
                await threadRequest.update({ typeId: ACCEPTED });
            }

            return ctx.ok({
                thread: await Thread.scope({ method: ['available', senderId] }).findByPk(threadId)
            });
        }

        const thread = await sequelize.transaction(async transaction => {
            let thread = await Thread.create({}, { transaction });

            thread.request = await thread.createRequest({ senderId, receiverId }, { transaction });

            thread.users = await ThreadUser.bulkCreate(
                [
                    { userId: senderId, threadId: thread.id, seeFrom: new Date() },
                    { userId: receiverId, threadId: thread.id, seeFrom: new Date() }
                ],
                { validate: true, transaction }
            );

            return thread;
        });

        return ctx.created({ thread });
    }

    static async actionTyping(ctx) {
        let { isTyping } = ctx.request.body;
        let { threadId } = ctx.params;
        let { user } = ctx.state;

        if (!_.isBoolean(isTyping)) {
            return ctx.badRequest();
        }

        let threadUsers = await ThreadUser.findAll({
            attributes: ['userId'],
            where: {
                threadId
            }
        });

        let threadUser = _.find(threadUsers, v => {
            return v.userId === user.id;
        });

        if (_.isEmpty(threadUser)) {
            return ctx.notFound();
        }

        const profile = await user.getProfile();
        let data = {
            action: isTyping,
            sender: _.extend(await user.fields({}, user), { profile: await profile.fields() }),
            threadId
        };

        _.forEach(threadUsers, v => {
            global.io.to(v.userId).emit('message:typing', data);
        });

        return ctx.accepted();
    }

    static async actionDelete(ctx) {
        const { id: userId } = ctx.state.user;
        const { threadId } = ctx.params;

        if (!(await Thread.scope({ method: ['available', userId] }).count({ where: { id: threadId } }))) {
            return ctx.notFound(ErrorMessages.THREAD_NOT_FOUND_OR_YOU_HAVE_NOT_ACCESS);
        }
        const threadUsers = await ThreadRequest.findOne({
            where: { threadId, $or: { receiverId: userId, senderId: userId } }
        });

        await ThreadUser.update({ seeFrom: new Date() }, { where: { threadId, userId } });
        const receiverId = threadUsers.senderId === userId ? threadUsers.receiverId : threadUsers.senderId;
        await ThreadRequest.create({ statusId: ConnectStatus.REJECTED, threadId, senderId: userId, receiverId });
        await threadUsers.destroy();

        return ctx.accepted();
    }
}

module.exports = ThreadHandler;
