'use strict';

const _isEmpty = require('lodash/isEmpty');

const { User, UserConnect, generateSearchQuery } = require('./../data/models');
const { ErrorMessages } = require('./../constants');

class UserConnectHandler {
    static async actionCreate(ctx) {
        const { id: senderId } = ctx.state.user;
        const { receiverId } = ctx.params;

        if (senderId === receiverId) {
            return ctx.badRequest(ErrorMessages.INVALID_RECEIVER_ID);
        } else if (!(await User.count({ where: { id: receiverId } }))) {
            return ctx.badRequest(ErrorMessages.USER_NOT_FOUND);
        }

        const connects = await UserConnect.findOne({
            where: { senderId, receiverId }
        });

        if (!_isEmpty(connects)) {
            return ctx.badRequest(ErrorMessages.YOU_ARE_ALREADY_FOLLOWING_THIS_USER);
        }

        return ctx.ok({ data: await UserConnect.create({ senderId, receiverId }) });
    }

    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { q = '' } = ctx.query;

        const query = !_isEmpty(q) ? generateSearchQuery(q) : {};
        const { id: userId } = ctx.state.user;

        const { rows: connection, count: total } = await UserConnect.scope({
            method: ['connections', userId]
        }).findAndCountAll({
            where: query,
            offset,
            limit
        });

        return ctx.ok({
            data: connection,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionRemove(ctx) {
        return (await UserConnect.destroy({
            where: {
                senderId: ctx.state.user.id,
                receiverId: ctx.params.id
            }
        }))
            ? ctx.accepted()
            : ctx.notFound();
    }
}

module.exports = UserConnectHandler;
