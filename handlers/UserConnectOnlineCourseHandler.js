'use strict';

const _map = require('lodash/map');
const _get = require('lodash/get');
const _take = require('lodash/take');
const _isEmpty = require('lodash/isEmpty');
const _without = require('lodash/without');

const {
    User,
    UserProfile,
    OnlineCourse,
    UserOnlineCourse,
    UserConnectOnlineCourse,
    sequelize,
    generateSearchQuery
} = require('./../data/models');
const { ErrorMessages } = require('../constants');

class UserConnectOnlineCourseHandler {
    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { sort = 'asc', q = '' } = ctx.query;
        const { id: courseId } = ctx.params;
        const { user } = ctx.state;

        if (
            !(await OnlineCourse.count({
                where: {
                    id: courseId,
                    userId: user.id
                }
            }))
        ) {
            return ctx.notFound(ErrorMessages.COURSE_NOT_FOUND);
        }

        const queryString = !_isEmpty(q) ? generateSearchQuery(q) : {};

        const { rows: connectOnlineCourseUser, count: total } = await User.findAndCountAll({
            subQuery: false,
            include: [
                {
                    model: UserConnectOnlineCourse,
                    as: 'connectOnlineCourseUsers',
                    attributes: [],
                    where: {
                        courseId
                    }
                },
                {
                    model: UserProfile,
                    as: 'profile',
                    where: queryString
                }
            ],
            order: sequelize.literal(`"profile"."firstName" ${sort}`),
            limit,
            offset
        });

        return ctx.ok({
            data: connectOnlineCourseUser,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionNotInCourse(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { sort = 'asc', q = '' } = ctx.query;
        const { id: courseId } = ctx.params;
        const { user } = ctx.state;

        const course = await OnlineCourse.findOne({
            where: {
                id: courseId,
                userId: user.id
            }
        });

        if (_isEmpty(course)) {
            return ctx.notFound();
        }

        const queryString = !_isEmpty(q) ? generateSearchQuery(q) : {};

        const { rows: connectOnlineCourseUser, count: total } = await User.findAndCountAll({
            subQuery: false,
            include: [
                {
                    model: UserConnectOnlineCourse,
                    as: 'connectOnlineCourseUsers',
                    attributes: [],
                    where: {
                        courseId
                    }
                },
                {
                    model: UserProfile,
                    as: 'profile',
                    where: queryString
                }
            ],
            order: sequelize.literal(`"profile"."firstName" ${sort}`),
            limit,
            offset
        });

        return ctx.ok({
            data: connectOnlineCourseUser,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionSearch(ctx) {
        const { user } = ctx.state;
        const { id: courseId } = ctx.params;
        const { sort = 'asc', q = '' } = ctx.query;
        const { limit, offset, page: currentPage } = ctx.state.paginate;

        const course = await UserConnectOnlineCourse.findOne({
            where: {
                courseId,
                userId: user.id
            }
        });

        if (_isEmpty(course)) {
            return ctx.notFound();
        }

        const availableUsersLiteral = {
            id: {
                $notIn: UserConnectOnlineCourse.generateNestedQuery({
                    attributes: ['userId'],
                    where: {
                        courseId
                    }
                })
            }
        };

        const queryString = !_isEmpty(q) ? generateSearchQuery(q) : {};

        const { rows: users, count: total } = await User.scope({
            method: ['connects', user.id, availableUsersLiteral, queryString]
        }).findAndCountAll({
            subQuery: false,
            order: sequelize.literal(`"profile"."firstName" ${sort}`),
            limit,
            offset
        });

        return ctx.ok({
            data: users,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionCreate(ctx) {
        const { user } = ctx.state;
        const { id: courseId } = ctx.params;

        const course = await OnlineCourse.findOne({
            where: {
                id: courseId,
                userId: user.id
            }
        });

        if (_isEmpty(course)) {
            return ctx.notFound(ErrorMessages.COURSE_NOT_FOUND);
        }

        let ids = _take(_without(_get(ctx, 'request.body.ids'), user.id));

        if (_isEmpty(ids)) {
            return ctx.badRequest(ErrorMessages.IDS_REQUIRED);
        }

        await UserConnectOnlineCourse.bulkCreate(
            _map(ids, id => ({
                courseId,
                userId: id
            }))
        );

        return ctx.created();
    }

    static async actionDelete(ctx) {
        const { user } = ctx.state;
        const { courseId, userId } = ctx.params;

        const course = await OnlineCourse.findOne({
            where: {
                id: courseId,
                userId: user.id
            }
        });

        if (_isEmpty(course)) {
            return ctx.notFound(ErrorMessages.COURSE_NOT_FOUND);
        }

        const result = await sequelize.transaction(async transaction => {
            await UserOnlineCourse.destroy(
                {
                    where: {
                        userId,
                        courseId
                    }
                },
                { transaction }
            );

            await UserConnectOnlineCourse.destroy(
                {
                    where: {
                        userId,
                        courseId
                    }
                },
                { transaction }
            );
        });

        return result ? ctx.accepted() : ctx.notFound();
    }
}

module.exports = UserConnectOnlineCourseHandler;
