'use strict';

const _isEmpty = require('lodash/isEmpty');

const {
    User,
    Media,
    Group,
    UserRate,
    MediaRate,
    GroupRate,
    OnlineCourse,
    OnlineCourseRate
} = require('./../data/models');
const { ErrorMessages } = require('./../constants');

class RateHandler {
    static async actionRateMedia(ctx) {
        const { id: userId } = ctx.state.user;
        const { mediaId } = ctx.params;
        const { rate } = ctx.request.body;

        if (!(await Media.count({ where: { id: mediaId } }))) {
            return ctx.notFound(ErrorMessages.MEDIA_NOT_FOUND);
        }

        let mediaRate = await MediaRate.findOne({
            where: { userId, mediaId }
        });

        if (_isEmpty(mediaRate)) {
            mediaRate = MediaRate.create({ mediaId, userId, rate });
        } else {
            mediaRate.rate = rate;

            await mediaRate.save();
        }

        return ctx.ok({ data: mediaRate });
    }

    static async actionRateGroup(ctx) {
        const { id: userId } = ctx.state.user;
        const { groupId } = ctx.params;
        const { rate } = ctx.request.body;

        if (!(await Group.count({ where: { id: groupId } }))) {
            return ctx.notFound(ErrorMessages.GROUP_NOT_FOUND);
        }

        let groupRate = await GroupRate.findOne({
            where: { userId, groupId }
        });

        if (_isEmpty(groupRate)) {
            groupRate = GroupRate.create({ groupId, userId, rate });
        } else {
            groupRate.rate = rate;

            await groupRate.save();
        }

        return ctx.ok({ data: groupRate });
    }

    static async actionRateUser(ctx) {
        const { id: userId } = ctx.state.user;
        const { ratedUserId } = ctx.params;
        const { rate } = ctx.request.body;

        if (!(await User.count({ where: { id: ratedUserId } }))) {
            return ctx.notFound(ErrorMessages.USER_NOT_FOUND);
        }

        let userRate = await UserRate.findOne({
            where: { userId, ratedUserId }
        });

        if (_isEmpty(userRate)) {
            userRate = GroupRate.create({ ratedUserId, userId, rate });
        } else {
            userRate.rate = rate;

            await userRate.save();
        }

        return ctx.ok({ data: userRate });
    }

    static async actionRateOnlineCourse(ctx) {
        const { id: userId } = ctx.state.user;
        const { onlineCourseId } = ctx.params;
        const { rate } = ctx.request.body;

        if (!(await OnlineCourse.count({ where: { id: onlineCourseId } }))) {
            return ctx.notFound(ErrorMessages.ONLINE_COURSE_NOT_FOUND);
        }

        let onlineCourseRate = await OnlineCourseRate.findOne({
            where: { userId, onlineCourseId }
        });

        if (_isEmpty(onlineCourseRate)) {
            onlineCourseRate = GroupRate.create({ onlineCourseId, userId, rate });
        } else {
            onlineCourseRate.rate = rate;

            await onlineCourseRate.save();
        }

        return ctx.ok({ data: onlineCourseRate });
    }
}

module.exports = RateHandler;
