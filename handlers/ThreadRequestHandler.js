'use strict';

const _ = require('lodash');

const { MessageRecipient, ThreadRequest, Thread, generateSearchQuery } = require('../data/models');
const { ErrorMessages, ConnectStatus } = require('../constants');

class ThreadRequestHandler {
    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { id: userId } = ctx.state.user;
        const { q = '' } = ctx.query;
        const unSeen = false;

        const searchCondition = !_.isEmpty(q) ? generateSearchQuery(q) : {};

        const { rows: threads, count: total } = await Thread.scope({
            method: ['request', userId, unSeen, searchCondition]
        }).findAndCountAll({
            order: [['lastMessage', 'updatedAt', 'DESC']],
            where: { $lastMessageId$: { $ne: null } },
            offset,
            limit
        });

        await MessageRecipient.update(
            { isSeen: true },
            {
                where: {
                    userId,
                    isSeen: false,
                    threadId: {
                        $in: ThreadRequest.generateNestedQuery({
                            attributes: ['threadId'],
                            where: { receiverId: userId, statusId: ConnectStatus.REJECTED }
                        })
                    }
                }
            }
        );

        return ctx.ok({
            threads,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionUpdate(ctx) {
        const { id: receiverId } = ctx.state.user;
        const { statusId } = ctx.request.body;
        const { threadId } = ctx.params;

        if (!_.includes([ConnectStatus.ACCEPTED, ConnectStatus.REJECTED], statusId)) {
            return ctx.badRequest(ErrorMessages.INVALID_STATUS_ID);
        }

        const threadRequest = await ThreadRequest.findOne({
            where: { receiverId, threadId, statusId: ConnectStatus.REJECTED }
        });

        if (_.isEmpty(threadRequest)) {
            return ctx.notFound(ErrorMessages.THREAD_REQUEST_NOT_FOUND);
        }

        threadRequest.statusId = statusId;
        await threadRequest.save();

        return ctx.accepted();
    }
}

module.exports = ThreadRequestHandler;
