'use strict';

const _get = require('lodash/get');
const _has = require('lodash/has');
const _pick = require('lodash/pick');
const _extend = require('lodash/extend');
const _pickBy = require('lodash/pickBy');
const _isEmpty = require('lodash/isEmpty');

const {
    OnlineCourseMedia,
    OnlineCourseComment,
    OnlineCourseCommentLink,
    UserConnectOnlineCourse,
    OnlineCourseCommentAttachment,
    sequelize
} = require('../data/models');
const { ErrorMessages } = require('./../constants');

class OnlineCourseCommentHandler {
    static async actionCreate(ctx) {
        let attachment = _get(ctx, 'request.files.attachment');
        let { body } = ctx.request;
        let { user } = ctx.state;

        body = _pickBy(body, v => !_isEmpty(v));

        const isLink = _has(body, 'link.url');
        const hasBody = _has(body, 'text') || isLink;
        const hasAttachment = !_isEmpty(attachment);

        if (!hasBody && !hasAttachment) {
            return ctx.badRequest();
        }

        const onlineCourseMedia = await OnlineCourseMedia.findOne({
            where: { id: ctx.params.mediaOnlineCourseId }
        });

        if (
            !(await UserConnectOnlineCourse.count({
                where: { onlineCourseId: onlineCourseMedia.onlineCourseId, userId: user.id }
            }))
        ) {
            return ctx.notFound(ErrorMessages.INVALID_CREDENTIALS);
        }

        user.profile = await user.getProfile();

        const comment = await sequelize.transaction(async transaction => {
            const model = await onlineCourseMedia.createComment(
                {
                    userId: user.id,
                    text: body.text
                },
                { transaction }
            );

            if (hasBody && isLink) {
                _extend(model, {
                    link: await OnlineCourseCommentLink.create(
                        {
                            onlineCourseCommentId: model.id,
                            ..._pick(body.link, ['url', 'image', 'title', 'description'])
                        },
                        { transaction }
                    )
                });
            } else if (!_isEmpty(attachment)) {
                const documentModel = new OnlineCourseCommentAttachment({
                    onlineCourseCommentId: model.id,
                    meta: attachment
                });

                model.attachment = await documentModel.save({ transaction });
            }

            return _extend(model, {
                user,
                onlineCourseMedia: {
                    userId: onlineCourseMedia.userId
                }
            });
        });

        return ctx.created({ data: comment });
    }

    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { user } = ctx.state;

        const { rows: comments, count: total } = await OnlineCourseComment.scope({
            method: ['expand', user.id, ctx.params.mediaOnlineCourseId]
        }).findAndCountAll({
            limit,
            offset
        });

        return ctx.ok({
            data: comments,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionDelete(ctx) {
        const { id: userId } = ctx.state.user;

        const comment = await OnlineCourseComment.findOne({
            where: {
                id: ctx.params.mediaOnlineCourseId,
                $or: [
                    { userId: userId },
                    { '$onlineCourse.userId$': userId },
                    { '$onlineCourseMedia.userId$': userId }
                ]
            },
            include: ['onlineCourse', 'onlineCourseMedia']
        });

        if (_isEmpty(comment)) {
            return ctx.notFound(ErrorMessages.INVALID_CREDENTIALS);
        }

        await comment.destroy();

        return ctx.accepted();
    }
}

module.exports = OnlineCourseCommentHandler;
