'use strict';

const _isEmpty = require('lodash/isEmpty');

const { ErrorMessages, GroupConnectionStatus, GroupConnectionType } = require('./../constants');
const { Group, User, UserGroup, UserConnectGroup } = require('./../data/models');

class UserGroupHandler {
    static async actionInvite(ctx) {
        const { id: userId } = ctx.state.user;

        const { requestedUserId, groupId } = ctx.params;

        if (!(await User.count({ where: { id: requestedUserId } }))) {
            return ctx.badRequest(ErrorMessages.USER_NOT_FOUND);
        } else if (!(await Group.count({ where: { id: groupId } }))) {
            return ctx.badRequest(ErrorMessages.GROUP_NOT_FOUND);
        } else if (userId === requestedUserId) {
            return ctx.badRequest(ErrorMessages.INVALID_RECEIVER_ID);
        } else if (!(await Group.count({ where: { userId } }))) {
            return ctx.badRequest(ErrorMessages.ONLY_ADMIN_CAN_INVITE);
        } else if (await UserGroup.count({ where: { userId: requestedUserId, groupId } })) {
            return ctx.badRequest(ErrorMessages.ARE_ALREADY_IN_THIS_GROUP);
        }

        return ctx.ok({
            data: await UserGroup.create({
                groupId,
                userId: requestedUserId,
                type: GroupConnectionType.INVITE
            })
        });
    }

    static async actionRequest(ctx) {
        const { id: userId } = ctx.state.user;
        const { groupId } = ctx.params;

        if (!(await Group.count({ where: { id: groupId } }))) {
            return ctx.badRequest(ErrorMessages.GROUP_NOT_FOUND);
        } else if (await Group.count({ where: { userId } })) {
            return ctx.badRequest(ErrorMessages.ARE_ALREADY_IN_THIS_GROUP);
        }
        return ctx.ok({
            data: await UserGroup.create({
                userId,
                groupId,
                type: GroupConnectionType.REQUEST
            })
        });
    }

    static async actionDelete(ctx) {
        const { userId, groupId } = ctx.params;

        if (!(await Group.count({ where: { userId: ctx.state.user.id } }))) {
            return ctx.badRequest(ErrorMessages.ONLY_ADMIN_CAN_REMOVE);
        } else if (ctx.state.user.id === userId) {
            return ctx.badRequest(ErrorMessages.ARE_GROUP_ADMIN);
        }

        return (await UserGroup.destroy({
            where: {
                groupId,
                userId
            }
        }))
            ? ctx.accepted()
            : ctx.notFound();
    }

    static async actionAcceptInvitation(ctx) {
        const { id: userId } = ctx.state.user;
        const { groupId } = ctx.params;
        const status = ctx.query.status ? GroupConnectionStatus.ACCEPTED : GroupConnectionStatus.DECLINED;

        const invitation = await UserGroup.findOne({
            where: { userId, groupId, status: GroupConnectionStatus.PENDING, type: GroupConnectionType.INVITE }
        });

        if (_isEmpty(invitation)) {
            return ctx.notFound(ErrorMessages.USER_NOT_FOUND);
        }

        if (await UserConnectGroup.count({ where: { groupId, userId } })) {
            return ctx.badRequest(ErrorMessages.ARE_ALREADY_IN_THIS_GROUP);
        }

        invitation.status = status;

        await UserConnectGroup.create({
            groupId: invitation.groupId,
            userId
        });

        return ctx.ok({ data: await invitation.save() });
    }

    static async actionAcceptRequest(ctx) {
        const { id: userId } = ctx.state.user;
        const { groupId, requestedUserId } = ctx.params;
        const status = ctx.query.status ? GroupConnectionStatus.ACCEPTED : GroupConnectionStatus.DECLINED;

        const group = await Group.findOne({ where: { userId, id: groupId } });

        if (_isEmpty(group)) {
            return ctx.badRequest(ErrorMessages.ONLY_ADMIN_CAN_ACCEPT);
        }

        const request = await UserGroup.findOne({
            where: {
                groupId,
                userId: requestedUserId,
                type: GroupConnectionType.REQUEST,
                status: GroupConnectionStatus.PENDING
            }
        });

        if (_isEmpty(request)) {
            return ctx.notFound('request not found');
        }

        if (await UserConnectGroup.count({ where: { groupId, userId: requestedUserId } })) {
            return ctx.badRequest(ErrorMessages.ARE_ALREADY_IN_THIS_GROUP);
        }

        request.status = status;

        await UserConnectGroup.create({
            groupId,
            userId: requestedUserId
        });

        return ctx.ok({ data: await request.save() });
    }
}

module.exports = UserGroupHandler;
