'use strict';

const fs = require('fs');
const _ = require('lodash');

const { MessageRecipient, Message, Thread, ThreadUser, sequelize } = require('../data/models');
const { ErrorMessages, MessageType, MediaType, ConnectStatus } = require('../constants');
const { Thumbnail } = require('../components');
// const { Notification } = require('../services');
const config = require('../config');

class MessageHandler {
    static async actionIndex(ctx) {
        const { limit, offset, page: currentPage } = ctx.state.paginate;
        const { id: userId } = ctx.state.user;
        const { threadId } = ctx.params;

        if (!(await Thread.scope({ method: ['available', userId] }).count({ where: { id: threadId } }))) {
            return ctx.notFound(ErrorMessages.THREAD_NOT_FOUND_OR_YOU_HAVE_NOT_ACCESS);
        }

        const { rows: messages, count: total } = await Message.scope({
            method: ['messages', userId]
        }).findAndCountAll({
            where: { threadId },
            order: [['createdAt', 'DESC']],
            offset,
            limit
        });

        return ctx.ok({
            messages,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionCreate(ctx) {
        const { extensions, size, dimension } = config.get('params:validation:files:image');
        let media = _.get(ctx.request, 'files.media');
        const { text } = ctx.request.body;
        const { threadId } = ctx.params;
        let typeId = MessageType.TEXT;
        const { user } = ctx.state;
        const userId = user.id;
        let thumbnail = '';

        const thread = await Thread.scope({ method: ['available', userId] }).findByPk(threadId);

        if (_.isEmpty(text) && _.isEmpty(media)) {
            return ctx.notFound(ErrorMessages.MESSAGE_REQUIRED);
        } else if (_.isEmpty(thread)) {
            return ctx.notFound(ErrorMessages.THREAD_NOT_FOUND);
        } else if (!thread.canWrite(userId)) {
            return ctx.forbidden(ErrorMessages.THREAD_NOT_FOUND_OR_YOU_HAVE_NOT_ACCESS);
        }

        if (!_.isEmpty(media)) {
            if (!_.includes(extensions, media.ext)) {
                return ctx.unsupportedMediaType(ErrorMessages.INVALID_FILE_TYPE);
            } else if (media.size > size) {
                return ctx.badRequest(ErrorMessages.INVALID_FILE_SIZE);
            }

            thumbnail = await Thumbnail.generator(media, dimension);

            typeId = MessageType.ATTACHMENT;
        }

        const id = await sequelize.transaction(async transaction => {
            const index = (await Message.count({ where: { threadId } })) + 1;

            const model = await Message.create({ text, typeId, threadId, userId, index }, { transaction });

            const messageId = model.id;

            if (media) {
                const mediaKey = `messages/${messageId}/${_.get(media, 'key')}`;
                const thumbnailKey = `messages/${messageId}/${_.get(thumbnail, 'key')}`;

                await model.createMedia(
                    {
                        typeId: MediaType.MESSAGE_MEDIA_ATTACHMENT,
                        thumbnail: thumbnailKey,
                        data: mediaKey,
                        meta: media
                    },
                    { transaction }
                );

                fs.mkdirSync(`media/messages/${messageId}/`, { recursive: true }, err => err);
                fs.writeFileSync(`media/${mediaKey}`, fs.readFileSync(media.path));

                fs.mkdirSync(`media/messages/${messageId}/`, { recursive: true }, err => err);
                fs.writeFileSync(`media/${thumbnailKey}`, fs.readFileSync(media.path));

                // Todo s3 upload
                // await S3.upload(media, { Key: mediaKey });
                // await S3.upload(thumbnail, { Key: thumbnailKey });
            }

            if (!_.isEmpty(thread.request)) {
                if (thread.request.statusId === ConnectStatus.REJECTED && userId === thread.request.receiverId) {
                    await thread.request.update({ statusId: ConnectStatus.ACCEPTED }, { transaction });
                }
            }

            return messageId;
        });

        const message = await Message.scope({ method: ['expand', userId] }).findByPk(id);

        // Notification.messageSend(user, message, thread).catch(console.trace);

        return ctx.created({ message });
    }

    static async actionRead(ctx) {
        const { threadId } = ctx.params;
        const { user } = ctx.state;

        let threadUsers = await ThreadUser.findAll({
            attributes: ['userId'],
            where: {
                threadId
            }
        });

        let threadUser = _.find(threadUsers, v => v.userId === user.id);

        if (_.isEmpty(threadUser)) {
            return ctx.notFound();
        }

        await MessageRecipient.update(
            {
                isSeen: true,
                isRead: true
            },
            {
                where: {
                    userId: user.id,
                    isRead: false,
                    threadId
                }
            }
        );

        const profile = await user.getProfile();
        let data = {
            action: {
                read: new Date()
            },
            sender: _.extend(await user.fields({}, user), { profile: await profile.fields() }),
            threadId
        };

        _.forEach(threadUsers, v => {
            if (v.userId !== user.id) {
                global.io.to(v.userId).emit('message:read', data);
            }
        });

        return ctx.accepted();
    }

    static async actionDelete(ctx) {
        const { threadId, messageId } = ctx.params;
        const { user } = ctx.state;
        const userId = user.id;

        const [thread, message] = await Promise.all([
            Thread.scope({ method: ['available', userId] }).findByPk(threadId),
            Message.findOne({ where: { id: messageId, threadId } })
        ]);

        if (_.isEmpty(thread) || _.isEmpty(message)) {
            return ctx.notFound(ErrorMessages.THREAD_NOT_FOUND_OR_YOU_HAVE_NOT_ACCESS);
        }

        if (message.userId === userId) {
            if (_.get(thread, 'lastMessageId') === messageId) {
                const lastMessage = await Message.findOne({
                    where: { threadId, id: { $ne: messageId } },
                    order: [['createdAt', 'DESC']],
                    attributes: ['id']
                });

                thread.lastMessageId = _.get(lastMessage, 'id') || null;
                await thread.save();
            }

            await message.destroy();

            // Notification.messageDelete(user, messageId, threadId).catch(console.trace);
        }

        return ctx.accepted();
    }
}

module.exports = MessageHandler;
