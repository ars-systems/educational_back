'use strict';

const validator = require('validator');
const _extend = require('lodash/extend');
const _isEmpty = require('lodash/isEmpty');

const { ErrorMessages, UserTokenType } = require('../constants');
const { User, UserToken, sequelize } = require('../data/models');
const { Security, Mailer } = require('../components');
const config = require('../config');

class UserPasswordHandler {
    static async actionChange(ctx) {
        const { user } = ctx.state;
        let { password, newPassword } = ctx.body;

        password = Security.cleanPassword(password);
        newPassword = Security.cleanPassword(newPassword);

        if (!(await user.comparePassword(password))) {
            return ctx.badRequest(ErrorMessages.INVALID_OLD_PASSWORD);
        } else if (password === newPassword) {
            return ctx.badRequest(ErrorMessages.PASSWORDS_DO_NOT_MATCH);
        }

        user.password = newPassword;
        await user.save();

        return ctx.ok({
            token: user.generateToken()
        });
    }

    static async actionForgot(ctx) {
        const user = await User.findOne({
            where: { email: validator.normalizeEmail(ctx.query.email) }
        });

        if (_isEmpty(user)) {
            return ctx.notFound(ErrorMessages.USER_NOT_FOUND);
        }

        const token = await user.createForgotPasswordToken({});

        Mailer.send(
            _extend(
                {
                    data: {
                        firstName: user.profile.firstName,
                        forgotPasswordCode: `http://${config.get('server:url')}/users/reset?token=${token}`
                    },
                    to: [user.email]
                },
                config.get('params:emailTemplates:forgotPassword')
            )
        ).catch(console.error);

        return ctx.accepted();
    }

    static async actionReset(ctx) {
        const { token, password } = ctx.body;

        const userToken = await UserToken.findOne({
            where: {
                token,
                typeId: UserTokenType.FORGOT_PASSWORD_TOKEN
            }
        });

        if (_isEmpty(userToken)) {
            return ctx.notFound(ErrorMessages.INVALID_TOKEN);
        }

        const user = await userToken.getUser();

        user.password = Security.cleanPassword(password);

        await sequelize.transaction(async transaction => {
            await user.save({ transaction });
            await userToken.destroy({ transaction });
        });

        return ctx.ok();
    }
}

module.exports = UserPasswordHandler;
