'use strict';

const fs = require('fs');
const path = require('path');
const Router = require('koa-router');

const router = new Router();
const basename = path.basename(__filename);

fs.readdirSync(__dirname)
    .filter(file => file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js')
    .forEach(file => router.use(require(`./${file}`).routes()));

router.get('/ping', ctx => ctx.ok('pong'));

module.exports = router;
