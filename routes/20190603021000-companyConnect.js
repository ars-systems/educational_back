'use strict';

const Router = require('koa-router');

const CompanyConnectHandler = require('../handlers/CompanyConnectHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/company'
});

router.use(auth);

router.get('/connects/pending', CompanyConnectHandler.actionPending);
router.get('/connects', CompanyConnectHandler.actionIndex);
router.get('/internal', CompanyConnectHandler.actionConnection);
router.get('/users', CompanyConnectHandler.actionUsers);
router.get('/auto-complete', CompanyConnectHandler.actionAutoComplete);

router.post('/connects', CompanyConnectHandler.actionCreate);

router.put('/connects', CompanyConnectHandler.actionUpdate);
router.put('/connects/role/:id', CompanyConnectHandler.actionUpdateRole);
router.put('/connects/work/:id', CompanyConnectHandler.actionUpdateWork);

router.delete('/connects/:id', CompanyConnectHandler.actionDisconnect);

module.exports = router;
