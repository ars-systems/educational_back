'use strict';

const Router = require('koa-router');

const CompanyPartnershipHandler = require('../handlers/CompanyPartnershipHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/company/partnership'
});

router.use(auth);

router.get('/', CompanyPartnershipHandler.actionIndex);
router.get('/auto-complete', CompanyPartnershipHandler.actionAutoComplete);
router.get('/pending', CompanyPartnershipHandler.actionPending);
router.get('/companies', CompanyPartnershipHandler.actionCompanies);

router.post('/', CompanyPartnershipHandler.actionCreate);

router.put('/', CompanyPartnershipHandler.actionUpdate);

router.delete('/:id', CompanyPartnershipHandler.actionDisconnect);

module.exports = router;
