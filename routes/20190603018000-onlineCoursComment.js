'use strict';

const Router = require('koa-router');

const OnlineCourseCommentHandler = require('../handlers/OnlineCourseCommentHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/cours/:mediaOnlineCourseId/comments'
});

router.use(auth);

router.get('/', OnlineCourseCommentHandler.actionIndex);

router.post('/', OnlineCourseCommentHandler.actionCreate);

router.delete('/', OnlineCourseCommentHandler.actionDelete);

module.exports = router;
