'use strict';

const Router = require('koa-router');

const UserGroupHandler = require('../handlers/UserGroupHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/group'
});

router.get('/invite/accept/:groupId', auth, UserGroupHandler.actionAcceptInvitation);
router.get('/request/accept/:groupId/:requestedUserId', auth, UserGroupHandler.actionAcceptRequest);

router.put('/invite/:groupId/:requestedUserId', auth, UserGroupHandler.actionInvite);
router.put('/request/:groupId/', auth, UserGroupHandler.actionRequest);

router.delete('/user/:groupId/:userId', auth, UserGroupHandler.actionDelete);

module.exports = router;
