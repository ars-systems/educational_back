'use strict';

const Router = require('koa-router');

const MessageHandler = require('../handlers/MessageHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/threads'
});

router.get('/:threadId/messages', auth, MessageHandler.actionIndex);

router.post('/:threadId/messages', auth, MessageHandler.actionCreate);

router.put('/:threadId/messages/read', auth, MessageHandler.actionRead);

router.delete('/:threadId/messages/:messageId', auth, MessageHandler.actionDelete);

module.exports = router;
