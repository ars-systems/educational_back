'use strict';

const Router = require('koa-router');

const UserOnlineCourseHandler = require('../handlers/UserOnlineCourseHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/courses'
});

router.get('/invite/:courseId', auth, UserOnlineCourseHandler.actionAcceptInvitation);
router.get('/request/:courseId/:requestedUserId', auth, UserOnlineCourseHandler.actionAcceptRequest);

router.put('/invite/:courseId/:requestedUserId', auth, UserOnlineCourseHandler.actionInvite);
router.put('/request/:courseId/', auth, UserOnlineCourseHandler.actionRequest);

module.exports = router;
