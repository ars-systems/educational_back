'use strict';

const Router = require('koa-router');

const RateHandler = require('../handlers/RateHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/rate'
});

router.use(auth);

router.put('/media/:mediaId', auth, RateHandler.actionRateMedia);
router.put('/group/:groupId', auth, RateHandler.actionRateGroup);
router.put('/user/:ratedUserId', auth, RateHandler.actionRateUser);
router.put('/onlineCourse/:onlineCourseId', auth, RateHandler.actionRateOnlineCourse);

module.exports = router;
