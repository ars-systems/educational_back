'use strict';

const Router = require('koa-router');

const GroupCommentHandler = require('../handlers/GroupCommentHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/group/:mediaGroupId/comments'
});

router.use(auth);

router.get('/', GroupCommentHandler.actionIndex);

router.post('/', GroupCommentHandler.actionCreate);

router.delete('/', GroupCommentHandler.actionDelete);

module.exports = router;
