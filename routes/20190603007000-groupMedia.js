'use strict';

const Router = require('koa-router');

const GroupMediaHandler = require('../handlers/GroupMediaHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/users/groups/media'
});

router.use(auth);

router.get('/', GroupMediaHandler.actionIndex);
router.get('/:groupMediaId', GroupMediaHandler.actionView);

router.post('/:groupId', GroupMediaHandler.actionCreate);

router.put('/:groupMediaId', GroupMediaHandler.actionUpdate);

router.delete('/:groupMediaId', GroupMediaHandler.actionDelete);

module.exports = router;
