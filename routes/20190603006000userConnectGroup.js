'use strict';

const Router = require('koa-router');

const UserConnectGroupHandler = require('../handlers/UserConnectGroupHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/users/connects/groups'
});

router.use(auth);

router.get('/:id', auth, UserConnectGroupHandler.actionIndex);
router.get('/search/:id', UserConnectGroupHandler.actionSearch);

router.post('/:id', UserConnectGroupHandler.actionCreate);

router.delete('/:id', UserConnectGroupHandler.actionDelete);

module.exports = router;
