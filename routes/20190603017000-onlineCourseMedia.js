'use strict';

const Router = require('koa-router');

const OnlineCourseMediaHandler = require('../handlers/OnlineCourseMediaHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/users/courses/media'
});

router.use(auth);

router.get('/', OnlineCourseMediaHandler.actionIndex);
router.get('/:courseMediaId', OnlineCourseMediaHandler.actionView);

router.post('/:courseId', OnlineCourseMediaHandler.actionCreate);

router.put('/:courseMediaId', OnlineCourseMediaHandler.actionUpdate);

router.delete('/:courseMediaId', OnlineCourseMediaHandler.actionDelete);

module.exports = router;
