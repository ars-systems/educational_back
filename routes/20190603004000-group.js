'use strict';

const Router = require('koa-router');

const GroupHandler = require('../handlers/GroupHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/group'
});

router.get('/', GroupHandler.actionIndex);
router.get('/:groupId', GroupHandler.actionView);

router.post('/', auth, GroupHandler.actionCreate);

router.delete('/:groupId', auth, GroupHandler.actionDelete);

module.exports = router;
