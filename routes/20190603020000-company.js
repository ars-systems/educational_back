'use strict';

const Router = require('koa-router');

const CompanyHandler = require('./../handlers/CompanyHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/company'
});

router.use(auth);

router.get('/', CompanyHandler.actionIndex);

router.post('/', CompanyHandler.actionCreate);
router.put('/', CompanyHandler.actionUpdate);

router.put('/logo', CompanyHandler.actionUploadLogo);

router.delete('/', CompanyHandler.actionDelete);
router.delete('/logo', CompanyHandler.actionRemoveLogo);

module.exports = router;
