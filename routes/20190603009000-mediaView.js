'use strict';

const Router = require('koa-router');

const MediaViewHandler = require('../handlers/MediaViewHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/media'
});

router.get('/view/:id', auth, MediaViewHandler.actionView);
router.get('/followers/view', auth, MediaViewHandler.actionFollowerMedia);

router.put('/view/:mediaId', MediaViewHandler.actionSave);

module.exports = router;
