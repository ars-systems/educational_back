'use strict';

const Router = require('koa-router');

const ThreadRequestHandler = require('../handlers/ThreadRequestHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/threads'
});

router.get('/requests/info', auth, ThreadRequestHandler.actionIndex);

router.put('/:threadId/requests', auth, ThreadRequestHandler.actionUpdate);

module.exports = router;
