'use strict';

const Router = require('koa-router');

const OnlineCourseHandler = require('../handlers/OnlineCourseHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/courses'
});

router.get('/', OnlineCourseHandler.actionIndex);
router.get('/:id', OnlineCourseHandler.actionView);

router.post('/', auth, OnlineCourseHandler.actionCreate);

router.put('/:courseId', auth, OnlineCourseHandler.actionUpdate);

router.delete('/:id', auth, OnlineCourseHandler.actionDelete);

module.exports = router;
