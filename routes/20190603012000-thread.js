'use strict';

const Router = require('koa-router');

const ThreadHandler = require('../handlers/ThreadHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/threads'
});

router.get('/', auth, ThreadHandler.actionIndex);
router.get('/:threadId', auth, ThreadHandler.actionView);

router.post('/', auth, ThreadHandler.actionCreate);

router.put('/:threadId/typing', auth, ThreadHandler.actionTyping);

router.delete('/:threadId', auth, ThreadHandler.actionDelete);

module.exports = router;
