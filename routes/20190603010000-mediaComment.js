'use strict';

const Router = require('koa-router');

const MediaCommentHandler = require('../handlers/MediaCommentHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/media/:mediaId/comments'
});

router.use(auth);

router.get('/', MediaCommentHandler.actionIndex);

router.post('/', MediaCommentHandler.actionCreate);

router.delete('/', MediaCommentHandler.actionDelete);

module.exports = router;
