'use strict';

const Router = require('koa-router');

const UserHandler = require('../handlers/UserHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/users'
});

router.get('/connect', auth, UserHandler.actionNotConnect);
router.get('/activation', UserHandler.actionActivation);
router.get('/', auth, UserHandler.actionIndex);
router.get('/:id', auth, UserHandler.actionView);

router.post('/', UserHandler.actionCreate);
router.post('/login', UserHandler.actionLogin);

router.put('/', auth, UserHandler.actionUpdate);
router.put('/avatar', auth, UserHandler.actionUploadAvatar);

router.delete('/', auth, UserHandler.actionDelete);

module.exports = router;
