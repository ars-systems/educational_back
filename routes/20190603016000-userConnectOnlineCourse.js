'use strict';

const Router = require('koa-router');

const UserConnectOnlineCourseHandler = require('../handlers/UserConnectOnlineCourseHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/users/connects/courses'
});

router.use(auth);

router.get('/:id', auth, UserConnectOnlineCourseHandler.actionNotInCourse);
// router.get('/:id', auth, UserConnectOnlineCourseHandler.actionIndex);
router.get('/search/:id', UserConnectOnlineCourseHandler.actionSearch);

router.post('/:id', UserConnectOnlineCourseHandler.actionCreate);

router.delete('/:courseId/:userId', UserConnectOnlineCourseHandler.actionDelete);

module.exports = router;
