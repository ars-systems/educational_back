'use strict';

const Router = require('koa-router');

const UserConnectHandler = require('../handlers/UserConnectHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/connects'
});

router.get('/', auth, UserConnectHandler.actionIndex);

router.post('/:receiverId', auth, UserConnectHandler.actionCreate);

router.delete('/:id', auth, UserConnectHandler.actionRemove);

module.exports = router;
