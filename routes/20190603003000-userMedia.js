'use strict';

const Router = require('koa-router');

const UserMediaHandler = require('../handlers/UserMediaHandler');
const auth = require('../middlewares/auth');

const router = new Router({
    prefix: '/media'
});

router.get('/', auth, UserMediaHandler.actionIndex);
router.get('/:id', auth, UserMediaHandler.actionView);

router.post('/', auth, UserMediaHandler.actionCreate);

router.put('/read/:mediaId', auth, UserMediaHandler.actionRead);

router.delete('/:id', auth, UserMediaHandler.actionDelete);

module.exports = router;
