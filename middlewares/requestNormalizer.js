'use strict';

const _get = require('lodash/get');
const koaBody = require('koa-body');
const fileType = require('file-type');
const compose = require('koa-compose');
const readChunk = require('read-chunk');
const _extend = require('lodash/extend');
const _isEmpty = require('lodash/isEmpty');
const _isArray = require('lodash/isArray');
const _isObject = require('lodash/isObject');
const _toString = require('lodash/toString');

const { Security, File } = require('../components');
const paginate = require('./paginate');

const fileTypes = {
    csv: { ext: 'csv', mime: 'text/csv' }
};

async function normalizeFile(file) {
    const fileInfo = fileType(readChunk.sync(_get(file, 'path'), 0, 4 + 4096)) || {};

    // TODO check docx file type
    if (_isEmpty(fileInfo)) {
        const isCsv = await File.validateCsv(_get(file, 'path'));

        if (isCsv) {
            _extend(fileInfo, fileTypes.csv);
        }
    }

    return {
        key: `${Security.generateRandomString()}.${_get(fileInfo, 'ext')}`,
        size: _toString(_get(file, 'size')),
        mime: _get(fileInfo, 'mime'),
        ext: _get(fileInfo, 'ext'),
        path: _get(file, 'path'),
        name: _get(file, 'name')
    };
}

async function normalizer(ctx, next) {
    if (ctx.request.type === 'multipart/form-data') {
        for (let key of Object.keys(ctx.request.files)) {
            const value = _get(ctx.request.files, key);

            if (_isArray(value)) {
                ctx.request.files[key] = [];

                for (let item of value) {
                    ctx.request.files[key].push(await normalizeFile(item));
                }
            } else if (_isObject(value)) {
                ctx.request.files[key] = await normalizeFile(value);
            }
        }
    }

    await next();
}

module.exports = params =>
    compose([koaBody(_get(params, 'bodyParser')), normalizer, paginate(_get(params, 'paginate'))]);
