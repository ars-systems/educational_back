'use strict';

const os = require('os');
const mm = require('music-metadata');
const ffmpeg = require('fluent-ffmpeg');

class File {
    /**
     * @param pathToFile
     * @param pathToSnapshot
     * @param time
     * @param dimension
     * @return {Promise<any>}
     */
    static snapshot(pathToFile, pathToSnapshot, time, dimension) {
        return new Promise(resolve => {
            ffmpeg(pathToFile)
                .takeScreenshots({ timemarks: [time], filename: pathToSnapshot, folder: os.tmpdir() })
                .on('error', () => resolve(false))
                .on('end', () => resolve(true));
        });
    }

    /**
     * @param pathToFile
     * @return Object
     */
    static async audioMetadata(pathToFile) {
        try {
            return await mm.parseFile(pathToFile, { native: true });
        } catch (e) {
            return {};
        }
    }
}

module.exports = File;
