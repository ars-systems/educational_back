'use strict';

const os = require('os');
const fs = require('fs');
const _ = require('lodash');
const sharp = require('sharp');

const config = require('../config');
const File = require('./File');

class Thumbnail {
    static async generator(file, dimension) {
        const { availableMethods } = config.get('params:validation:files:thumbnail');
        const methodName = _.get(file, 'type');

        if (_.includes(availableMethods, methodName)) {
            return this[methodName](file, dimension);
        }

        return file;
    }

    static async image(file, dimension) {
        try {
            const { ext } = config.get('params:validation:files:thumbnail:image');
            const key = `thumbnail_${_.head(_.split(file.key, '.'))}.${ext}`;
            const path = `${os.tmpdir()}/${key}`;

            await sharp(_.get(file, 'path'))
                .jpeg({ progressive: true, force: false })
                .png({ progressive: true, force: false })
                .rotate()
                .resize(dimension || config.get('params:validation:files:dimension'))
                .toFile(path);

            const size = _.toString(fs.statSync(path).size);

            return { ...file, size, path, key, ext };
        } catch (e) {
            console.log(e);
        }

        return file;
    }

    static async video(file) {
        const { ext, time, mime, dimension } = config.get('params:validation:files:thumbnail:video');
        const key = `thumbnail_${_.head(_.split(file.key, '.'))}.${ext}`;

        const snapshot = await File.snapshot(`${_.get(file, 'path')}`, key, time, dimension);

        if (snapshot) {
            const path = `${os.tmpdir()}/${key}`;
            const size = _.toString(fs.statSync(path).size);

            return { ...file, ext, key, mime, path, size };
        }

        return {};
    }

    static async audio(file) {
        const { ext } = config.get('params:validation:files:thumbnail:audio');

        const metadata = await File.audioMetadata(`${_.get(file, 'path')}`);

        const picture = _.get(metadata, 'common.picture[0]', {});
        const mime = _.get(picture, 'format', '');

        if (!_.isEmpty(picture) && !_.isEmpty(mime)) {
            const key = `thumbnail_${_.head(_.split(file.key, '.'))}.${ext}`;
            const { title, artist, album } = _.get(metadata, 'common');
            const path = `${os.tmpdir()}/${key}`;

            fs.writeFileSync(path, _.get(picture, 'data'), 'binary');

            return { ...file, ext, key, path, mime, title, artist, album };
        }

        return {};
    }
}

module.exports = Thumbnail;
