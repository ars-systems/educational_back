'use strict';

const bcrypt = require('bcrypt');
const crypto = require('crypto');
const _trim = require('lodash/trim');
const _chain = require('lodash/chain');
const _toString = require('lodash/toString');
const randomString = require('randomstring');

class Security {
    /**
     * @param password
     * @return {*}
     */
    static cleanPassword(password) {
        return _trim(password).replace(/ /g, '');
    }
    /**
     * @param options
     */
    static generateRandomString(options = 32) {
        return randomString.generate(options);
    }

    /**
     * @return {string}
     */
    static generateRandomKey(options = 16) {
        return crypto.randomBytes(options).toString('hex');
    }

    /**
     * @param password
     * @return {Promise<*>}
     */
    static generatePasswordHash(password) {
        return bcrypt.hash(password, 10);
    }

    /**
     * @param password
     * @param hash
     * @return {*}
     */
    static validatePassword(password, hash) {
        return bcrypt.compare(password, hash);
    }

    /**
     * @param ids
     * @return []
     */
    static cleanAndCastArrayIDs(ids) {
        return _chain(ids)
            .castArray()
            .filter(id => _toString(id) && id !== '')
            .uniq()
            .sortBy()
            .value();
    }
}

module.exports = Security;
