'use strict';

const moment = require('moment');
const _get = require('lodash/get');
const _extend = require('lodash/extend');
const _isEmpty = require('lodash/isEmpty');
const sendGrid = require('@sendgrid/mail');

const config = require('../config');

class Mailer {
    send(context) {
        if (_isEmpty(context.to)) {
            throw new Error('email addressees are required');
        }
        return new Promise(resolve => {
            sendGrid.setApiKey(config.get('sendGrid:api:key'));

            return resolve(
                sendGrid.send({
                    to: context.to,
                    from: config.get('sendGrid:from'),
                    subject: context.subject,
                    dynamic_template_data: _extend(context.data, { date: moment().format('YYYY-MM-DD') }),
                    templateId: _get(context, 'templateId')
                })
            );
        });
    }
}

module.exports = new Mailer();
