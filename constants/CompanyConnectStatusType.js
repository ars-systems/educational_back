'use strict';

module.exports = {
    ACCEPTED: 1,
    PENDING: 2,
    DISCONNECTED: 3
};
