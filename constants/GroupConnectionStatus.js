'use strict';

module.exports = {
    PENDING: 1,
    DECLINED: 2,
    ACCEPTED: 3
};
