'use strict';

module.exports = {
    MEMBER: 1,
    ADMIN: 2,
    MANAGER: 3
};
