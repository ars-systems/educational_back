'use strict';

module.exports = {
    BOOK: 1,
    TEST: 2,
    ARTICLE: 3,
    AUDIO_BOOK: 4,
    ONLINE_COURSE: 5,
    GROUP_MEDIA_ATTACHMENT: 6,
    MESSAGE_MEDIA_ATTACHMENT: 7,
    ONLINE_COURSE_ATTACHMENT: 8
};
