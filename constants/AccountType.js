'use strict';

module.exports = {
    OTHER: 1,
    STUDENT: 2,
    COMPANY: 3,
    LECTURER: 4
};
