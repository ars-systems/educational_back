'use strict';

const ReadType = require('./ReadType');
const GroupType = require('./GroupType');
const MediaType = require('./MediaType');
const SocialType = require('./SocialType');
const AccountType = require('./AccountType');
const MessageType = require('./MessageType');
const CompanyRole = require('./CompanyRole');
const ConnectStatus = require('./ConnectStatus');
const MediaCategory = require('./MediaCategory');
const UserTokenType = require('./UserTokenType');
const ErrorMessages = require('./ErrorMessages');
const ConnectionType = require('./ConnectionType');
const GroupMediaType = require('./GroupMediaType');
const ActivityLogType = require('./ActivityLogType');
const OnlineCourseType = require('./OnlineCourseType');
const GroupConnectionType = require('./GroupConnectionType');
const OnlineCourseCategory = require('./OnlineCourseCategory');
const GroupConnectionStatus = require('./GroupConnectionStatus');
const OnlineCourseMediaType = require('./OnlineCourseMediaType');
const UserConnectStatusType = require('./UserConnectStatusType');
const CompanyConnectStatusType = require('./CompanyConnectStatusType');
const OnlineCourseConnectionType = require('./OnlineCourseConnectionType');
const OnlineCourseConnectionStatus = require('./OnlineCourseConnectionStatus');

module.exports = {
    OnlineCourseConnectionStatus,
    OnlineCourseConnectionType,
    CompanyConnectStatusType,
    UserConnectStatusType,
    OnlineCourseMediaType,
    GroupConnectionStatus,
    OnlineCourseCategory,
    GroupConnectionType,
    OnlineCourseType,
    ActivityLogType,
    GroupMediaType,
    ConnectionType,
    ErrorMessages,
    UserTokenType,
    MediaCategory,
    ConnectStatus,
    CompanyRole,
    MessageType,
    AccountType,
    SocialType,
    MediaType,
    GroupType,
    ReadType
};
