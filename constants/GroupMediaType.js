'use strict';

module.exports = {
    TEXT: 1,
    LINK: 2,
    FILE: 3,
    NO_CONTENT: 4
};
