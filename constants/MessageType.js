'use strict';

module.exports = {
    TEXT: 1,
    LINK: 2,
    ATTACHMENT: 3
};
