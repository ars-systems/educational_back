'use strict';

module.exports = {
    CONNECTED: 1,
    SENT_REQUEST: 2,
    RECEIVED_REQUEST: 3,
    NOT_CONNECTED: 4,
    DISCONNECTED: 5
};
