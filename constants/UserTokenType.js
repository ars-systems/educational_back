'use strict';

module.exports = {
    ACTIVATION: 1,
    ACCESS_TOKEN: 2,
    FORGOT_PASSWORD_TOKEN: 3
};
