'use strict';

module.exports = {
    READ: 1,
    READING: 2,
    WANT_TO_READ: 3
};
