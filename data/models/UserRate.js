'use strict';

const _omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
    const UserRate = sequelize.define(
        'UserRate',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            userId: {
                type: DataTypes.BIGINT
            },
            ratedUserId: {
                type: DataTypes.BIGINT
            },
            rate: {
                type: DataTypes.SMALLINT
            }
        },
        {
            tableName: 'userRate',
            timestamps: true,
            setterMethods: {}
        }
    );

    UserRate.associate = models => {
        UserRate.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });
    };

    UserRate.addScopes = models => {};

    UserRate.prototype.fields = function() {
        const model = this.get();
        const hiddenFields = ['createdAt', 'updatedAt'];

        return _omit(model, hiddenFields);
    };

    return UserRate;
};
