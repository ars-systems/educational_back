'use strict';

const _set = require('lodash/set');
const jwt = require('jsonwebtoken');
const _omit = require('lodash/omit');
const validator = require('validator');
const _isEmpty = require('lodash/isEmpty');

const { UserTokenType, AccountType } = require('../../constants');
const { Security } = require('../../components');
const config = require('../../config');

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define(
        'User',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: {
                    message: 'email.unique'
                },
                validate: {
                    isEmail: true
                }
            },
            password: {
                allowNull: false,
                type: DataTypes.STRING,
                validate: {
                    len: [6]
                }
            },
            accessTokenSalt: {
                type: DataTypes.STRING
            },
            isVerified: {
                type: DataTypes.BOOLEAN
            }
        },
        {
            tableName: 'user',
            timestamps: true,
            setterMethods: {
                email(value) {
                    this.setDataValue('email', value ? validator.normalizeEmail(value) : null);
                },
                password(value) {
                    this.setDataValue('password', value ? Security.cleanPassword(value) : null);
                }
            }
        }
    );

    User.beforeSave(async user => {
        if (user.isNewRecord || user.changed('password')) {
            user.accessTokenSalt = Security.generateRandomString(6);
            user.password = await Security.generatePasswordHash(user.password);
        }
    });

    User.associate = models => {
        User.hasOne(models.UserProfile, {
            as: 'profile',
            foreignKey: 'userId'
        });

        User.hasMany(models.UserConnect, {
            as: 'sentRequests',
            foreignKey: 'senderId'
        });

        User.hasMany(models.UserConnect, {
            as: 'receivedRequests',
            foreignKey: 'receiverId'
        });

        User.hasOne(models.UserWork, {
            as: 'work',
            foreignKey: 'userId'
        });

        User.hasOne(models.Company, {
            as: 'company',
            foreignKey: 'userId'
        });

        User.hasOne(models.CompanyUserConnect, {
            as: 'role',
            foreignKey: 'userId'
        });

        User.hasOne(models.UserToken, {
            as: 'activationToken',
            foreignKey: 'userId',
            scope: {
                typeId: UserTokenType.ACTIVATION
            }
        });

        User.hasOne(models.UserToken, {
            as: 'forgotPasswordToken',
            foreignKey: 'userId',
            scope: {
                typeId: UserTokenType.FORGOT_PASSWORD_TOKEN
            }
        });

        User.hasOne(models.UserToken, {
            as: 'tempAccessToken',
            foreignKey: 'userId',
            scope: {
                typeId: UserTokenType.ACCESS_TOKEN
            }
        });

        User.hasMany(models.Media, {
            as: 'medias',
            foreignKey: 'userId'
        });

        User.hasOne(models.MediaRead, {
            as: 'read',
            foreignKey: 'userId'
        });

        User.hasMany(models.UserConnectGroup, {
            as: 'connectGroupUsers',
            foreignKey: 'userId'
        });

        User.hasMany(models.UserConnectOnlineCourse, {
            as: 'connectOnlineCourseUsers',
            foreignKey: 'userId'
        });

        User.hasMany(models.Group, {
            as: 'groups',
            foreignKey: 'userId'
        });

        User.hasMany(models.OnlineCourse, {
            as: 'courses',
            foreignKey: 'userId'
        });

        User.belongsTo(models.UserConnect, {
            as: 'connect',
            foreignKey: 'id'
        });

        User.hasMany(models.UserRate, {
            as: 'rate',
            foreignKey: 'userId'
        });
    };

    User.addScopes = models => {
        const profileAttributes = [
            'university',
            'profession',
            'firstName',
            'lastName',
            'avatar',
            'gender',
            'typeId',
            'work'
        ];

        User.addScope('expand', () => {
            return {
                include: [
                    {
                        model: models.UserProfile,
                        as: 'profile',
                        attributes: profileAttributes
                    }
                ]
            };
        });

        User.addScope('withMedia', (id, limit, offset) => {
            return {
                subQuery: false,
                where: { id },
                include: ['profile', 'medias'],
                order: [['medias', 'id', 'DESC']],
                limit,
                offset
            };
        });

        User.addScope('media', userId => {
            return {
                where: {
                    id: {
                        $in: models.UserConnect.generateNestedQuery({
                            attributes: ['senderId'],
                            where: { receiverId: userId }
                        })
                    }
                },
                subQuery: false,
                include: [
                    'profile',
                    { model: models.UserConnect, as: 'connect', attributes: [] },
                    { model: models.Media, as: 'medias', limit: 3, order: [['id', 'desc']] }
                ]
            };
        });

        User.addScope('notConnect', (userId, searchCondition = {}, typeId) => {
            const include = !_isEmpty(typeId)
                ? [
                      {
                          model: models.UserProfile,
                          as: 'profile',
                          where: { typeId }
                      }
                  ]
                : ['profile'];

            return {
                subQuery: false,
                where: {
                    $and: [
                        {
                            id: {
                                $notIn: models.UserConnect.generateNestedQuery({
                                    attributes: [
                                        sequelize.literal(
                                            `CASE WHEN "senderId" = '${userId}' THEN "receiverId" ELSE "senderId" END`
                                        )
                                    ],
                                    where: {
                                        $or: [{ receiverId: userId }, { senderId: userId }]
                                    }
                                })
                            }
                        },
                        { id: { $ne: userId } },
                        searchCondition
                    ]
                },
                include,
                order: [['profile', 'firstName', 'ASC']],
                replacements: { userId }
            };
        });

        User.addScope('sentCompanyConnects', (userId, status) => {
            return {
                where: {
                    id: {
                        $in: models.CompanyConnect.generateNestedQuery({
                            attributes: ['receiverId'],
                            where: { senderId: userId, statusId: status }
                        })
                    }
                },
                include: [
                    {
                        model: models.UserProfile,
                        as: 'profile',
                        attributes: profileAttributes
                    },
                    {
                        model: models.UserWork,
                        as: 'work',
                        include: [{ model: models.Company, as: 'company', attributes: ['name'] }]
                    }
                ]
            };
        });

        User.addScope('receivedCompanyConnects', (userId, status) => {
            return {
                where: {
                    id: {
                        $in: models.CompanyConnect.generateNestedQuery({
                            attributes: ['senderId'],
                            where: { receiverId: userId, statusId: status }
                        })
                    }
                },
                include: [
                    {
                        model: models.UserProfile,
                        as: 'profile',
                        attributes: profileAttributes
                    },
                    {
                        model: models.Company,
                        as: 'company',
                        attributes: ['name']
                    },
                    {
                        model: models.UserWork,
                        as: 'work',
                        include: [{ model: models.Company, as: 'company', attributes: ['name'] }]
                    }
                ]
            };
        });
    };

    User.prototype.comparePassword = function(candidatePassword = '') {
        return Security.validatePassword(candidatePassword, this.password);
    };

    User.prototype.generateToken = function() {
        return {
            type: 'jwt',
            access: jwt.sign(
                {
                    salt: this.accessTokenSalt,
                    id: this.id
                },
                config.get('jwt:secret')
            )
        };
    };

    User.prototype.fields = async function(models, params) {
        let model = this.get();
        if (!_isEmpty(model.profile)) {
            if (model.profile.typeId === AccountType.LECTURER || model.profile.typeId === AccountType.COMPANY) {
                const rates = await this.getRate();
                let rateAvg = 0;

                if (!_isEmpty(rates)) {
                    let sum = rates.reduce((previous, current) => (current.rate += previous.rate));
                    rateAvg = sum / rates.length;
                }

                _set(model, 'rate', rateAvg);
            }
        }

        let hiddenFields = ['password', 'createdAt', 'updatedAt', 'accessTokenSalt'];

        return _omit(model, hiddenFields);
    };

    return User;
};
