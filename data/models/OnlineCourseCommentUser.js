'use strict';

module.exports = (sequelize, DataTypes) => {
    const OnlineCourseCommentUser = sequelize.define(
        'OnlineCourseCommentUser',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            text: {
                type: DataTypes.TEXT
            },
            channel: {
                type: DataTypes.TEXT
            }
        },
        {
            tableName: 'onlineCourseCommentUser',
            timestamps: true
        }
    );

    OnlineCourseCommentUser.associate = models => {
        OnlineCourseCommentUser.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });

        OnlineCourseCommentUser.belongsTo(models.OnlineCourseComment, {
            as: 'onlineCourseComment',
            foreignKey: 'commentId'
        });
    };

    return OnlineCourseCommentUser;
};
