'use strict';

const { Security } = require('../../components');
const { UserTokenType } = require('../../constants');

module.exports = (sequelize, DataTypes) => {
    const UserToken = sequelize.define(
        'UserToken',
        {
            token: {
                primaryKey: true,
                type: DataTypes.STRING
            },
            typeId: {
                allowNull: false,
                type: DataTypes.SMALLINT,
                validate: {
                    isIn: [Object.values(UserTokenType)]
                }
            }
        },
        {
            tableName: 'userToken',
            timestamps: true
        }
    );

    UserToken.beforeSave(async user => {
        user.token = Security.generateRandomKey();
    });

    UserToken.associate = function(models) {
        UserToken.belongsTo(models.User, {
            as: 'user'
        });
    };

    return UserToken;
};
