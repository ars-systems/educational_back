'use strict';

const _trim = require('lodash/trim');
const _omit = require('lodash/omit');
const validator = require('validator');

module.exports = (sequelize, DataTypes) => {
    const GroupLink = sequelize.define(
        'GroupLink',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            url: {
                type: DataTypes.TEXT,
                allowNull: false,
                validate: {
                    isImage(value) {
                        if (value && !validator.isURL(value)) {
                            throw new Error('Link url is not valid.');
                        }
                    }
                }
            },
            title: {
                type: DataTypes.STRING
            },
            description: {
                type: DataTypes.TEXT
            },
            image: {
                type: DataTypes.TEXT,
                validate: {
                    isImage(value) {
                        if (value && !validator.isURL(value)) {
                            throw new Error('Link image is not valid.');
                        }
                    }
                }
            }
        },
        {
            tableName: 'groupLink',
            timestamps: true,
            setterMethods: {
                url(value) {
                    this.setDataValue('url', _trim(value));
                },
                title(value) {
                    this.setDataValue('title', _trim(value));
                },
                description(value) {
                    this.setDataValue('description', _trim(value));
                },
                image(value) {
                    this.setDataValue('image', _trim(value));
                }
            }
        }
    );

    GroupLink.associate = models => {
        GroupLink.belongsTo(models.Group, {
            as: 'group',
            foreignKey: 'groupId'
        });
    };

    GroupLink.prototype.fields = function() {
        return _omit(this.get(), ['id', 'groupId', 'createdAt', 'updatedAt']);
    };

    return GroupLink;
};
