'use strict';

const _omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
    const GroupRate = sequelize.define(
        'GroupRate',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            userId: {
                type: DataTypes.BIGINT
            },
            groupId: {
                type: DataTypes.BIGINT
            },
            rate: {
                type: DataTypes.SMALLINT
            }
        },
        {
            tableName: 'groupRate',
            timestamps: true,
            setterMethods: {}
        }
    );

    GroupRate.associate = models => {
        GroupRate.belongsTo(models.Group, {
            as: 'user',
            foreignKey: 'groupId'
        });
    };

    GroupRate.addScopes = models => {};

    GroupRate.prototype.fields = function() {
        const model = this.get();
        const hiddenFields = ['createdAt', 'updatedAt'];

        return _omit(model, hiddenFields);
    };

    return GroupRate;
};
