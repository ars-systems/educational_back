'use strict';

const _omit = require('lodash/omit');

const { ReadType } = require('./../../constants');

module.exports = (sequelize, DataTypes) => {
    const MediaRead = sequelize.define(
        'MediaRead',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            userId: {
                type: DataTypes.BIGINT
            },
            mediaId: {
                type: DataTypes.BIGINT
            },
            status: {
                type: DataTypes.SMALLINT,
                validate: {
                    isIn: [Object.values(ReadType)]
                }
            }
        },
        {
            tableName: 'mediaRead',
            timestamps: true,
            setterMethods: {}
        }
    );

    MediaRead.associate = models => {
        MediaRead.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });
    };

    MediaRead.addScopes = models => {};

    MediaRead.prototype.fields = function() {
        const model = this.get();
        const hiddenFields = ['createdAt', 'updatedAt'];

        return _omit(model, hiddenFields);
    };

    return MediaRead;
};
