'use strict';

const _set = require('lodash/set');
const _omit = require('lodash/omit');
const _pickBy = require('lodash/pickBy');
const _isNull = require('lodash/isNull');
const _isUndefined = require('lodash/isUndefined');

module.exports = (sequelize, DataTypes) => {
    const MediaComment = sequelize.define(
        'MediaComment',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            text: {
                type: DataTypes.TEXT
            }
        },
        {
            tableName: 'mediaComment',
            timestamps: true
        }
    );

    MediaComment.prototype.fields = async function(models, params) {
        let model = this.get();

        let hiddenFields = ['userId'];

        _set(model, 'attachment', this.attachment);
        _set(model, 'link', this.link);
        _set(model, 'user', this.user);

        return _pickBy(_omit(model, hiddenFields), v => !_isUndefined(v) && !_isNull(v));
    };

    MediaComment.associate = models => {
        MediaComment.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });

        MediaComment.belongsTo(models.Media, {
            as: 'media',
            foreignKey: 'mediaId'
        });

        MediaComment.hasOne(models.MediaCommentLink, {
            as: 'link',
            foreignKey: 'mediaCommentId'
        });

        MediaComment.hasOne(models.MediaCommentAttachment, {
            as: 'attachment',
            foreignKey: 'mediaCommentId'
        });
    };

    MediaComment.addScopes = models => {
        MediaComment.addScope('count', (userId, mediaId) => {
            return {
                where: {
                    mediaId
                },
                include: [
                    {
                        model: models.User,
                        as: 'users',
                        attributes: []
                    }
                ]
            };
        });

        MediaComment.addScope('expand', (userId, mediaId) => {
            return {
                subQuery: false,
                where: {
                    mediaId,
                    $or: [
                        {
                            '$media.userId$': userId
                        },
                        {
                            $or: [
                                {
                                    userId
                                },
                                {
                                    '$user.id$': userId
                                },
                                {
                                    $and: [
                                        sequelize.where(
                                            sequelize.col('media.userId'),
                                            '=',
                                            sequelize.col('MediaComment.userId')
                                        )
                                    ]
                                }
                            ]
                        }
                    ]
                },
                include: [
                    {
                        model: models.User,
                        as: 'user',
                        include: [
                            {
                                model: models.UserProfile,
                                as: 'profile'
                            }
                        ]
                    },
                    {
                        model: models.MediaCommentLink,
                        as: 'link'
                    },
                    {
                        model: models.MediaCommentAttachment,
                        as: 'attachment'
                    },
                    {
                        model: models.Media,
                        as: 'media',
                        attributes: []
                    }
                ],
                order: [['createdAt', 'DESC']]
            };
        });
    };

    return MediaComment;
};
