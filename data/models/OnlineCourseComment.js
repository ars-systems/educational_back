'use strict';

const _set = require('lodash/set');
const _omit = require('lodash/omit');
const _pickBy = require('lodash/pickBy');
const _isNull = require('lodash/isNull');
const _isUndefined = require('lodash/isUndefined');

module.exports = (sequelize, DataTypes) => {
    const OnlineCourseComment = sequelize.define(
        'OnlineCourseComment',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            text: {
                type: DataTypes.TEXT
            }
        },
        {
            tableName: 'onlineCourseComment',
            timestamps: true
        }
    );

    OnlineCourseComment.prototype.fields = async function(models, params) {
        let model = this.get();

        let hiddenFields = ['onlineCourseId', 'userId'];

        _set(model, 'attachment', this.attachment);
        _set(model, 'link', this.link);
        _set(model, 'user', this.user);

        return _pickBy(_omit(model, hiddenFields), v => !_isUndefined(v) && !_isNull(v));
    };

    OnlineCourseComment.associate = models => {
        OnlineCourseComment.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });

        OnlineCourseComment.belongsTo(models.OnlineCourse, {
            as: 'onlineCourse',
            foreignKey: 'onlineCourseId'
        });

        OnlineCourseComment.belongsTo(models.OnlineCourseMedia, {
            as: 'onlineCourseMedia',
            foreignKey: 'onlineCourseId'
        });

        OnlineCourseComment.hasOne(models.OnlineCourseCommentLink, {
            as: 'link',
            foreignKey: 'onlineCourseCommentId'
        });

        OnlineCourseComment.hasOne(models.OnlineCourseCommentAttachment, {
            as: 'attachment',
            foreignKey: 'onlineCourseCommentId'
        });

        OnlineCourseComment.hasMany(models.OnlineCourseCommentUser, {
            as: 'users',
            foreignKey: 'commentId'
        });
    };

    OnlineCourseComment.addScopes = models => {
        OnlineCourseComment.addScope('count', (userId, courseId) => {
            return {
                where: {
                    courseId
                },
                include: [
                    {
                        model: models.OnlineCourseCommentUser,
                        as: 'users',
                        attributes: []
                    }
                ]
            };
        });

        OnlineCourseComment.addScope('expand', (userId, courseId) => {
            return {
                subQuery: false,
                where: {
                    courseId,
                    $or: [
                        {
                            '$onlineCourseMedia.userId$': userId
                        },
                        {
                            $or: [
                                {
                                    userId
                                },
                                {
                                    '$users.id$': userId
                                },
                                {
                                    $and: [
                                        sequelize.where(
                                            sequelize.col('onlineCourseMedia.userId'),
                                            '=',
                                            sequelize.col('OnlineCourseComment.userId')
                                        )
                                    ]
                                }
                            ]
                        }
                    ]
                },
                include: [
                    {
                        model: models.User,
                        as: 'user',
                        include: [
                            {
                                model: models.UserProfile,
                                as: 'profile'
                            }
                        ]
                    },
                    {
                        model: models.OnlineCourseCommentLink,
                        as: 'link'
                    },
                    {
                        model: models.OnlineCourseCommentAttachment,
                        as: 'attachment'
                    },
                    {
                        model: models.OnlineCourseMedia,
                        as: 'onlineCourseMedia',
                        attributes: []
                    },
                    {
                        model: models.OnlineCourseCommentUser,
                        as: 'users',
                        attributes: []
                    }
                ],
                order: [['createdAt', 'DESC']]
            };
        });
    };

    return OnlineCourseComment;
};
