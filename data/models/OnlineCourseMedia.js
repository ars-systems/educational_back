'use strict';

const _set = require('lodash/set');
const _trim = require('lodash/trim');
const _omit = require('lodash/omit');
const _isEmpty = require('lodash/isEmpty');

const { OnlineCourseMediaType } = require('../../constants');

module.exports = (sequelize, DataTypes) => {
    const OnlineCourseMedia = sequelize.define(
        'OnlineCourseMedia',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            text: {
                type: DataTypes.TEXT,
                validate: {
                    len: [0, 20000]
                }
            },
            onlineCourseId: {
                type: DataTypes.BIGINT
            },
            typeId: {
                allowNull: false,
                type: DataTypes.SMALLINT,
                defaultValue: OnlineCourseMediaType.NO_CONTENT,
                validate: {
                    isIn: [Object.values(OnlineCourseMediaType)]
                }
            }
        },
        {
            tableName: 'onlineCourseMedia',
            timestamps: true,
            setterMethods: {
                text(value) {
                    this.setDataValue('text', value ? _trim(value) : null);
                }
            }
        }
    );

    OnlineCourseMedia.filters = onlineCourseId => {
        return {
            all: {
                onlineCourseId
            },
            text: {
                onlineCourseId,
                typeId: OnlineCourseMediaType.TEXT
            },
            file: {
                onlineCourseId,
                typeId: OnlineCourseMediaType.FILE
            },
            link: {
                onlineCourseId,
                typeId: OnlineCourseMediaType.LINK
            },
            live: {
                onlineCourseId,
                typeId: OnlineCourseMediaType.LIVE
            }
        };
    };

    OnlineCourseMedia.associate = models => {
        OnlineCourseMedia.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });

        OnlineCourseMedia.belongsTo(models.OnlineCourse, {
            as: 'onlineCourse',
            foreignKey: 'onlineCourseId'
        });

        OnlineCourseMedia.hasOne(models.OnlineCourseLink, {
            as: 'link',
            foreignKey: 'onlineCourseId'
        });

        OnlineCourseMedia.hasMany(models.OnlineCourseAttachment, {
            as: 'attachment',
            foreignKey: 'onlineCourseId'
        });

        OnlineCourseMedia.hasMany(models.OnlineCourseComment, {
            as: 'comments',
            foreignKey: 'onlineCourseId'
        });

        OnlineCourseMedia.hasMany(models.UserConnectOnlineCourse, {
            as: 'connectOnlineCourse',
            foreignKey: 'onlineCourseId'
        });
    };

    OnlineCourseMedia.addScopes = models => {
        OnlineCourseMedia.addScope('expand', (userId, filterCondition = {}) => {
            return {
                where: filterCondition,
                subQuery: false,
                include: [
                    {
                        model: models.User,
                        as: 'user',
                        include: [
                            {
                                model: models.UserProfile,
                                as: 'profile'
                            }
                        ]
                    },
                    {
                        model: models.OnlineCourseLink,
                        as: 'link'
                    },
                    {
                        model: models.OnlineCourseAttachment,
                        as: 'attachment'
                    }
                ]
            };
        });
    };

    OnlineCourseMedia.prototype.fields = async function() {
        let model = this.get();

        let hiddenFields = ['userId', 'users'];

        if (!_isEmpty(this.attachment)) {
            model.attachment = this.attachment;
        } else {
            hiddenFields.push('attachment');
        }
        _set(model, 'link', this.link);

        _set(model, 'user', this.user);
        if (this.comment) {
            _set(model, 'comments', this.comment);
        }
        _set(model, 'comments.count', await this.countComments());

        return _omit(model, hiddenFields);
    };

    return OnlineCourseMedia;
};
