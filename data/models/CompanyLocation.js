'use strict';

const _trim = require('lodash/trim');
const _omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
    const CompanyLocation = sequelize.define(
        'CompanyLocation',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            firstAddress: {
                allowNull: false,
                type: DataTypes.STRING
            },
            secondAddress: {
                type: DataTypes.STRING
            },
            country: {
                allowNull: false,
                type: DataTypes.STRING
            },
            city: {
                allowNull: false,
                type: DataTypes.STRING
            },
            state: {
                allowNull: false,
                type: DataTypes.STRING
            },
            zipCode: {
                allowNull: false,
                type: DataTypes.STRING
            },
            latitude: {
                type: DataTypes.DECIMAL(9, 6)
            },
            longitude: {
                type: DataTypes.DECIMAL(9, 6)
            }
        },
        {
            tableName: 'companyLocation',
            timestamps: true,
            setterMethods: {
                address(value) {
                    this.setDataValue('address', value ? _trim(value) : null);
                },
                city(value) {
                    this.setDataValue('city', value ? _trim(value) : null);
                },
                zipCode(value) {
                    this.setDataValue('zipCode', value ? _trim(value) : null);
                }
            }
        }
    );

    CompanyLocation.associate = models => {
        CompanyLocation.belongsTo(models.Company, {
            as: 'company',
            foreignKey: 'companyId'
        });
    };

    CompanyLocation.prototype.fields = async function() {
        return _omit(this.get(), ['id', 'companyId', 'createdAt', 'updatedAt']);
    };

    return CompanyLocation;
};
