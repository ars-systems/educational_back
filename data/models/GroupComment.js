'use strict';

const _set = require('lodash/set');
const _omit = require('lodash/omit');
const _pickBy = require('lodash/pickBy');
const _isNull = require('lodash/isNull');
const _isUndefined = require('lodash/isUndefined');

module.exports = (sequelize, DataTypes) => {
    const GroupComment = sequelize.define(
        'GroupComment',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            text: {
                type: DataTypes.TEXT
            }
        },
        {
            tableName: 'groupComment',
            timestamps: true
        }
    );

    GroupComment.prototype.fields = async function(models, params) {
        let model = this.get();

        let hiddenFields = ['groupId', 'userId'];

        _set(model, 'attachment', this.attachment);
        _set(model, 'link', this.link);
        _set(model, 'user', this.user);

        return _pickBy(_omit(model, hiddenFields), v => !_isUndefined(v) && !_isNull(v));
    };

    GroupComment.associate = models => {
        GroupComment.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });

        GroupComment.belongsTo(models.Group, {
            as: 'group',
            foreignKey: 'groupId'
        });

        GroupComment.belongsTo(models.GroupMedia, {
            as: 'groupMedia',
            foreignKey: 'groupId'
        });

        GroupComment.hasOne(models.GroupCommentLink, {
            as: 'link',
            foreignKey: 'groupCommentId'
        });

        GroupComment.hasOne(models.GroupCommentAttachment, {
            as: 'attachment',
            foreignKey: 'groupCommentId'
        });

        GroupComment.hasMany(models.GroupCommentUser, {
            as: 'users',
            foreignKey: 'commentId'
        });
    };

    GroupComment.addScopes = models => {
        GroupComment.addScope('count', (userId, groupId) => {
            return {
                where: {
                    groupId
                },
                include: [
                    {
                        model: models.GroupCommentUser,
                        as: 'users',
                        attributes: []
                    }
                ]
            };
        });

        GroupComment.addScope('expand', (userId, groupId) => {
            return {
                subQuery: false,
                where: {
                    groupId,
                    $or: [
                        {
                            '$groupMedia.userId$': userId
                        },
                        {
                            $or: [
                                {
                                    userId
                                },
                                {
                                    '$users.id$': userId
                                },
                                {
                                    $and: [
                                        sequelize.where(
                                            sequelize.col('groupMedia.userId'),
                                            '=',
                                            sequelize.col('GroupComment.userId')
                                        )
                                    ]
                                }
                            ]
                        }
                    ]
                },
                include: [
                    {
                        model: models.User,
                        as: 'user',
                        include: [
                            {
                                model: models.UserProfile,
                                as: 'profile'
                            }
                        ]
                    },
                    {
                        model: models.GroupCommentLink,
                        as: 'link'
                    },
                    {
                        model: models.GroupCommentAttachment,
                        as: 'attachment'
                    },
                    {
                        model: models.GroupMedia,
                        as: 'groupMedia',
                        attributes: []
                    },
                    {
                        model: models.GroupCommentUser,
                        as: 'users',
                        attributes: []
                    }
                ],
                order: [['createdAt', 'DESC']]
            };
        });
    };

    return GroupComment;
};
