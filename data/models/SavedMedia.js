'use strict';

const _omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
    const SavedMedia = sequelize.define(
        'SavedMedia',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            userId: {
                type: DataTypes.BIGINT
            },
            mediaId: {
                type: DataTypes.BIGINT
            }
        },
        {
            tableName: 'savedMedia',
            timestamps: true,
            setterMethods: {}
        }
    );

    SavedMedia.associate = models => {};

    SavedMedia.addScopes = models => {};

    SavedMedia.prototype.fields = function() {
        const model = this.get();
        const hiddenFields = ['createdAt', 'updatedAt'];

        return _omit(model, hiddenFields);
    };

    return SavedMedia;
};
