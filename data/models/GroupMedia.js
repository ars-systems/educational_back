'use strict';

const _set = require('lodash/set');
const _has = require('lodash/has');
const _trim = require('lodash/trim');
const _omit = require('lodash/omit');
const _isEmpty = require('lodash/isEmpty');

const { GroupMediaType } = require('../../constants');

module.exports = (sequelize, DataTypes) => {
    const GroupMedia = sequelize.define(
        'GroupMedia',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            text: {
                type: DataTypes.TEXT,
                validate: {
                    len: [0, 20000]
                }
            },
            groupId: {
                type: DataTypes.BIGINT
            },
            typeId: {
                allowNull: false,
                type: DataTypes.SMALLINT,
                defaultValue: GroupMediaType.NO_CONTENT,
                validate: {
                    isIn: [Object.values(GroupMediaType)]
                }
            }
        },
        {
            tableName: 'groupMedia',
            timestamps: true,
            setterMethods: {
                text(value) {
                    this.setDataValue('text', value ? _trim(value) : null);
                }
            }
        }
    );

    GroupMedia.filters = groupId => {
        return {
            all: {
                groupId
            },
            text: {
                groupId,
                typeId: GroupMediaType.TEXT
            },
            file: {
                groupId,
                typeId: GroupMediaType.FILE
            },
            link: {
                groupId,
                typeId: GroupMediaType.LINK
            }
        };
    };

    GroupMedia.associate = models => {
        GroupMedia.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });

        GroupMedia.belongsTo(models.Group, {
            as: 'group',
            foreignKey: 'groupId'
        });

        GroupMedia.belongsTo(GroupMedia, {
            as: 'parent',
            foreignKey: 'parentId'
        });

        GroupMedia.belongsTo(models.GroupMedia, {
            as: 'root',
            foreignKey: 'rootId'
        });

        GroupMedia.hasOne(models.GroupLink, {
            as: 'link',
            foreignKey: 'groupId'
        });

        GroupMedia.hasMany(models.GroupAttachment, {
            as: 'attachment',
            foreignKey: 'groupId'
        });

        GroupMedia.hasMany(models.GroupComment, {
            as: 'comments',
            foreignKey: 'groupId'
        });

        GroupMedia.hasMany(models.UserConnectGroup, {
            as: 'connectGroups',
            foreignKey: 'groupId'
        });
    };

    GroupMedia.addScopes = models => {
        GroupMedia.addScope('expand', (userId, filterCondition = {}) => {
            return {
                where: filterCondition,
                subQuery: false,
                include: [
                    {
                        model: models.User,
                        as: 'user',
                        include: [
                            {
                                model: models.UserProfile,
                                as: 'profile'
                            }
                        ]
                    },
                    {
                        model: models.GroupMedia,
                        as: 'root',
                        include: [
                            {
                                model: models.User,
                                as: 'user',
                                include: [
                                    {
                                        model: models.UserProfile,
                                        as: 'profile'
                                    }
                                ]
                            },
                            {
                                model: models.GroupLink,
                                as: 'link'
                            },
                            {
                                model: models.GroupAttachment,
                                as: 'attachment'
                            }
                        ]
                    },
                    {
                        model: models.GroupLink,
                        as: 'link'
                    },
                    {
                        model: models.GroupAttachment,
                        as: 'attachment'
                    }
                ]
            };
        });
    };

    GroupMedia.prototype.fields = async function(models, params) {
        let model = this.get();

        let hiddenFields = ['userId', 'users', 'root', 'parentId', 'rootId'];

        if (_isEmpty(model.root)) {
            if (!_isEmpty(this.attachment)) {
                model.attachment = this.attachment;
            } else {
                hiddenFields.push('attachment');
            }
            _set(model, 'link', this.link);
        } else {
            _set(model, 'id', this.id);
            _set(model, 'typeId', this.root.typeId);

            if (!_isEmpty(this.root.attachment)) {
                model.attachment = this.root.attachment;
            } else {
                hiddenFields.push('attachment');
            }

            _set(model, 'link', this.root.link);
        }

        if (_has(this, 'root.user') && this.user.id !== this.root.user.id) {
            _set(model, 'owner', this.root.user);
        }

        _set(model, 'user', this.user);
        if (this.comment) {
            _set(model, 'comments', this.comment);
        }
        _set(model, 'comments.count', await this.countComments());

        return _omit(model, hiddenFields);
    };

    return GroupMedia;
};
