'use strict';

const _omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
    const GroupAttachment = sequelize.define(
        'GroupAttachment',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            key: {
                type: DataTypes.STRING
            }
        },
        {
            tableName: 'groupAttachment',
            timestamps: true,
            setterMethods: {}
        }
    );

    GroupAttachment.associate = models => {
        GroupAttachment.belongsTo(models.GroupMedia, {
            as: 'group',
            foreignKey: 'groupId'
        });
    };

    GroupAttachment.prototype.fields = async function() {
        const model = this.get();

        return _omit(model, ['groupId', 'createdAt', 'updatedAt']);
    };

    return GroupAttachment;
};
