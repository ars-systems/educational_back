'use strict';

const _trim = require('lodash/trim');
const _omit = require('lodash/omit');
const validator = require('validator');
const _pickBy = require('lodash/pickBy');
const _identity = require('lodash/identity');

module.exports = (sequelize, DataTypes) => {
    const OnlineCourseCommentLink = sequelize.define(
        'OnlineCourseCommentLink',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            url: {
                type: DataTypes.TEXT,
                allowNull: false,
                validate: {
                    isImage(value) {
                        if (value && !validator.isURL(value)) {
                            throw new Error('Link url is not valid');
                        }
                    }
                }
            },
            title: {
                type: DataTypes.STRING
            },
            description: {
                type: DataTypes.TEXT
            },
            image: {
                type: DataTypes.TEXT,
                validate: {
                    isImage(value) {
                        if (value && !validator.isURL(value)) {
                            throw new Error('Link image is not valid');
                        }
                    }
                }
            }
        },
        {
            tableName: 'onlineCourseCommentLink',
            timestamps: true,
            setterMethods: {
                title(value) {
                    this.setDataValue('title', _trim(value));
                },
                description(value) {
                    this.setDataValue('description', _trim(value));
                }
            }
        }
    );

    OnlineCourseCommentLink.prototype.fields = async function() {
        return _pickBy(_omit(this.get(), ['id', 'onlineCourseCommentId', 'createdAt', 'updatedAt']), _identity);
    };

    OnlineCourseCommentLink.associate = models => {
        OnlineCourseCommentLink.belongsTo(models.OnlineCourseComment, {
            as: 'onlineCourseComment',
            foreignKey: 'onlineCourseCommentId'
        });
    };

    return OnlineCourseCommentLink;
};
