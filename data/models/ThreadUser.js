'use strict';

module.exports = (sequelize, DataTypes) => {
    const ThreadUser = sequelize.define(
        'ThreadUser',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            seeFrom: {
                allowNull: false,
                type: DataTypes.DATE
            }
        },
        {
            tableName: 'threadUser',
            timestamps: true
        }
    );

    ThreadUser.associate = models => {
        ThreadUser.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });

        ThreadUser.belongsTo(models.Thread, {
            as: 'thread',
            foreignKey: 'threadId'
        });
    };

    return ThreadUser;
};
