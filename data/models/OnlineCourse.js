'use strict';

const _set = require('lodash/set');
const _omit = require('lodash/omit');
const _isEmpty = require('lodash/isEmpty');

const { OnlineCourseCategory, OnlineCourseType } = require('./../../constants');

module.exports = (sequelize, DataTypes) => {
    const OnlineCourse = sequelize.define(
        'OnlineCourse',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            name: {
                allowNull: false,
                type: DataTypes.STRING
            },
            image: {
                type: DataTypes.STRING
            },
            description: {
                allowNull: false,
                type: DataTypes.STRING
            },
            availability: {
                type: DataTypes.BOOLEAN
            },
            category: {
                allowNull: false,
                type: DataTypes.SMALLINT,
                validate: {
                    isIn: [Object.values(OnlineCourseCategory)]
                }
            },
            type: {
                allowNull: false,
                type: DataTypes.SMALLINT,
                validate: {
                    isIn: [Object.values(OnlineCourseType)]
                }
            },
            duration: {
                type: DataTypes.SMALLINT
            },
            participantsCount: {
                type: DataTypes.SMALLINT
            },
            startDate: {
                type: DataTypes.DATE,
                validate: {
                    customValidator(value) {
                        if (value <= new Date(Date.now() + 3 * 86400000)) {
                            throw new Error('Online course is required at least 3 days (72 hours) before.');
                        }
                    }
                }
            },
            userId: {
                type: DataTypes.BIGINT
            },
            status: {
                type: DataTypes.SMALLINT
            }
        },
        {
            tableName: 'onlineCourse',
            timestamps: true,
            setterMethods: {}
        }
    );

    OnlineCourse.associate = models => {
        OnlineCourse.belongsTo(models.User, {
            as: 'creator',
            foreignKey: 'userId'
        });

        OnlineCourse.hasMany(models.Media, {
            as: 'medias',
            foreignKey: 'id'
        });
    };

    OnlineCourse.addScopes = models => {};

    OnlineCourse.prototype.fields = async function() {
        const model = this.get();
        const hiddenFields = ['createdAt', 'updatedAt'];

        const rates = await this.getRate();
        let rateAvg = 0;

        if (!_isEmpty(rates)) {
            let sum = rates.reduce((previous, current) => (current.rate += previous.rate));
            rateAvg = sum / rates.length;
        }
        _set(model, 'rate', rateAvg);

        return _omit(model, hiddenFields);
    };

    return OnlineCourse;
};
