'use strict';

const _omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
    const MessageRecipient = sequelize.define(
        'MessageRecipient',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            isSeen: {
                defaultValue: false,
                type: DataTypes.BOOLEAN
            },
            isRead: {
                type: DataTypes.BOOLEAN,
                defaultValue: false
            }
        },
        {
            tableName: 'messageRecipient',
            timestamps: true
        }
    );

    MessageRecipient.associate = models => {
        MessageRecipient.belongsTo(models.Message, {
            as: 'message',
            foreignKey: 'messageId'
        });

        MessageRecipient.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });

        MessageRecipient.belongsTo(models.Thread, {
            as: 'thread',
            foreignKey: 'threadId'
        });
    };

    MessageRecipient.prototype.fields = async function() {
        const model = this.get();
        const hiddenFields = ['typeId', 'threadId', 'updatedAt', 'messageId', 'createdAt'];

        return _omit(model, hiddenFields);
    };

    return MessageRecipient;
};
