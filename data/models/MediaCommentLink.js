'use strict';

const _trim = require('lodash/trim');
const _omit = require('lodash/omit');
const validator = require('validator');
const _pickBy = require('lodash/pickBy');
const _identity = require('lodash/identity');

module.exports = (sequelize, DataTypes) => {
    const MediaCommentLink = sequelize.define(
        'MediaCommentLink',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            url: {
                type: DataTypes.TEXT,
                allowNull: false,
                validate: {
                    isImage(value) {
                        if (value && !validator.isURL(value)) {
                            throw new Error('Link url is not valid');
                        }
                    }
                }
            },
            title: {
                type: DataTypes.STRING
            },
            description: {
                type: DataTypes.TEXT
            },
            image: {
                type: DataTypes.TEXT,
                validate: {
                    isImage(value) {
                        if (value && !validator.isURL(value)) {
                            throw new Error('Link image is not valid');
                        }
                    }
                }
            }
        },
        {
            tableName: 'mediaCommentLink',
            timestamps: true,
            setterMethods: {
                title(value) {
                    this.setDataValue('title', _trim(value));
                },
                description(value) {
                    this.setDataValue('description', _trim(value));
                }
            }
        }
    );

    MediaCommentLink.prototype.fields = async function() {
        return _pickBy(_omit(this.get(), ['id', 'mediaCommentId', 'createdAt', 'updatedAt']), _identity);
    };

    MediaCommentLink.associate = models => {
        MediaCommentLink.belongsTo(models.MediaComment, {
            as: 'mediaComment',
            foreignKey: 'mediaCommentId'
        });
    };

    return MediaCommentLink;
};
