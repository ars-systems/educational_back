'use strict';

const _trim = require('lodash/trim');
const _omit = require('lodash/omit');
const validator = require('validator');

module.exports = (sequelize, DataTypes) => {
    const OnlineCourseLink = sequelize.define(
        'OnlineCourseLink',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            url: {
                type: DataTypes.TEXT,
                allowNull: false,
                validate: {
                    isImage(value) {
                        if (value && !validator.isURL(value)) {
                            throw new Error('Link url is not valid.');
                        }
                    }
                }
            },
            title: {
                type: DataTypes.STRING
            },
            description: {
                type: DataTypes.TEXT
            },
            image: {
                type: DataTypes.TEXT,
                validate: {
                    isImage(value) {
                        if (value && !validator.isURL(value)) {
                            throw new Error('Link image is not valid.');
                        }
                    }
                }
            }
        },
        {
            tableName: 'onlineCourseLink',
            timestamps: true,
            setterMethods: {
                url(value) {
                    this.setDataValue('url', _trim(value));
                },
                title(value) {
                    this.setDataValue('title', _trim(value));
                },
                description(value) {
                    this.setDataValue('description', _trim(value));
                },
                image(value) {
                    this.setDataValue('image', _trim(value));
                }
            }
        }
    );

    OnlineCourseLink.associate = models => {
        OnlineCourseLink.belongsTo(models.OnlineCourse, {
            as: 'onlineCourse',
            foreignKey: 'onlineCourseId'
        });
    };

    OnlineCourseLink.prototype.fields = function() {
        return _omit(this.get(), ['id', 'onlineCourseId', 'createdAt', 'updatedAt']);
    };

    return OnlineCourseLink;
};
