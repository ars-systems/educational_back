'use strict';

const _omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
    const UserConnect = sequelize.define(
        'UserConnect',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            }
        },
        {
            tableName: 'userConnect',
            timestamps: true
        }
    );

    UserConnect.associate = models => {
        UserConnect.belongsTo(models.User, { as: 'sender' });

        UserConnect.belongsTo(models.User, { as: 'receiver' });

        UserConnect.belongsTo(models.User, { as: 'connect', foreignKey: 'receiverId' });
    };

    UserConnect.addScopes = models => {
        UserConnect.addScope('connections', senderId => {
            return {
                where: { senderId },
                include: [
                    {
                        model: models.User,
                        as: 'connect',
                        attributes: ['id', 'email'],
                        include: [{ model: models.UserProfile, as: 'profile' }]
                    }
                ]
            };
        });
    };

    UserConnect.prototype.fields = async function() {
        const model = this.get();
        const hiddenFields = ['id', 'createdAt', 'updatedAt'];

        return _omit(model, hiddenFields);
    };

    return UserConnect;
};
