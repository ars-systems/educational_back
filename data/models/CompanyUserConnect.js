'use strict';

const _omit = require('lodash/omit');

const { CompanyRole, UserConnectStatusType } = require('../../constants');

module.exports = (sequelize, DataTypes) => {
    const CompanyUserConnect = sequelize.define(
        'CompanyUserConnect',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            roleId: {
                type: DataTypes.SMALLINT,
                defaultValue: CompanyRole.MEMBER,
                validate: {
                    isIn: [Object.values(CompanyRole)]
                }
            },
            statusId: {
                type: DataTypes.SMALLINT,
                defaultValue: UserConnectStatusType.ACCEPTED,
                validate: {
                    isIn: [Object.values(UserConnectStatusType)]
                }
            }
        },
        {
            tableName: 'companyUserConnect',
            timestamps: true
        }
    );

    CompanyUserConnect.associate = models => {
        CompanyUserConnect.belongsTo(models.Company, {
            as: 'company',
            foreignKey: 'companyId'
        });

        CompanyUserConnect.belongsTo(models.CompanyConnect, {
            as: 'user',
            foreignKey: 'userId'
        });

        CompanyUserConnect.belongsTo(models.User, {
            as: 'receiver',
            foreignKey: 'userId'
        });

        CompanyUserConnect.belongsTo(models.CompanyConnect, {
            as: 'companyConnect',
            foreignKey: 'connectId'
        });
    };

    CompanyUserConnect.addScopes = models => {
        CompanyUserConnect.addScope('connects', (userId, companyId, queryString = {}) => {
            return {
                subQuery: false,
                where: {
                    companyId,
                    statusId: UserConnectStatusType.ACCEPTED,
                    ...queryString
                },
                include: [
                    {
                        model: models.User,
                        as: 'receiver',
                        include: [
                            {
                                model: models.UserProfile,
                                as: 'profile'
                            },
                            {
                                model: models.UserWork,
                                as: 'work',
                                attributes: ['industry', 'occupation'],
                                include: [
                                    {
                                        model: models.Company,
                                        as: 'company',
                                        attributes: ['name']
                                    }
                                ]
                            }
                        ]
                    }
                ]
            };
        });

        CompanyUserConnect.addScope('disconnects', (userId, companyId, queryString = {}) => {
            return {
                subQuery: false,
                where: {
                    companyId,
                    statusId: UserConnectStatusType.DISCONNECTED,
                    ...queryString
                },
                include: [
                    {
                        model: models.Company,
                        as: 'company',
                        attributes: ['name']
                    },
                    {
                        model: models.User,
                        as: 'receiver',
                        include: [
                            {
                                model: models.UserProfile,
                                as: 'profile'
                            },
                            {
                                model: models.UserWork,
                                as: 'work',
                                attributes: ['industry', 'occupation'],
                                include: [
                                    {
                                        model: models.Company,
                                        as: 'company',
                                        attributes: ['name']
                                    }
                                ]
                            }
                        ]
                    }
                ]
            };
        });
    };

    CompanyUserConnect.prototype.fields = async function() {
        return _omit(this.get(), ['id', 'userId', 'statusId', 'createdAt', 'updatedAt', 'connectId', 'companyId']);
    };

    return CompanyUserConnect;
};
