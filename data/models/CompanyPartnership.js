'use strict';

const _omit = require('lodash/omit');

const { CompanyConnectStatusType } = require('../../constants');

module.exports = (sequelize, DataTypes) => {
    const CompanyPartnership = sequelize.define(
        'CompanyPartnership',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            statusId: {
                type: DataTypes.SMALLINT,
                defaultValue: CompanyConnectStatusType.ACCEPTED,
                validate: {
                    isIn: [Object.values(CompanyConnectStatusType)]
                }
            }
        },
        {
            tableName: 'companyPartnership',
            timestamps: true
        }
    );

    CompanyPartnership.associate = models => {
        CompanyPartnership.belongsTo(models.CompanyConnectPartnership, {
            as: 'connectPartnership',
            foreignKey: 'connectPartnershipId'
        });

        CompanyPartnership.belongsTo(models.Company, {
            as: 'company',
            foreignKey: 'companyId'
        });
    };

    CompanyPartnership.prototype.fields = async function() {
        return _omit(this.get(), []);
    };

    return CompanyPartnership;
};
