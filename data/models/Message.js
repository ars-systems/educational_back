'use strict';

const _ = require('lodash');

const { MessageType } = require('../../constants');

module.exports = (sequelize, DataTypes) => {
    const Message = sequelize.define(
        'Message',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            text: {
                type: DataTypes.TEXT,
                validate: {
                    len: [0, 20000]
                }
            },
            typeId: {
                allowNull: false,
                type: DataTypes.SMALLINT,
                validate: {
                    isIn: [Object.values(MessageType)]
                },
                set(value) {
                    this.setDataValue('typeId', _.toNumber(value));
                }
            },
            index: {
                defaultValue: 0,
                type: DataTypes.INTEGER
            }
        },
        {
            tableName: 'message',
            timestamps: true,
            setterMethods: {
                text(value) {
                    this.setDataValue('text', value ? _.trim(value) : null);
                }
            }
        }
    );

    Message.associate = models => {
        Message.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });

        Message.belongsTo(models.Thread, {
            as: 'thread',
            foreignKey: 'threadId'
        });

        Message.hasOne(models.Media, {
            as: 'media',
            foreignKey: 'messageId'
        });

        Message.hasMany(models.MessageRecipient, {
            as: 'recipients',
            foreignKey: 'messageId'
        });
    };

    Message.addScopes = models => {
        const include = [
            {
                model: models.User,
                as: 'user',
                attributes: ['id', 'email'],
                include: [{ model: models.UserProfile, as: 'profile', attributes: ['firstName'] }]
            },
            {
                model: models.Media,
                as: 'media'
            }
        ];

        Message.addScope('expand', userId => {
            return {
                where: {
                    id: {
                        $notIn: models.MessageRecipient.generateNestedQuery({
                            attributes: ['messageId'],
                            where: { userId }
                        })
                    }
                },
                replacements: { userId },
                include
            };
        });

        Message.addScope('messages', userId => {
            return {
                include
            };
        });
    };

    Message.prototype.fields = async function() {
        const model = this.get();
        const hiddenFields = ['userId', 'recipients', 'updatedAt'];

        return _.omit(model, hiddenFields);
    };

    return Message;
};
