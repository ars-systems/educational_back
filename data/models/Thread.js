'use strict';

const _ = require('lodash');

const { ConnectStatus } = require('../../constants');

module.exports = (sequelize, DataTypes) => {
    const Thread = sequelize.define(
        'Thread',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            }
        },
        {
            tableName: 'thread',
            timestamps: true
        }
    );

    Thread.associate = models => {
        Thread.belongsTo(models.Message, {
            as: 'lastMessage',
            foreignKey: 'lastMessageId'
        });

        Thread.hasOne(models.ThreadRequest, {
            as: 'request',
            foreignKey: 'threadId'
        });

        Thread.hasMany(models.ThreadUser, {
            as: 'users',
            foreignKey: 'threadId'
        });

        Thread.hasMany(models.Message, {
            as: 'messages',
            foreignKey: 'threadId'
        });

        Thread.hasMany(models.MessageRecipient, {
            as: 'recipient',
            foreignKey: 'threadId'
        });
    };

    Thread.addScopes = models => {
        const include = [
            {
                model: models.ThreadRequest,
                as: 'request'
            },
            {
                model: models.Message,
                as: 'lastMessage',
                include: [
                    {
                        model: models.User,
                        as: 'user',
                        include: [{ model: models.UserProfile, as: 'profile', attributes: ['firstName'] }]
                    },
                    {
                        model: models.Media,
                        as: 'media'
                    }
                ]
            }
        ];

        Thread.addScope('available', userId => {
            const requiredCondition = {
                id: {
                    $in: models.ThreadUser.generateNestedQuery({
                        attributes: ['threadId'],
                        where: { userId }
                    })
                }
            };

            return {
                where: { $and: [requiredCondition] },
                replacements: { userId },
                include
            };
        });

        Thread.addScope('main', (userId, unSeen = false, searchQuery = {}) => {
            const requiredCondition = {
                $and: [
                    {
                        id: {
                            $in: models.ThreadUser.generateNestedQuery({
                                attributes: ['threadId'],
                                where: { userId }
                            })
                        }
                    }
                ]
            };

            const exceptionCondition = {
                $or: [
                    { $request$: { $is: null } },
                    {
                        $or: [
                            {
                                '$request.statusId$': ConnectStatus.ACCEPTED
                            },
                            {
                                '$request.statusId$': ConnectStatus.REJECTED,
                                '$request.receiverId$': { $ne: userId }
                            }
                        ]
                    }
                ]
            };

            let unSeenCondition = {};
            if (unSeen) {
                unSeenCondition = {
                    lastMessageId: {
                        $in: models.MessageRecipient.generateNestedQuery({
                            attributes: ['messageId'],
                            where: { isSeen: false, userId }
                        })
                    }
                };
            }

            let searchCondition = {};
            if (!_.isEmpty(searchQuery)) {
                searchCondition = {
                    id: {
                        $in: models.ThreadUser.generateNestedQuery({
                            attributes: ['threadId'],
                            where: {
                                userId: {
                                    $in: models.User.generateNestedQuery({
                                        attributes: ['id'],
                                        where: { $and: [{ id: { $ne: userId } }, searchQuery] }
                                    })
                                }
                            }
                        })
                    }
                };
            }

            return {
                where: { $and: [requiredCondition, exceptionCondition, searchCondition, unSeenCondition] },
                replacements: { userId },
                include
            };
        });

        Thread.addScope('request', (userId, unSeen = false, searchQuery = {}) => {
            const requiredCondition = {
                $and: [
                    {
                        $request$: { $ne: null },
                        '$request.receiverId$': userId,
                        '$request.statusId$': ConnectStatus.REJECTED
                    }
                ]
            };

            let unSeenCondition = {};
            if (unSeen) {
                unSeenCondition = {
                    lastMessageId: {
                        $in: models.MessageRecipient.generateNestedQuery({
                            attributes: ['messageId'],
                            where: { isSeen: false, userId }
                        })
                    }
                };
            }

            let searchCondition = {};
            if (!_.isEmpty(searchQuery)) {
                searchCondition = {
                    id: {
                        $in: models.ThreadUser.generateNestedQuery({
                            attributes: ['threadId'],
                            where: {
                                userId: {
                                    $in: models.User.generateNestedQuery({
                                        attributes: ['id'],
                                        where: { $and: [{ id: { $ne: userId } }, searchQuery] }
                                    })
                                }
                            }
                        })
                    }
                };
            }

            return {
                where: { $and: [requiredCondition, unSeenCondition, searchCondition] },
                replacements: { userId },
                include
            };
        });
    };

    Thread.prototype.canWrite = function(userId) {
        let hasAccess = true;

        if (!_.isEmpty(this.request)) {
            hasAccess = this.request.senderId === userId || this.request.statusId !== ConnectStatus.REJECTED;
        }

        return hasAccess;
    };

    Thread.prototype.fields = async function(models, params) {
        const model = this.get();
        const { userId } = params;
        const { id: threadId, lastMessageId: messageId } = model;
        const hiddenFields = ['lastMessageId', 'users', 'request', 'updatedAt'];

        if (!(await models.MessageRecipient.count({ where: { messageId, userId } }))) {
            this.lastMessage = await models.Message.scope({ method: ['expand', userId] }).findOne({
                order: [['createdAt', 'DESC']],
                where: { threadId }
            });
        }

        const canWrite = this.canWrite(userId);
        const unreadCount = await this.countRecipient({ where: { userId, isRead: false } });
        const unreadMessagesId = await models.MessageRecipient.findAll({
            where: { threadId: this.id, userId, isRead: false }
        }).map(a => a.id);
        const data = await models.User.scope({
            method: ['expand', userId]
        }).findOne({
            where: {
                id: {
                    $in: models.ThreadUser.generateNestedQuery({
                        where: { threadId, userId: { $ne: userId } },
                        attributes: ['userId']
                    })
                }
            }
        });

        _.set(model, '_meta', { canWrite, unreadCount, unreadMessagesId });
        _.set(model, 'interlocutor', data);
        _.set(model, 'lastMessage', this.lastMessage || null);

        return _.omit(model, hiddenFields);
    };

    return Thread;
};
