'use strict';

const _omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
    const OnlineCourseAttachment = sequelize.define(
        'OnlineCourseAttachment',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            key: {
                type: DataTypes.STRING
            }
        },
        {
            tableName: 'onlineCourseAttachment',
            timestamps: true,
            setterMethods: {}
        }
    );

    OnlineCourseAttachment.associate = models => {
        OnlineCourseAttachment.belongsTo(models.OnlineCourseMedia, {
            as: 'onlineCourse',
            foreignKey: 'onlineCourseId'
        });
    };

    OnlineCourseAttachment.prototype.fields = async function() {
        const model = this.get();

        return _omit(model, ['onlineCourseId', 'createdAt', 'updatedAt']);
    };

    return OnlineCourseAttachment;
};
