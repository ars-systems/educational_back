'use strict';

const _omit = require('lodash/omit');
const _words = require('lodash/words');

const { AccountType } = require('../../constants');

module.exports = (sequelize, DataTypes) => {
    const UserProfile = sequelize.define(
        'UserProfile',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            firstName: {
                allowNull: false,
                type: DataTypes.STRING(50),
                validate: {
                    len: [2, 50]
                }
            },
            lastName: {
                type: DataTypes.STRING(50),
                validate: {
                    len: [2, 50]
                }
            },
            avatar: {
                type: DataTypes.STRING
            },
            university: {
                type: DataTypes.STRING
            },
            profession: {
                type: DataTypes.STRING
            },
            work: {
                type: DataTypes.STRING
            },
            gender: {
                type: DataTypes.BOOLEAN
            },
            typeId: {
                type: DataTypes.SMALLINT,
                validate: {
                    isIn: [Object.values(AccountType)]
                }
            }
        },
        {
            tableName: 'userProfile',
            timestamps: true,
            setterMethods: {
                firstName(value) {
                    this.setDataValue('firstName', value ? _words(value).join(' ') : null);
                },
                lastName(value) {
                    this.setDataValue('lastName', value ? _words(value).join(' ') : null);
                }
            }
        }
    );

    UserProfile.associate = models => {
        UserProfile.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });
    };

    UserProfile.prototype.fields = async function() {
        const model = this.get();

        let hiddenFields = ['id', 'userId', 'createdAt', 'updatedAt'];

        return _omit(model, hiddenFields);
    };

    return UserProfile;
};
