'use strict';

const _get = require('lodash/get');
const _set = require('lodash/set');
const _pick = require('lodash/pick');
const _omit = require('lodash/omit');
const _first = require('lodash/first');
const _isEmpty = require('lodash/isEmpty');

const { MediaType, MediaCategory } = require('../../constants');

module.exports = (sequelize, DataTypes) => {
    const Media = sequelize.define(
        'Media',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            data: {
                type: DataTypes.STRING,
                allowNull: false
            },
            thumbnail: {
                type: DataTypes.STRING
            },
            image: {
                type: DataTypes.STRING
            },
            meta: {
                type: DataTypes.JSON
            },
            name: {
                type: DataTypes.STRING
            },
            sphere: {
                type: DataTypes.STRING
            },
            author: {
                type: DataTypes.STRING
            },
            pageCount: {
                type: DataTypes.INTEGER
            },
            description: {
                type: DataTypes.STRING
            },
            availability: {
                type: DataTypes.BOOLEAN
            },
            typeId: {
                type: DataTypes.SMALLINT,
                validate: {
                    isIn: [Object.values(MediaType)]
                }
            },
            category: {
                type: DataTypes.SMALLINT,
                validate: {
                    isIn: [Object.values(MediaCategory)]
                }
            },
            onlineCourseDate: {
                type: DataTypes.DATE
            }
        },
        {
            tableName: 'media',
            timestamps: true,
            setterMethods: {
                meta(value) {
                    this.setDataValue(
                        'meta',
                        _pick(value, ['size', 'name', 'mime', 'type', 'ext', 'title', 'artist', 'album'])
                    );
                }
            }
        }
    );

    Media.filters = userId => {
        return {
            all: {},
            createdByMe: {
                userId
            },
            saved: {
                $or: {
                    '$connects.userId$': userId,
                    '$connectGroups->group->groupUsers.userId$': userId
                }
            }
        };
    };

    Media.associate = models => {
        Media.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });

        Media.hasMany(models.MediaRead, {
            as: 'read',
            foreignKey: 'mediaId'
        });

        Media.hasMany(models.MediaRead, {
            as: 'status',
            foreignKey: 'mediaId'
        });

        Media.hasMany(models.MediaRate, {
            as: 'rate',
            foreignKey: 'mediaId'
        });

        Media.hasMany(models.MediaComment, {
            as: 'comments',
            foreignKey: 'mediaId'
        });

        Media.belongsTo(models.Message, {
            as: 'message',
            foreignKey: 'messageId'
        });
    };

    Media.addScopes = models => {};

    Media.prototype.fields = async function() {
        const model = this.get();
        const hiddenFields = ['userId', 'updatedAt'];

        if (this.read) {
            _set(model, 'read', this.read);
        }
        _set(model, 'read.count', await this.countRead());
        _set(model, 'commentCount', await this.countComments());

        const rates = await this.getRate();
        let rateAvg = 0;

        if (!_isEmpty(rates)) {
            let sum = rates.reduce((previous, current) => (current.rate += previous.rate));
            rateAvg = sum / rates.length;
        }

        _set(model, 'rate', rateAvg);

        const mediaReadStatus = await this.getStatus({ where: { userId: _get(this, 'user.id') || -1 } });
        if (!_isEmpty(mediaReadStatus)) {
            _set(model, 'read.status', _first(mediaReadStatus).status);
        }

        return _omit(model, hiddenFields);
    };

    return Media;
};
