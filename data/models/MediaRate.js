'use strict';

const _omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
    const MediaRate = sequelize.define(
        'MediaRate',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            userId: {
                type: DataTypes.BIGINT
            },
            mediaId: {
                type: DataTypes.BIGINT
            },
            rate: {
                type: DataTypes.SMALLINT
            }
        },
        {
            tableName: 'mediaRate',
            timestamps: true,
            setterMethods: {}
        }
    );

    MediaRate.associate = models => {
        MediaRate.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });
    };

    MediaRate.addScopes = models => {};

    MediaRate.prototype.fields = function() {
        const model = this.get();
        const hiddenFields = ['createdAt', 'updatedAt'];

        return _omit(model, hiddenFields);
    };

    return MediaRate;
};
