'use strict';

module.exports = (sequelize, DataTypes) => {
    const UserConnectOnlineCourse = sequelize.define(
        'UserConnectOnlineCourse',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            }
        },
        {
            tableName: 'userConnectOnlineCourse',
            timestamps: true
        }
    );

    UserConnectOnlineCourse.associate = models => {
        UserConnectOnlineCourse.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });

        UserConnectOnlineCourse.belongsTo(models.UserConnectOnlineCourse, {
            as: 'connectOnlineCourse',
            foreignKey: 'courseId'
        });

        UserConnectOnlineCourse.hasMany(models.OnlineCourse, {
            as: 'course',
            foreignKey: 'id'
        });
    };

    return UserConnectOnlineCourse;
};
