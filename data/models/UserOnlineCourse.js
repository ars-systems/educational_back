'use strict';

const _omit = require('lodash/omit');

const { GroupConnectionStatus, GroupConnectionType } = require('./../../constants');

module.exports = (sequelize, DataTypes) => {
    const UserOnlineCourse = sequelize.define(
        'UserOnlineCourse',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            status: {
                type: DataTypes.SMALLINT,
                validate: {
                    isIn: [Object.values(GroupConnectionStatus)]
                }
            },
            type: {
                type: DataTypes.SMALLINT,
                validate: {
                    isIn: [Object.values(GroupConnectionType)]
                }
            }
        },
        {
            tableName: 'userOnlineCourse',
            timestamps: true
        }
    );

    UserOnlineCourse.associate = models => {
        UserOnlineCourse.belongsTo(models.User, { as: 'user' });

        UserOnlineCourse.belongsTo(models.OnlineCourse, { as: 'course' });
    };

    UserOnlineCourse.addScopes = models => {};

    UserOnlineCourse.prototype.fields = async function() {
        const model = this.get();
        const hiddenFields = ['id', 'createdAt', 'updatedAt'];

        return _omit(model, hiddenFields);
    };

    return UserOnlineCourse;
};
