'use strict';

const _omit = require('lodash/omit');

const { CompanyConnectStatusType } = require('../../constants');

module.exports = (sequelize, DataTypes) => {
    const CompanyConnectPartnership = sequelize.define(
        'CompanyConnectPartnership',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            statusId: {
                type: DataTypes.SMALLINT,
                defaultValue: CompanyConnectStatusType.PENDING,
                validate: {
                    isIn: [Object.values(CompanyConnectStatusType)]
                }
            }
        },
        {
            tableName: 'companyConnectPartnership',
            timestamps: true
        }
    );

    CompanyConnectPartnership.associate = models => {
        CompanyConnectPartnership.belongsTo(models.Company, {
            as: 'sender',
            foreignKey: 'senderCompanyId'
        });

        CompanyConnectPartnership.belongsTo(models.Company, {
            as: 'receiver',
            foreignKey: 'receiverCompanyId'
        });

        CompanyConnectPartnership.hasOne(models.CompanyPartnership, {
            as: 'companyPartnership',
            foreignKey: 'connectPartnershipId'
        });
    };

    CompanyConnectPartnership.addScopes = models => {};

    CompanyConnectPartnership.prototype.fields = async function() {
        return _omit(this.get(), []);
    };

    return CompanyConnectPartnership;
};
