'use strict';

module.exports = (sequelize, DataTypes) => {
    const UserConnectGroup = sequelize.define(
        'UserConnectGroup',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            }
        },
        {
            tableName: 'userConnectGroup',
            timestamps: true
        }
    );

    UserConnectGroup.associate = models => {
        UserConnectGroup.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });

        UserConnectGroup.belongsTo(models.UserConnectGroup, {
            as: 'connectGroupUsers',
            foreignKey: 'groupId'
        });

        UserConnectGroup.hasMany(models.Group, {
            as: 'group',
            foreignKey: 'id'
        });
    };

    return UserConnectGroup;
};
