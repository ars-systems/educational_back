'use strict';

const _omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
    const OnlineCourseRate = sequelize.define(
        'OnlineCourseRate',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            userId: {
                type: DataTypes.BIGINT
            },
            OnlineCourseId: {
                type: DataTypes.BIGINT
            },
            rate: {
                type: DataTypes.SMALLINT
            }
        },
        {
            tableName: 'onlineCourseRate',
            timestamps: true,
            setterMethods: {}
        }
    );

    OnlineCourseRate.associate = models => {
        OnlineCourseRate.belongsTo(models.OnlineCourse, {
            as: 'onlineCourse',
            foreignKey: 'onlineCourseId'
        });
    };

    OnlineCourseRate.addScopes = models => {};

    OnlineCourseRate.prototype.fields = function() {
        const model = this.get();
        const hiddenFields = ['createdAt', 'updatedAt'];

        return _omit(model, hiddenFields);
    };

    return OnlineCourseRate;
};
