'use strict';

const { ConnectStatus } = require('../../constants');

module.exports = (sequelize, DataTypes) => {
    const ThreadRequest = sequelize.define(
        'ThreadRequest',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            statusId: {
                type: DataTypes.SMALLINT,
                defaultValue: ConnectStatus.ACCEPTED,
                validate: {
                    isIn: [Object.values(ConnectStatus)]
                }
            }
        },
        {
            tableName: 'threadRequest',
            timestamps: true
        }
    );

    ThreadRequest.associate = models => {
        ThreadRequest.belongsTo(models.Thread, {
            as: 'thread',
            foreignKey: 'threadId'
        });

        ThreadRequest.belongsTo(models.User, {
            as: 'sender'
        });

        ThreadRequest.belongsTo(models.User, {
            as: 'receiver'
        });
    };

    return ThreadRequest;
};
