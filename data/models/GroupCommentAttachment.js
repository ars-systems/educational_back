'use strict';

const _get = require('lodash/get');
const _omit = require('lodash/omit');
const _pick = require('lodash/pick');
const _includes = require('lodash/includes');

const allowedMimeTypes = ['jpg', 'jpeg', 'png'];
const allowedFileSize = 25 * 1024 * 1024;

module.exports = (sequelize, DataTypes) => {
    const GroupCommentAttachment = sequelize.define(
        'GroupCommentAttachment',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            key: {
                type: DataTypes.STRING
            },
            meta: {
                allowNull: false,
                type: DataTypes.JSON,
                validate: {
                    mime: async function(value) {
                        const ext = _get(value, 'ext');

                        if (!ext || !_includes(allowedMimeTypes, ext)) {
                            throw new Error('Attachment invalid mime type.');
                        } else if (_get(value, 'size') > allowedFileSize) {
                            throw new Error('Attachment exceeded  size limit.');
                        }

                        this.setDataValue('key', `group/comments/${this.groupCommentId}/${value.key}`);
                        this.setDataValue('meta', _pick(value, ['size', 'name', 'mime', 'ext']));
                    }
                }
            }
        },
        {
            tableName: 'groupCommentAttachment',
            timestamps: true
        }
    );

    GroupCommentAttachment.prototype.fields = function() {
        return _omit(this.get(), [
            'id',
            'groupCommentId',
            'meta.size',
            'meta.mime',
            'meta.ext',
            'createdAt',
            'updatedAt'
        ]);
    };

    GroupCommentAttachment.associate = models => {
        GroupCommentAttachment.belongsTo(models.GroupComment, {
            as: 'groupComment',
            foreignKey: 'groupCommentId'
        });
    };

    return GroupCommentAttachment;
};
