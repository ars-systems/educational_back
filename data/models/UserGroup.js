'use strict';

const _omit = require('lodash/omit');

const { GroupConnectionStatus, GroupConnectionType } = require('./../../constants');

module.exports = (sequelize, DataTypes) => {
    const UserGroup = sequelize.define(
        'UserGroup',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            status: {
                type: DataTypes.SMALLINT,
                validate: {
                    isIn: [Object.values(GroupConnectionStatus)]
                }
            },
            type: {
                type: DataTypes.SMALLINT,
                validate: {
                    isIn: [Object.values(GroupConnectionType)]
                }
            }
        },
        {
            tableName: 'userGroup',
            timestamps: true
        }
    );

    UserGroup.associate = models => {
        UserGroup.belongsTo(models.User, { as: 'user' });

        UserGroup.belongsTo(models.Group, { as: 'group' });
    };

    UserGroup.addScopes = models => {};

    UserGroup.prototype.fields = async function() {
        const model = this.get();
        const hiddenFields = ['id', 'createdAt', 'updatedAt'];

        return _omit(model, hiddenFields);
    };

    return UserGroup;
};
