'use strict';

const _trim = require('lodash/trim');
const _omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
    const UserWork = sequelize.define(
        'UserWork',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            industry: {
                type: DataTypes.STRING(100),
                validate: {
                    len: [0, 100]
                }
            },
            occupation: {
                type: DataTypes.STRING(100),
                validate: {
                    len: [0, 100]
                }
            }
        },
        {
            tableName: 'userWork',
            timestamps: true,
            setterMethods: {
                occupation(value) {
                    this.setDataValue('occupation', value ? _trim(value) : null);
                },
                industry(value) {
                    this.setDataValue('industry', value ? _trim(value) : null);
                }
            }
        }
    );

    UserWork.associate = models => {
        UserWork.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });

        UserWork.belongsTo(models.Company, {
            as: 'company',
            foreignKey: 'companyId'
        });
    };

    UserWork.prototype.fields = async function() {
        const userWork = this.get();

        let hiddenFields = ['id', 'userId', 'companyId', 'createdAt', 'updatedAt'];

        return _omit(userWork, hiddenFields);
    };

    return UserWork;
};
