'use strict';

module.exports = (sequelize, DataTypes) => {
    const GroupCommentUser = sequelize.define(
        'GroupCommentUser',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            text: {
                type: DataTypes.TEXT
            },
            channel: {
                type: DataTypes.TEXT
            }
        },
        {
            tableName: 'groupCommentUser',
            timestamps: true
        }
    );

    GroupCommentUser.associate = models => {
        GroupCommentUser.belongsTo(models.User, {
            as: 'user',
            foreignKey: 'userId'
        });

        GroupCommentUser.belongsTo(models.GroupComment, {
            as: 'groupComment',
            foreignKey: 'commentId'
        });
    };

    return GroupCommentUser;
};
