'use strict';

const _set = require('lodash/set');
const _omit = require('lodash/omit');
const _isEmpty = require('lodash/isEmpty');

const { GroupType } = require('./../../constants');

module.exports = (sequelize, DataTypes) => {
    const Group = sequelize.define(
        'Group',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            image: {
                type: DataTypes.STRING
            },
            description: {
                type: DataTypes.STRING
            },
            availability: {
                type: DataTypes.STRING
            },
            tags: {
                type: DataTypes.ARRAY(DataTypes.TEXT)
            },
            typeId: {
                type: DataTypes.SMALLINT,
                validate: {
                    isIn: [Object.values(GroupType)]
                }
            }
        },
        {
            tableName: 'group',
            timestamps: true,
            setterMethods: {}
        }
    );

    Group.associate = models => {
        Group.hasMany(models.GroupMedia, {
            as: 'media',
            foreignKey: 'groupId'
        });

        Group.hasMany(models.UserConnectGroup, {
            as: 'userConnectGroup',
            foreignKey: 'groupId'
        });

        Group.belongsTo(models.User, {
            as: 'groupAdmin',
            foreignKey: 'userId'
        });

        Group.hasMany(models.GroupRate, {
            as: 'rate',
            foreignKey: 'groupId'
        });
    };

    Group.addScopes = models => {
        Group.addScope('count', () => {
            return {
                subQuery: false,
                attributes: {
                    include: [[sequelize.fn('COUNT', sequelize.col('userConnectGroup.id')), 'participantsCount']]
                },
                include: [
                    {
                        model: models.UserConnectGroup,
                        as: 'userConnectGroup',
                        attributes: [],
                        include: [
                            {
                                model: models.User,
                                as: 'user',
                                attributes: [],
                                include: [
                                    {
                                        model: models.UserProfile,
                                        as: 'profile',
                                        attributes: []
                                    }
                                ]
                            }
                        ]
                    }
                ]
            };
        });
    };

    Group.prototype.fields = async function() {
        const model = this.get();
        const hiddenFields = ['userId', 'createdAt', 'updatedAt'];

        const rates = await this.getRate();
        let rateAvg = 0;

        if (!_isEmpty(rates)) {
            let sum = rates.reduce((previous, current) => (current.rate += previous.rate));
            rateAvg = sum / rates.length;
        }

        _set(model, 'rate', rateAvg);
        _set(model, 'media', this.media);
        _set(model, '_meta', this._meta);

        return _omit(model, hiddenFields);
    };

    return Group;
};
