'use strict';

const _omit = require('lodash/omit');
const _castArray = require('lodash/castArray');

const { UserConnectStatusType } = require('../../constants');

module.exports = (sequelize, DataTypes) => {
    const CompanyConnect = sequelize.define(
        'CompanyConnect',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                autoIncrement: true
            },
            statusId: {
                type: DataTypes.SMALLINT,
                defaultValue: UserConnectStatusType.PENDING,
                validate: {
                    isIn: [Object.values(UserConnectStatusType)]
                }
            }
        },
        {
            tableName: 'companyConnect',
            timestamps: true
        }
    );

    CompanyConnect.associate = models => {
        CompanyConnect.belongsTo(models.User, {
            as: 'sender'
        });

        CompanyConnect.belongsTo(models.User, {
            as: 'receiver'
        });

        CompanyConnect.belongsTo(models.Company, {
            as: 'company'
        });

        CompanyConnect.hasOne(models.CompanyUserConnect, {
            as: 'userConnect',
            foreignKey: 'connectId'
        });
    };

    CompanyConnect.addScopes = models => {
        CompanyConnect.addScope('connectionsByIds', (userId, ids) => {
            return {
                raw: true,
                attributes: [
                    'id',
                    [
                        sequelize.literal('CASE WHEN "senderId" = :userId THEN "receiverId" ELSE "senderId" END'),
                        'userId'
                    ]
                ],
                where: {
                    $or: [
                        {
                            senderId: {
                                $in: _castArray(ids)
                            },
                            receiverId: userId
                        },
                        {
                            senderId: userId,
                            receiverId: {
                                $in: _castArray(ids)
                            }
                        }
                    ],
                    statusId: UserConnectStatusType.ACCEPTED
                },
                replacements: { userId }
            };
        });

        CompanyConnect.addScope('connections', userId => {
            return {
                raw: true,
                attributes: [
                    'id',
                    [
                        sequelize.literal('CASE WHEN "senderId" = :userId THEN "receiverId" ELSE "senderId" END'),
                        'userId'
                    ]
                ],
                where: {
                    $or: [
                        {
                            senderId: userId
                        },
                        {
                            receiverId: userId
                        }
                    ],
                    statusId: UserConnectStatusType.ACCEPTED
                },
                replacements: { userId }
            };
        });

        CompanyConnect.addScope('connects', (userId, companyId, queryString = {}) => {
            return {
                subQuery: false,
                where: {
                    '$receiver.id$': {
                        $ne: userId
                    },
                    companyId,
                    statusId: UserConnectStatusType.ACCEPTED,
                    ...queryString
                },
                include: [
                    {
                        model: models.Company,
                        as: 'company',
                        attributes: ['name']
                    },
                    {
                        model: models.User,
                        as: 'sender',
                        include: ['profile']
                    },
                    {
                        model: models.User,
                        as: 'receiver',
                        include: [
                            {
                                model: models.UserProfile,
                                as: 'profile'
                            },
                            {
                                model: models.CompanyUserConnect,
                                as: 'role',
                                attributes: ['roleId']
                            }
                        ]
                    }
                ]
            };
        });

        CompanyConnect.addScope('disconnects', (userId, companyId, queryString = {}) => {
            return {
                where: {
                    companyId,
                    statusId: UserConnectStatusType.DISCONNECTED,
                    ...queryString
                },
                include: [
                    {
                        model: models.Company,
                        as: 'company',
                        attributes: ['name']
                    },
                    {
                        model: models.User,
                        as: 'sender',
                        include: ['profile']
                    },
                    {
                        model: models.User,
                        as: 'receiver',
                        include: [
                            {
                                model: models.UserProfile,
                                as: 'profile'
                            }
                        ]
                    }
                ]
            };
        });
    };

    CompanyConnect.prototype.fields = async function() {
        return _omit(this.get(), ['connectId', 'senderId', 'receiverId', 'companyId']);
    };

    return CompanyConnect;
};
