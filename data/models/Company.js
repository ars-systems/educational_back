'use strict';

const _set = require('lodash/set');
const _omit = require('lodash/omit');
const _trim = require('lodash/trim');
const validator = require('validator');
const _isEmpty = require('lodash/isEmpty');

const { Security } = require('../../components');
const { CompanyConnectStatusType, ConnectionType } = require('./../../constants');

module.exports = (sequelize, DataTypes) => {
    const Company = sequelize.define(
        'Company',
        {
            id: {
                primaryKey: true,
                type: DataTypes.BIGINT,
                defaultValue: DataTypes.BIGINT
            },
            name: {
                allowNull: false,
                type: DataTypes.STRING
            },
            ein: {
                type: DataTypes.STRING
            },
            websiteUrl: {
                type: DataTypes.STRING,
                validate: {
                    isUrl: async function(value) {
                        if (!validator.isURL(value)) {
                            throw new Error('WebsiteUrl is not valid');
                        } else if (this.isSeller && !(await Security.checkUrlAvailability(value))) {
                            throw new Error('WebsiteUrl is not available');
                        }
                    }
                }
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
                validate: {
                    isEmail: true
                }
            },
            logo: {
                type: DataTypes.STRING
            },
            isSeller: {
                type: DataTypes.BOOLEAN,
                defaultValue: false
            }
        },
        {
            tableName: 'company',
            timestamps: true,
            setterMethods: {
                name(value) {
                    this.setDataValue('name', value ? _trim(value) : null);
                },
                ein(value) {
                    this.setDataValue('ein', value ? _trim(value) : null);
                },
                websiteUrl(value) {
                    this.setDataValue('websiteUrl', value ? _trim(value) : null);
                },
                email(value) {
                    this.setDataValue('email', value ? validator.normalizeEmail(value) : null);
                },
                logo(value) {
                    this.setDataValue('logo', _isEmpty(value) ? value : `media/company/${this.id}/${value}`);
                }
            }
        }
    );

    Company.associate = models => {
        Company.belongsTo(models.User, {
            as: 'users',
            foreignKey: 'userId'
        });

        Company.hasOne(models.CompanyLocation, {
            as: 'location',
            foreignKey: 'companyId'
        });

        Company.hasMany(models.CompanyConnect, {
            as: 'connect',
            foreignKey: 'companyId'
        });

        Company.hasOne(models.UserWork, {
            as: 'work',
            foreignKey: 'companyId'
        });

        Company.hasMany(models.CompanyConnectPartnership, {
            as: 'sentRequests',
            foreignKey: 'senderCompanyId'
        });

        Company.hasMany(models.CompanyConnectPartnership, {
            as: 'receivedRequests',
            foreignKey: 'receiverCompanyId'
        });

        Company.hasOne(models.CompanyPartnership, {
            as: 'partnership',
            foreignKey: 'companyId'
        });
    };

    Company.addScopes = models => {
        Company.addScope('expand', userId => {
            return {
                where: {
                    userId
                },
                include: [
                    {
                        model: models.CompanyLocation,
                        as: 'location'
                    }
                ]
            };
        });

        Company.addScope('connects', companyId => {
            return {
                subQuery: false,
                where: {
                    $and: [
                        {
                            $or: [
                                {
                                    id: {
                                        $in: models.CompanyConnectPartnership.generateNestedQuery({
                                            attributes: ['receiverCompanyId'],
                                            where: {
                                                senderCompanyId: companyId,
                                                statusId: CompanyConnectStatusType.ACCEPTED
                                            }
                                        })
                                    }
                                },
                                {
                                    id: {
                                        $in: models.CompanyConnectPartnership.generateNestedQuery({
                                            attributes: ['senderCompanyId'],
                                            where: {
                                                receiverCompanyId: companyId,
                                                statusId: CompanyConnectStatusType.ACCEPTED
                                            }
                                        })
                                    }
                                }
                            ]
                        }
                    ]
                },
                include: [{ model: models.CompanyPartnership, as: 'partnership', attributes: ['createdAt'] }],
                attributes: ['id', 'name', 'logo', 'email']
            };
        });

        Company.addScope('disconnects', companyId => {
            return {
                subQuery: false,
                where: {
                    $and: [
                        {
                            $or: [
                                {
                                    id: {
                                        $in: models.CompanyConnectPartnership.generateNestedQuery({
                                            attributes: ['receiverCompanyId'],
                                            where: {
                                                senderCompanyId: companyId,
                                                statusId: CompanyConnectStatusType.DISCONNECTED
                                            }
                                        })
                                    }
                                },
                                {
                                    id: {
                                        $in: models.CompanyConnectPartnership.generateNestedQuery({
                                            attributes: ['senderCompanyId'],
                                            where: {
                                                receiverCompanyId: companyId,
                                                statusId: CompanyConnectStatusType.DISCONNECTED
                                            }
                                        })
                                    }
                                }
                            ]
                        }
                    ]
                },
                attributes: ['id', 'name', 'logo', 'email']
            };
        });

        Company.addScope('sentCompanyConnects', (companyId, status) => {
            return {
                where: {
                    id: {
                        $in: models.CompanyConnectPartnership.generateNestedQuery({
                            attributes: ['receiverCompanyId'],
                            where: { senderCompanyId: companyId, statusId: status }
                        })
                    }
                },
                attributes: ['id', 'name', 'logo']
            };
        });

        Company.addScope('receivedCompanyConnects', (companyId, status) => {
            return {
                where: {
                    id: {
                        $in: models.CompanyConnectPartnership.generateNestedQuery({
                            attributes: ['senderCompanyId'],
                            where: { receiverCompanyId: companyId, statusId: status }
                        })
                    }
                },
                attributes: ['id', 'name', 'logo']
            };
        });
    };

    Company.prototype.fields = async function(models, params) {
        const company = this.get();

        _set(company, 'location', this.location);

        let companyModel = await models.Company.findOne({ where: { userId: params.userId } });

        if (!_isEmpty(companyModel) && company.id !== companyModel.id) {
            let companyPartnership = await models.CompanyConnectPartnership.findOne({
                where: {
                    $or: [
                        {
                            senderCompanyId: companyModel.id,
                            receiverCompanyId: company.id
                        },
                        {
                            senderCompanyId: company.id,
                            receiverCompanyId: companyModel.id
                        }
                    ]
                }
            });

            if (_isEmpty(companyPartnership)) {
                companyPartnership = { type: ConnectionType.NOT_CONNECTED };
            } else if (companyPartnership.statusId === CompanyConnectStatusType.ACCEPTED) {
                companyPartnership = { type: ConnectionType.CONNECTED };
            } else if (companyPartnership.statusId === CompanyConnectStatusType.PENDING) {
                companyPartnership =
                    companyModel.id === companyPartnership.senderCompanyId
                        ? { type: ConnectionType.SENT_REQUEST }
                        : { type: ConnectionType.RECEIVED_REQUEST };
            } else {
                companyPartnership = { type: ConnectionType.DISCONNECTED };
            }

            _set(company, 'connection', companyPartnership);
        }

        return _omit(this.get(), ['userId']);
    };

    return Company;
};
