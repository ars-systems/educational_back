'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('groupCommentAttachment', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            key: {
                allowNull: false,
                type: Sequelize.STRING
            },
            meta: {
                type: Sequelize.JSON
            },
            groupCommentId: {
                allowNull: false,
                unique: true,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'groupComment',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addIndex('groupCommentAttachment', ['key', 'createdAt', 'updatedAt']);
    },

    async down(queryInterface) {
        await queryInterface.dropTable('groupCommentAttachment');
    }
};
