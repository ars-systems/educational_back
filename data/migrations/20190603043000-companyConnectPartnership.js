'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('companyConnectPartnership', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT
            },
            statusId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.SMALLINT
            },
            senderCompanyId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'company',
                    key: 'id'
                }
            },
            receiverCompanyId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'company',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    async down(queryInterface) {
        await queryInterface.dropTable('companyConnectPartnership');
    }
};
