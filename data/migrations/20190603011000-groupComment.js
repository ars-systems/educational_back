'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('groupComment', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            userId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            groupId: {
                allowNull: false,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'groupMedia',
                    key: 'id'
                }
            },
            replyId: {
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'groupComment',
                    key: 'id'
                }
            },
            text: {
                type: Sequelize.TEXT
            },
            isPrivate: {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addIndex('groupComment', ['text', 'createdAt', 'updatedAt']);
    },

    async down(queryInterface) {
        await queryInterface.dropTable('groupComment');
    }
};
