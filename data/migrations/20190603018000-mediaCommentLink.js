'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('mediaCommentLink', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            url: {
                allowNull: false,
                type: Sequelize.TEXT
            },
            title: {
                type: Sequelize.STRING
            },
            description: {
                type: Sequelize.TEXT
            },
            image: {
                type: Sequelize.TEXT
            },
            mediaCommentId: {
                allowNull: false,
                unique: true,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'mediaComment',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addIndex('mediaCommentLink', [
            'url',
            'title',
            'description',
            'image',
            'createdAt',
            'updatedAt'
        ]);
    },

    async down(queryInterface) {
        await queryInterface.dropTable('mediaCommentLink');
    }
};
