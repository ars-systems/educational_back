'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.addColumn('media', 'category', {
            type: Sequelize.SMALLINT
        });
    },

    async down(queryInterface) {}
};
