'use strict';

const { GroupConnectionStatus } = require('./../../constants');

module.exports = {
    async up(queryInterface, Sequelize) {
        return queryInterface.createTable('userGroup', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            userId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            groupId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'group',
                    key: 'id'
                }
            },
            status: {
                type: Sequelize.SMALLINT,
                defaultValue: GroupConnectionStatus.PENDING
            },
            type: {
                type: Sequelize.SMALLINT
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    async down(queryInterface) {
        return queryInterface.dropTable('userGroup');
    }
};
