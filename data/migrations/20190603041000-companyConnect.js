'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('companyConnect', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            statusId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.SMALLINT
            },
            senderId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            receiverId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            companyId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'company',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    async down(queryInterface) {
        await queryInterface.dropTable('companyConnect');
    }
};
