'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('messageRecipient', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            threadId: {
                allowNull: false,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'thread',
                    key: 'id'
                }
            },
            messageId: {
                allowNull: false,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'message',
                    key: 'id'
                }
            },
            userId: {
                allowNull: false,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            isSeen: {
                defaultValue: false,
                type: Sequelize.BOOLEAN
            },
            isRead: {
                defaultValue: false,
                type: Sequelize.BOOLEAN
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addIndex('messageRecipient', ['isSeen', 'isRead', 'createdAt', 'updatedAt']);
        await queryInterface.addIndex('messageRecipient', {
            unique: true,
            fields: ['threadId', 'messageId', 'userId']
        });
    },

    async down(queryInterface) {
        await queryInterface.dropTable('messageRecipient');
    }
};
