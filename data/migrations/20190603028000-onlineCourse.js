'use strict';

const { OnlineCourseConnectionStatus } = require('./../../constants');

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('onlineCourse', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            name: {
                allowNull: false,
                type: Sequelize.STRING
            },
            image: {
                type: Sequelize.STRING
            },
            description: {
                allowNull: false,
                type: Sequelize.STRING
            },
            availability: {
                allowNull: false,
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            category: {
                allowNull: false,
                type: Sequelize.SMALLINT
            },
            type: {
                allowNull: false,
                type: Sequelize.SMALLINT
            },
            duration: {
                type: Sequelize.SMALLINT,
                defaultValue: 3
            },
            participantsCount: {
                type: Sequelize.SMALLINT
            },
            userId: {
                allowNull: false,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            startDate: {
                type: Sequelize.DATE,
                defaultValue: new Date(Date.now() + 3 * 86400000)
            },
            status: {
                type: Sequelize.SMALLINT,
                defaultValue: OnlineCourseConnectionStatus.PENDING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    async down(queryInterface) {
        await queryInterface.dropTable('onlineCourse');
    }
};
