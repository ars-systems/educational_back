'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        return queryInterface.createTable('media', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            userId: {
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            typeId: {
                allowNull: false,
                type: Sequelize.SMALLINT
            },
            data: {
                type: Sequelize.STRING(250),
                allowNull: false,
                unique: true
            },
            thumbnail: {
                type: Sequelize.STRING(250)
            },
            image: {
                type: Sequelize.STRING(250)
            },
            meta: {
                type: Sequelize.JSON
            },
            name: {
                type: Sequelize.STRING
            },
            sphere: {
                type: Sequelize.STRING
            },
            author: {
                type: Sequelize.STRING
            },
            pageCount: {
                type: Sequelize.INTEGER,
                defaultValue: 1
            },
            description: {
                type: Sequelize.STRING
            },
            availability: {
                allowNull: false,
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            onlineCourseDate: {
                type: Sequelize.DATE
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    async down(queryInterface) {
        return queryInterface.dropTable('media');
    }
};
