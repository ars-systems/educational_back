'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('mediaCommentAttachment', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            key: {
                allowNull: false,
                type: Sequelize.STRING
            },
            meta: {
                type: Sequelize.JSON
            },
            mediaCommentId: {
                allowNull: false,
                unique: true,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'mediaComment',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addIndex('mediaCommentAttachment', ['key', 'createdAt', 'updatedAt']);
    },

    async down(queryInterface) {
        await queryInterface.dropTable('mediaCommentAttachment');
    }
};
