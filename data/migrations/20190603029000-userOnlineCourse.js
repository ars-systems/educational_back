'use strict';

const { OnlineCourseConnectionStatus } = require('./../../constants');

module.exports = {
    async up(queryInterface, Sequelize) {
        return queryInterface.createTable('userOnlineCourse', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            userId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            courseId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'onlineCourse',
                    key: 'id'
                }
            },
            status: {
                type: Sequelize.SMALLINT,
                defaultValue: OnlineCourseConnectionStatus.PENDING
            },
            type: {
                type: Sequelize.SMALLINT
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    async down(queryInterface) {
        return queryInterface.dropTable('userOnlineCourse');
    }
};
