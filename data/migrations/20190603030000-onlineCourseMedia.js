'use strict';

const { OnlineCourseType } = require('./../../constants');

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('onlineCourseMedia', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            text: {
                type: Sequelize.TEXT
            },
            userId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            onlineCourseId: {
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'onlineCourse',
                    key: 'id'
                }
            },
            typeId: {
                allowNull: false,
                type: Sequelize.SMALLINT,
                defaultValue: OnlineCourseType.NO_CONTENT
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addIndex('onlineCourseMedia', ['text', 'createdAt', 'updatedAt']);
    },

    async down(queryInterface) {
        await queryInterface.dropTable('onlineCourseMedia');
    }
};
