'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        return queryInterface.createTable('companyLocation', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT
            },
            firstAddress: {
                type: Sequelize.STRING
            },
            secondAddress: {
                type: Sequelize.STRING
            },
            country: {
                type: Sequelize.STRING
            },
            city: {
                type: Sequelize.STRING
            },
            state: {
                type: Sequelize.STRING
            },
            zipCode: {
                type: Sequelize.STRING,
                allowNull: false
            },
            latitude: {
                type: Sequelize.DECIMAL(9, 6)
            },
            longitude: {
                type: Sequelize.DECIMAL(9, 6)
            },
            companyId: {
                allowNull: false,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'company',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    async down(queryInterface) {
        await queryInterface.dropTable('companyLocation');
    }
};
