'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('message', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            text: {
                type: Sequelize.TEXT
            },
            typeId: {
                allowNull: false,
                type: Sequelize.SMALLINT
            },
            threadId: {
                allowNull: false,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'thread',
                    key: 'id'
                }
            },
            userId: {
                allowNull: false,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            index: {
                type: Sequelize.INTEGER
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addColumn('thread', 'lastMessageId', {
            allowNull: true,
            type: Sequelize.BIGINT,
            onDelete: 'CASCADE',
            references: {
                model: 'message',
                key: 'id'
            }
        });

        await queryInterface.addIndex('thread', ['lastMessageId']);
        await queryInterface.addIndex('message', ['text', 'createdAt', 'updatedAt']);
    },

    async down(queryInterface) {
        await queryInterface.removeColumn('thread', 'lastMessageId');
        await queryInterface.dropTable('message');
    }
};
