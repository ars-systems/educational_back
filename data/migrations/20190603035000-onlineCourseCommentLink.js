'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('onlineCourseCommentLink', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            url: {
                allowNull: false,
                type: Sequelize.TEXT
            },
            title: {
                type: Sequelize.STRING
            },
            description: {
                type: Sequelize.TEXT
            },
            image: {
                type: Sequelize.TEXT
            },
            onlineCourseCommentId: {
                allowNull: false,
                unique: true,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'onlineCourseComment',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addIndex('onlineCourseCommentLink', [
            'url',
            'title',
            'description',
            'image',
            'createdAt',
            'updatedAt'
        ]);
    },

    async down(queryInterface) {
        await queryInterface.dropTable('onlineCourseCommentLink');
    }
};
