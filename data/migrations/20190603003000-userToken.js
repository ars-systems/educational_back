'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('userToken', {
            token: {
                primaryKey: true,
                type: Sequelize.STRING
            },
            userId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            typeId: {
                allowNull: false,
                type: Sequelize.SMALLINT
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addIndex('userToken', {
            unique: true,
            fields: ['userId', 'typeId']
        });
    },

    async down(queryInterface) {
        await queryInterface.dropTable('userToken');
    }
};
