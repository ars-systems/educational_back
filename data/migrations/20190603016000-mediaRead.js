'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        return queryInterface.createTable('mediaRead', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            userId: {
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            mediaId: {
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'media',
                    key: 'id'
                }
            },
            status: {
                type: Sequelize.SMALLINT
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    async down(queryInterface) {
        return queryInterface.dropTable('mediaRead');
    }
};
