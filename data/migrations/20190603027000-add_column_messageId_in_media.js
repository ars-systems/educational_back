'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.addColumn('media', 'messageId', {
            onDelete: 'CASCADE',
            type: Sequelize.BIGINT,
            references: {
                model: 'message',
                key: 'id'
            }
        });
    },

    async down(queryInterface) {}
};
