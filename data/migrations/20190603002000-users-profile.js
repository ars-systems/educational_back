'use strict';

const { AccountType } = require('./../../constants');

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('userProfile', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            firstName: {
                allowNull: false,
                type: Sequelize.STRING(50),
                validate: {
                    len: [2, 50]
                }
            },
            lastName: {
                type: Sequelize.STRING(50),
                validate: {
                    len: [2, 50]
                }
            },
            avatar: {
                type: Sequelize.STRING
            },
            university: {
                type: Sequelize.STRING
            },
            profession: {
                type: Sequelize.STRING
            },
            work: {
                type: Sequelize.STRING
            },
            gender: {
                type: Sequelize.BOOLEAN
            },
            userId: {
                unique: true,
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            typeId: {
                type: Sequelize.SMALLINT,
                defaultValue: AccountType.OTHER
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addIndex('userProfile', ['firstName', 'lastName', 'createdAt']);
    },

    async down(queryInterface) {
        await queryInterface.dropTable('userProfile');
    }
};
