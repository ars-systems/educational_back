'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('userConnectOnlineCourse', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            courseId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'onlineCourse',
                    key: 'id'
                }
            },
            userId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addIndex('userConnectOnlineCourse', {
            unique: true,
            fields: ['courseId', 'userId']
        });
    },

    async down(queryInterface) {
        await queryInterface.dropTable('userConnectOnlineCourse');
    }
};
