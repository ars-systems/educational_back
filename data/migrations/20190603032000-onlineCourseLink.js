'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('onlineCourseLink', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            url: {
                allowNull: false,
                type: Sequelize.TEXT
            },
            title: {
                type: Sequelize.STRING
            },
            description: {
                type: Sequelize.TEXT
            },
            image: {
                type: Sequelize.TEXT
            },
            onlineCourseId: {
                allowNull: false,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'onlineCourseMedia',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addIndex('onlineCourseLink', [
            'url',
            'title',
            'description',
            'image',
            'createdAt',
            'updatedAt'
        ]);
    },

    async down(queryInterface) {
        await queryInterface.dropTable('onlineCourseLink');
    }
};
