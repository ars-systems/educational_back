'use strict';

module.exports = {
    async up(queryInterface) {
        await queryInterface.sequelize.query(`
                    CREATE OR REPLACE FUNCTION afterMessageInsert()
                      RETURNS TRIGGER AS $BODY$
                    BEGIN
                      UPDATE "thread"
                      SET "lastMessageId" = NEW."id"
                      WHERE "id" = NEW."threadId";
                      
                    INSERT INTO "messageRecipient" ("threadId", "messageId", "userId", "createdAt", "updatedAt")
                      SELECT
                        NEW."threadId",
                        NEW."id",
                        "threadUser"."userId",
                        NOW(),
                        NOW()
                      FROM "threadUser"
                      WHERE "threadUser"."userId" != NEW."userId" AND "threadId" = NEW."threadId";
                     
                      RETURN NEW;
                    END;
                    $BODY$
                    LANGUAGE plpgsql;
                    
                    CREATE TRIGGER afterMessageInsert
                      AFTER INSERT
                      ON "message"
                      FOR EACH ROW EXECUTE PROCEDURE afterMessageInsert();
        `);
    },

    async down(queryInterface) {
        await queryInterface.sequelize.query('DROP TRIGGER afterMessageInsert ON "message"');
    }
};
