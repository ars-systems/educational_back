'use strict';

const { GroupMediaType } = require('./../../constants');

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('groupMedia', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            text: {
                type: Sequelize.TEXT
            },
            userId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            groupId: {
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'group',
                    key: 'id'
                }
            },
            parentId: {
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'groupMedia',
                    key: 'id'
                }
            },
            rootId: {
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'groupMedia',
                    key: 'id'
                }
            },
            typeId: {
                allowNull: false,
                type: Sequelize.SMALLINT,
                defaultValue: GroupMediaType.NO_CONTENT
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addIndex('groupMedia', ['text', 'createdAt', 'updatedAt']);
    },

    async down(queryInterface) {
        await queryInterface.dropTable('groupMedia');
    }
};
