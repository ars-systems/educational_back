'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('onlineCourseAttachment', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            key: {
                type: Sequelize.STRING,
                onDelete: 'SET NUll',
                references: {
                    model: 'media',
                    key: 'data'
                }
            },
            onlineCourseId: {
                allowNull: false,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'onlineCourseMedia',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addIndex('onlineCourseAttachment', ['key', 'createdAt', 'updatedAt']);
    },

    async down(queryInterface) {
        await queryInterface.dropTable('onlineCourseAttachment');
    }
};
