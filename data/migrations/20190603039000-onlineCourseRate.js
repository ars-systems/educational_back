'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        return queryInterface.createTable('onlineCourseRate', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            userId: {
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            onlineCourseId: {
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'onlineCourse',
                    key: 'id'
                }
            },
            rate: {
                type: Sequelize.SMALLINT
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    async down(queryInterface) {
        return queryInterface.dropTable('onlineCourseRate');
    }
};
