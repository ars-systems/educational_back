'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('groupLink', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            url: {
                allowNull: false,
                type: Sequelize.TEXT
            },
            title: {
                type: Sequelize.STRING
            },
            description: {
                type: Sequelize.TEXT
            },
            image: {
                type: Sequelize.TEXT
            },
            groupId: {
                allowNull: false,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'groupMedia',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addIndex('groupLink', [
            'url',
            'title',
            'description',
            'image',
            'createdAt',
            'updatedAt'
        ]);
    },

    async down(queryInterface) {
        await queryInterface.dropTable('groupLink');
    }
};
