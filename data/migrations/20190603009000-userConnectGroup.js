'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('userConnectGroup', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT,
                autoIncrement: true
            },
            groupId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'group',
                    key: 'id'
                }
            },
            userId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });

        await queryInterface.addIndex('userConnectGroup', {
            unique: true,
            fields: ['groupId', 'userId']
        });
    },

    async down(queryInterface) {
        await queryInterface.dropTable('userConnectGroup');
    }
};
