'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('companyPartnership', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT
            },
            connectPartnershipId: {
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'companyConnectPartnership',
                    key: 'id'
                }
            },
            companyId: {
                allowNull: false,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'company',
                    key: 'id'
                }
            },
            statusId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.SMALLINT
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    async down(queryInterface) {
        await queryInterface.dropTable('companyPartnership');
    }
};
