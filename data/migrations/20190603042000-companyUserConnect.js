'use strict';

const { CompanyRole } = require('../../constants');

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('companyUserConnect', {
            id: {
                primaryKey: true,
                type: Sequelize.BIGINT
            },
            connectId: {
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'companyConnect',
                    key: 'id'
                }
            },
            userId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.BIGINT,
                references: {
                    model: 'user',
                    key: 'id'
                }
            },
            companyId: {
                allowNull: false,
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                references: {
                    model: 'company',
                    key: 'id'
                }
            },
            statusId: {
                allowNull: false,
                onDelete: 'CASCADE',
                type: Sequelize.SMALLINT
            },
            roleId: {
                onDelete: 'CASCADE',
                type: Sequelize.SMALLINT,
                defaultValue: CompanyRole.MEMBER
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    async down(queryInterface) {
        await queryInterface.dropTable('companyUserConnect');
    }
};
