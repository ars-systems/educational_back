'use strict';

const Koa = require('koa');
const app = new Koa();
const serve = require('koa-static');

const config = require('./config');
/**
 * ############## MIDDLEWARES ##############
 */
app.use(require('@koa/cors')());
app.use(require('koa-static')('./media'));
app.use(
    require('./middlewares/requestNormalizer')({
        bodyParser: {
            multipart: true,
            formidable: {
                maxFileSize: 262144000
            }
        },
        paginate: {
            limit: config.get('request:limit')
        }
    })
);
app.use(
    require('./middlewares/restify')({
        serializer: {
            options: {
                userId: 'state.user.id'
            }
        }
    })
);

/**
 * ############## ROUTES ##############
 */
const v1Routes = require('./routes');

app.use(v1Routes.routes());
app.use(v1Routes.allowedMethods());

/**
 * ############## SERVER CONFIGURATION ##############
 */
let port = process.env.PORT || config.get('server:port') || 4000;

const server = require('http').createServer(app.callback());

server.listen(port, () => {
    console.info(`Server is running on port : ${port}`);
});
