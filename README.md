# Educational backend

#### Check node version
###### node v11 +
##

#### Install sequelize globally
```bash
npm i sequelize-cli -g
```
##

#### Check postgres user credentials
##

#### Add config file 
config -> development -> main.json
```bash
{
  "db": {
    "host": NODE_ENV.HOST,
    "database": NODE_ENV.DB,
    "username": NODE_ENV.USERNAME,
    "password": NODE_ENV.PASSWORD,
    "dialect": "postgres",
    "logging" : true
  },
  "server": {
    "port": NODE_ENV.PORT,
    "url": NODE_ENV.URL
  },
  "sendGrid": {
    "api": {
      "key": NODE_ENV.SENDGRID
    },
    "from": NODE_ENV.EMAIL
  }
}
```

config -> development -> params.json
```bash
{}
```
##

#### Installing project dependencies
##

```bash 
*install packages
$ npm i

*create DB
$ sequelize db:create

* run migrations
$ npm run migrate-up

* run project
$ npm start
```
